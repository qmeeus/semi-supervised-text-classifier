# semi-supervised-text-classifier

Various deep learning models for long document processing

## Structure
 - `settings`: all config files for the models. They must be syntactically correct yaml files. They follow a hierarchical 
    structure, much like inheritance in object oriented programming. Children settings overwrite their parents.
 - `datasets` `embedding` `preprocessed_data` `outputs`
 - `notebooks`: various jupyter notebooks for experimentation and visualisation
 - `tensorflow_models`:
    - `data_loaders`: include loading and preprocessing functions + dataset containers used by the models
    - `mains`: scripts used for training, optimising, comparing and evaluating models
    - `supervised` `unsupervised` `semi_supervised`: where the models are defined. They follow an object oriented structure.
    - `base`: base classes
    - `utils`:
        - `custom_layers`: custom keras layers used in models
        - `io_utils`: read/write data
        - `logger`: log model training
        - `nn_utils`: model specific utils such as loss and metrics definitions, etc.
        - `op_utils`: numpy operations
        - `plotting`: visualisation helper functions
        
 - `tests`: testing modules
    
## CPU version
`docker build -f Dockerfile -t tensorflow --build-arg user_id=(id -u) .`

`docker run -d --name ssl-text-clf -v (pwd):/home/patrick/app tensorflow [main_file] [options]`

## GPU version
`docker build -f Dockerfile.gpu -t tensorflow --build-arg user_id=(id -u) .`

`docker run --runtime nvidia -d --name ssl-text-clf -v (pwd):/home/patrick/app tensorflow [main_file]`

## Logs
`docker logs -f ssl-text-clf`

## Debug a running container
`docker exec -it ssl-text-clf bash`
