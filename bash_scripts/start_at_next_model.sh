#!/bin/bash

while true; do
    status="$(ls outputs/grid_search_kate* 2> /dev/null | wc -l)"
    echo $status
    if [ $status == "ok" ]; then
        docker stop $1
        docker start $2
        exit 0
    fi
    sleep $3
done



