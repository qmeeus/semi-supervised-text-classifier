#!/bin/bash


# Checks every 30 min if container has stopped. When it has, launch next container
while true; do
    status="$(docker ps -a | grep $1 | sed -r 's/\s+/ /g' | cut -d' ' -f7)"
    if [ $status == "Exited" ]; then
        echo "Now go!"
        docker start $2
        exit 0
    else
        echo "Stay!"
    fi
    sleep $3
done

