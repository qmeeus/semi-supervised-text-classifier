# coding: utf-8
import pandas as pd
import numpy as np
from IPython.display import display
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler as Scaler
import statsmodels.api as sm
import matplotlib.pyplot as plt
import seaborn as sns


def postprocessing():
    df = pd.read_csv('all_results.csv')
    #display(df.groupby('model_name').agg(lambda g: g.isnull().sum()))

    # Convert start_date to datetime
    df['start_date'] = pd.to_datetime(df['start_date'])

    # Fill missing dates
    df.loc[(df['model_name'] == 'contr-ae+clf-128d') & (df['start_date'].isnull()), 'start_date'] = pd.Timestamp(2019,4,5)


    # Merge epc and examples_per_class
    epc = df['epc'].rename('examples_per_class')
    mask = df['examples_per_class'].notnull()
    df.loc[~mask, 'examples_per_class'] = df.loc[~mask, 'epc']
    df = df.drop('epc', axis=1)
    df['examples_per_class'] = df['examples_per_class'].astype('int')

    # Fill missing encoder_dims, convert to list and extract depth and encoding_dim
    df.loc[(df.model_name.str.endswith('512+128d')) & (df.encoder_dims.isnull()), 'encoder_dims'] = '[512, 128]'
    df.loc[(df.model_name.str.endswith('128d')) & (df.encoder_dims.isnull()), 'encoder_dims'] = '[128]'
    df.loc[(df.model_name == 'cae') & (df.encoder_dims.isnull()), 'encoder_dims'] = '[128]'
    df['encoder_layers'] = df['encoder_dims'].map(lambda s: eval(s))
    df['encoding_dim'] = df['encoder_layers'].map(lambda l: l[-1])
    df['depth'] = df['encoder_layers'].map(lambda l: len(l))
    df = df.drop(['encoder_dims', 'encoder_layers'], axis=1)

    # Fill missing competitive_topk and contractive
    df.loc[(df['model_name'] == 'ksparse-ae+clf-128d') & (df['start_date'] > pd.Timestamp(2019,3,25)), 'competitive_topk'] = 32
    df.loc[(df.model_name.str.contains('k')) & (df.competitive_topk.isnull()), ['competitive_topk', 'contractive']] = 32, 0.01
    df.loc[df.competitive_topk.isnull(), 'competitive_topk'] = 0
    df.loc[df.model_name == 'ssae', 'contractive'] = 0.
    df.loc[(~df.model_name.str.contains('contr')) & (df.contractive.isnull()), 'contractive'] = 0.

    # Rename the models
    mask = df.model_name.str.contains('\+')
    df.loc[mask, 'model_name'] = df.loc[mask, 'model_name'].map(lambda s: s.split('+')[0])

    display(df.describe())
    return df


def make_reg(df, sklearn=False):
    df = df.join(pd.get_dummies(df['model_name'])).drop('model_name', axis=1)
    X, y = df.drop('final_score', axis=1), df['final_score']
    X['contractive'] = X['contractive'].where(X.contractive != 0).map(np.log, na_action='ignore').fillna(0)
    X['examples_per_class'] = np.log(X['examples_per_class'])
    scale_cols = ['alpha', 'encoder_dims', 'examples_per_class', 'contractive']
    X[scale_cols] = Scaler().fit_transform(X[scale_cols].astype('float'))

    if sklearn:
        lr = LinearRegression()
        lr.fit(X, y)
        print("R-squared: {:.4f}".format(lr.score(X, y)))
        display(pd.DataFrame(lr.coef_, index=X.columns, columns=['coefs']))
    else:
        lr = sm.OLS(y, X)
        res = lr.fit()
        print(res.summary())


def visuals(df):
    import matplotlib.pyplot as plt
    import seaborn as sns

    # Score distribution
    sns.distplot(df.loc[df.model_name == 'contr-ae', 'final_score'])
    plt.show()

    # EPC vs SCORE
    contrae = df.loc[df.model_name == 'contr-ae'].copy()
    sns.jointplot(np.log(contrae.examples_per_class), contrae.final_score, kind='reg')

    sns.jointplot(np.log(contrae.examples_per_class), contrae.final_score, kind='reg')
    plt.show()

    sns.boxplot(x='examples_per_class', y='final_score', hue='alpha', data=contrae)
    plt.show()


if __name__ == '__main__':
    df = postprocessing()


