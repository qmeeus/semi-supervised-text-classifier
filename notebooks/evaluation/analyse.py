# coding: utf-8
import os.path as p
import subprocess
import pandas as pd
import numpy as np
import h5py
import statsmodels.api as sm
import matplotlib.pyplot as plt
from matplotlib.legend import Legend
import seaborn as sns

from IPython import embed
from IPython.display import display
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler as Scaler
from sklearn.feature_selection import SelectKBest, f_regression

sns.set_style('whitegrid')
plt.rcParams['figure.figsize'] = (10,10)
plt.rcParams['font.size'] = 16


VARIABLES = ['model_name', 'examples_per_class', 'start_date']
HYPERPARAMETERS = ['alpha', 'competitive_topk', 'contractive', 'encoding_dim', 'depth', 'l1', 'l2', 'amplification', "fc_layers", 'dropout_rate', 'input_dropout']
SCORES = ['runtime', 'accuracy', 'f1_score', 'AMI', 'NMI', 'cluster_acc', 'mse']
TARGET = 'f1_score'

MT_FIGURES_FOLDER = "/home/quent/Documents/repos/latex/master-thesis/figures"

def sh(*args):
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        out = proc.stdout.read()
    return out.decode('utf-8')


def infer_objects(df):
    for name in df.columns:
        column = df[name]
        if column.dtype == 'object':
            for dtype in ['int', 'float']:
                try:
                    df[name] = column.astype(dtype)
                    break
                except:
                    pass
    return df


def load_final_scores(filename):
    df = parse_results(filename)
    if 'unsupervised' in df.columns:
        df = df.where(df.unsupervised.notnull()).dropna(how='all')
        df['unsupervised'] = df.unsupervised.map({'true': True, 'false': False})
        df.loc[df.model_name == 'fc_classifier', 'unsupervised'] = False
        df.update(df.loc[df.unsupervised, 'model_name'].copy().map(lambda s: 'u' + s))

    return df


def parse_results(filename):
    dirname = p.basename(p.dirname(filename))
    attributes = dirname.split('_')
    start_date = pd.NaT if len(attributes) == 1 else pd.Timestamp(attributes.pop(-1))
    model_name = attributes[0] if len(attributes) == 1 else attributes.pop(-1)

    with open(filename) as f:
        lines = f.readlines()

    if not lines[0].startswith('run_id='):
        run_id, rest = zip(*[line.strip().split(' ', 1) for line in lines])
    else:
        rest = lines

    def parse(line):
        items = line.split(',')
        i = 0
        while True:
            i += 1
            if i == len(items):
                break
            if not '=' in items[i]:
                i -= 1
                items[i] = ','.join([items[i], items.pop(i+1)])
        return dict(map(lambda s: tuple(s.split('=')), items))

    results = list(map(parse, rest))
#     results = [dict(tuple(group.split('=')) for group in line.split(',') if group) for line in rest]
    df = pd.DataFrame(results).applymap(lambda x: x if x != 'None' else np.NaN)
    if 'model_name' not in df.columns:
        df['model_name'], df['start_date'] = model_name, start_date

    columns = list(df.columns)
    df = df[columns[-2:] + columns[:-2]]
    return infer_objects(df)


def get_results(directory='.'):
    args = 'find {} -type f -name results.txt'.format(directory).split()
    result_files = sh(*args).strip().split('\n')
    results = pd.DataFrame()
    for result_file in result_files:
        df = parse_results(result_file)
        results = pd.concat([results, df], axis=0).reset_index(drop=True)

    results.to_csv(p.join(directory, 'all_results.csv'), index=False)


def merge_columns(df, col1_name, col2_name):
    # col1 is the final name of the column
    if all(col in df.columns for col in (col1_name, col2_name)):
        col2 = df[col2_name].rename(col1_name)
        mask = df[col1_name].notnull()
        conflicts = mask & (df[col2_name].notnull())
        assert not conflicts.any(), "You have conflicts: {}".format(df.loc[conflicts, [col1_name, col2_name]])
        df.loc[~mask, col1_name] = df.loc[~mask, col2_name]
        df = df.drop(col2_name, axis=1)
    elif col2_name in df.columns:
        df = df.rename(columns={col2_name: col1_name})

    return df


def postprocessing(filename='all_results.txt', data=None):
    if not p.exists(filename):
        get_results(p.dirname(filename))

    if data is None:
        df = pd.read_csv(filename)
    else:
        assert isinstance(data, pd.DataFrame)
        df = data.copy()

    # Convert start_date to datetime
    df['start_date'] = pd.to_datetime(df['start_date'])

    # Fill missing dates
    missing_dates = df['start_date'].isnull()
    if missing_dates.any():
        print("Missing dates: {}".format(missing_dates.sum()))
        embed()

    # Merge epc and examples_per_class
    df = merge_columns(df, 'examples_per_class', 'epc')
    df['examples_per_class'] = df['examples_per_class'].astype('int')

    if 'grid_search' not in filename:
        return postprocess_new(df)
    else:
        return postprocess_old(df)


def postprocess_new(df):
    columns = list(df.columns)
    columns = [col if not col.startswith('train_') else col.replace('train_', '') for col in columns]
    columns = [col if not col.startswith('test_') else col.replace('test_', 'test_') for col in columns]
    df.columns = columns
    if ('model_name' in df.columns
            and df['model_name'].nunique() == 1
            and df.loc[0, 'model_name'] == 'baseline'):
        df = df.drop('model_name', axis=1)
        df = df.rename(columns={'model': 'model_name'})

    return df


def postprocess_old(df):
    # Merge final_score and f1_score
    df = merge_columns(df, TARGET, 'final_score')

    # Fill missing encoder_dims, convert to list and extract depth and encoding_dim
    df.loc[(df.model_name.str.endswith('512+128d')) & (df.encoder_dims.isnull()), 'encoder_dims'] = '[512, 128]'
    df.loc[(df.model_name.str.endswith('128d')) & (df.encoder_dims.isnull()), 'encoder_dims'] = '[128]'
    df.loc[(df.model_name == 'cae') & (df.encoder_dims.isnull()), 'encoder_dims'] = '[128]'
    df['encoder_layers'] = df['encoder_dims'].map(lambda s: eval(s) if type(s) == str else None)
    df['encoding_dim'] = df['encoder_layers'].map(lambda l: l[-1] if type(l) == list else None)
    df['depth'] = df['encoder_layers'].map(lambda l: len(l) if type(l) == list else 0)
    df = df.drop(['encoder_dims', 'encoder_layers'], axis=1)

    # Fill missing competitive_topk and contractive
    missing_topk = lambda df: df.competitive_topk.isnull()
    missing_contr = lambda df: df.contractive.isnull()
    df.loc[missing_topk(df) & (df.model_name == 'ksparse-ae+clf-128d'), 'competitive_topk'] = 32
    df.loc[missing_topk(df) & missing_contr(df) & df.model_name.str.contains('k'), ['competitive_topk', 'contractive']] = (32, 0.01)
#    df.loc[(df['model_name'] == 'ksparse-ae+clf-128d') & (df['start_date'] > pd.Timestamp(2019,3,25)), 'competitive_topk'] = 32
#    df.loc[(df.model_name.str.contains('k')) & (df.competitive_topk.isnull()), ['competitive_topk', 'contractive']] = 32, 0.01
    df.loc[missing_topk(df), 'competitive_topk'] = 0
    df.loc[missing_contr(df), 'contractive'] = 0.
    df.loc[(~df.model_name.str.contains('contr')) & (missing_contr(df)), 'contractive'] = 0.

    # Fill missing penalty and penalty_weight
    df = df.assign(l1=0., l2=0.)
    l1_mask = (df.penalty == 'l1') & (df.penalty_weight.notnull())
    df.loc[l1_mask, 'l1'] = df.loc[l1_mask, 'penalty_weight']
    l2_mask = (df.penalty == 'l2') & (df.penalty_weight.notnull())
    df.loc[l2_mask, 'l2'] = df.loc[l2_mask, 'penalty_weight']
    l1l2_mask = (df.penalty == 'elasticnet') & (df.penalty_weight.notnull())
    if l1l2_mask.any():
        df.loc[l1l2_mask, ['l1', 'l2']] = [*df.loc[l1l2_mask, 'penalty_weight'].map(eval)]
    df['l1'] = df['l1'].astype('float')
    df['l2'] = df['l2'].astype('float')
    df = df.drop(['penalty', 'penalty_weight'], axis=1)

    # Fill missing amplifications
    if 'amplification' in df.columns:
        df.loc[df.model_name == 'kate', 'amplification'] = df.loc[df.model_name == 'kate', 'amplification'].fillna(1.)
        df['amplification'] = df['amplification'].fillna(0.)

    # Fill missing dropout rates
    if 'dropout_rate' in df.columns:
        df.loc[df.dropout_rate.isnull(), 'dropout_rate'] = 0.

    # Rename the models
    mask = df.model_name.str.contains('\+')
    df.loc[mask, 'model_name'] = df.loc[mask, 'model_name'].map(lambda s: s.split('+')[0])

    df['model_name'] = df['model_name'].replace({'ksparse-ae': 'ksparse',
                                                 'contr-ae': 'cae',
                                                 'contr-ksparse-ae': 'ksparse',
                                                 'ae': 'ssae', 'tied-ae': 'ssae'})
    # Fix wrong model names
    df.loc[(df.model_name == 'cae') & (df.contractive == 0), 'model_name'] = "ssae"

    return df


def prepare_for_predictions(df, features, target, dummies=None, log10=None, log2=None, scale=None):

    for col_group in [features, [target], dummies, log10, log2, scale]:
        if col_group:
            assert all(col in df.columns for col in col_group)

    df = df.copy()

    df = df[features + [target]]

    # Transform categorical data to dummies
    if dummies:
        for col in dummies:
            df = df.join(pd.get_dummies(df[col])).drop(col, axis=1)

    def safe_log_transform(b, epsilon=1e-7):
        return lambda x: np.log(x+epsilon) / np.log(b)

    # Log-transform exponential data
    if log10:
        df[log10] = df[log10].applymap(safe_log_transform(10))

    if log2:
        df[log2] = df[log2].applymap(safe_log_transform(2))

    scale = list(df.columns) if scale == 'all' else scale

    if scale:
        df[scale] = Scaler().fit_transform(df[scale].astype('float'))

    df = df.loc[:, df.apply(lambda col: col.nunique() > 1, axis=0)]

    if df.isnull().any().any() or (df.dtypes == 'object').any():
        df.info()
        embed()

    return df.drop(target, axis=1), df[target]


def make_analysis(df, mode='stats', print_results=True, target=TARGET):

    dummies = ['model_name']
    features = [col for col in HYPERPARAMETERS + VARIABLES[:-1] if col in df.columns]
    log10 = ['contractive', 'examples_per_class']
    log2 = ['competitive_topk', 'encoding_dim']
    scale = ['alpha', 'encoding_dim', 'examples_per_class', 'contractive']

    X, y = prepare_for_predictions(df, features, target, dummies, log10, log2, scale)

    if mode == 'ml':
        model = LinearRegression()
        model.fit(X, y)
        if print_results: print("R-squared: {:.4f}".format(model.score(X, y)))
        results = pd.DataFrame(model.coef_, index=X.columns, columns=['coef'])
        if print_results: display(results.sort_values('coef', ascending=False))
    elif mode == 'stats':
        model = sm.OLS(y, X)
        results = model.fit()
        if print_results: print(results.summary())
    elif mode == 'kbest':
        model = SelectKBest(f_regression)
        model.fit(X, y)
        results = pd.DataFrame(np.stack([model.scores_, model.pvalues_], axis=1), index=X.columns, columns=['score', 'p_value'])
        if print_results: display(results.sort_values('score', ascending=False))
    else:
        raise NotImplementedError(mode)
    return model, results


def load_weights(filename, layer_name):
    with h5py.File(filename, 'r') as f:
        return f[layer_name][layer_name]['kernel:0'][()]


def pairwise_cosine(matrix):
    matrix = matrix / np.linalg.norm(matrix)
    n = matrix.shape[1]
    scores = np.empty(n)
    for i in range(n):
        for j in range(i + 1, n):
            scores[i] = np.arccos(matrix[:, i].dot(matrix[:, j]))

    return scores.mean(), scores.std()


def get_best_hyperparameters(df, variables, criteria=TARGET, agg='mean', high=True, all_alphas=False):
    hyperparams = [param for param in HYPERPARAMETERS if param in df.columns and param not in variables]
    groups = variables + hyperparams
    if not all_alphas:
        df = df.loc[((df.alpha > 0.) & (df.alpha < 1.)) | (df.model_name == 'classifier')]
    scores = df[groups + [criteria]].groupby(groups).agg(agg).reset_index()
    sort_column = criteria if type(agg) != list else (criteria, agg[0])
    best = scores.sort_values(sort_column, ascending=not high).groupby(variables).head(1)
    return best.sort_values(variables).reset_index(drop=True)


def filter_params(df, params):
    while isinstance(params.columns, pd.MultiIndex):
        try:
            params.columns = params.columns.droplevel(-1)
        except Exception as err:
            print("Problem with MultiIndex", err, params.columns)
            import ipdb; ipdb.set_trace()
    keys = list(params.columns)
    return df[df.set_index(keys).index.isin(params.set_index(keys).index)].reset_index()


def best_params_to_latex(best_params):
    best_params = best_params.drop(('depth', ''), axis=1)

    levels = [[s.replace('_', ' ') for s in level] for level in best_params.columns.levels]
    levels[0][-2] = 'labels/class'
    levels[0][-4] = 'top k'
    levels[0][-1] = 'model'
    best_params.columns = best_params.columns.set_levels(levels)

    for param_name in ['top k', 'contractive', 'l1', 'l2']:
        fmt = '{:.0f}' if param_name == 'top k' else '{:.0e}'
        best_params[(param_name, '')] = best_params[(param_name, '')].map(lambda x: x or np.NaN).map(fmt.format)

    best_params[('f1 score', 'mean')] = best_params[('f1 score', 'mean')].map('{:.1%}'.format)
    best_params[('f1 score', 'std')] = best_params[('f1 score', 'std')].map('{:.0e}'.format)
    best_params = best_params.replace({'nan': ''})

    best_params = best_params.set_index(['model', 'labels/class'])

    return best_params.to_latex()


def plot_or_save(filename):
    if filename:
        plt.tight_layout()
        plt.savefig(p.join(MT_FIGURES_FOLDER, filename)) or plt.close()
    else:
        plt.show()


def epc_vs_target(df, target=TARGET, filename=None):
    xlabel = 'examples_per_class'
    ylabel = target
    zlabel = 'model_name'
    columns = [xlabel, zlabel, ylabel]
    assert all(label in df.columns for label in columns)
    results = df[columns].groupby(columns[:-1]).agg(['mean', 'min', 'max']).unstack(level=zlabel)
    results.columns = results.columns.droplevel(0).swaplevel()

    fig, ax = plt.subplots(1, 1, figsize=(9,8))
    for z in results.columns.levels[0]:
        this_results = results.xs(z, axis=1, level=0).interpolate()
        x, y = list(this_results.index), this_results['mean']
        ymin, ymax = this_results['min'], this_results['max']
        ax.plot(x, y, label=z)
        ax.fill_between(x, ymin, ymax, alpha=0.3)

    plt.xscale('log')
    ax.set_xticklabels(['', '', 1, 10, 100, 1000])
    plt.legend()
    plt.xlabel(xlabel.replace('_', ' '))
    plt.ylabel(ylabel.replace('_', ' ') + ' (test set)')

    plot_or_save(filename)


def compare_fc_ae(filename=None):
    fc_results = postprocessing('baseline_results/all_results.csv').where(lambda df: df.model_name == 'fc_classifier')
    gs_results = postprocessing('grid_search_results/all_results.csv')
    best_params = get_best_hyperparameters(gs_results, VARIABLES[:-1])
    best_results = filter_params(gs_results, best_params.drop(TARGET, axis=1))
    epc_vs_target(pd.concat([best_results, fc_results], axis=0, sort=False), filename=filename)


def plot_epc_vs_target(results, target, filename=None):

    results = results.rename(columns={'model_name': 'model'})

    ax = sns.barplot(x='examples_per_class',
                     y=target,
                     hue='model',
                     data=results,
                     palette='Blues',
                     saturation=0.6,
                     errwidth=1.2,
                     errcolor='#3d424b',
                     capsize=0.05)

    ax.set_xlabel('examples per class'); ax.set_ylabel(target.replace('_', ' '))
    plt.gcf().set_size_inches((10, 8))

    plot_or_save(filename)

def alpha_effect_plot(filename=None):
    df = postprocessing('grid_search_results/all_results.csv')

    best_params = get_best_hyperparameters(df, VARIABLES[:-1] + ['alpha'], all_alphas=True)
    best_results = filter_params(df, best_params.drop(TARGET, axis=1))

    uns = best_results[best_results.alpha == 0]
    best_results = best_results[best_results.alpha != 0]

    fig, axs = plt.subplots(2, 2, figsize=(10, 8), sharex=True, sharey=True)
    for i, model in enumerate(['kate', 'ksparse']):
        for j, metric in enumerate(['f1_score', 'cluster_acc']):

            ax = axs[j, i]
            data = best_results[best_results.model_name == model]
            mean, ymin, ymax = uns.loc[uns.model_name == model, metric].apply(['mean', 'min', 'max']).values.ravel()
            sns.swarmplot(x='examples_per_class', y=metric, hue='alpha', data=data, ax=ax)
            ax.axhline(mean, *ax.get_xlim(), zorder=0, label='unsupervised')
            ax.fill_between(ax.get_xlim(), [ymin] * 2, [ymax] * 2, alpha=0.2, zorder=0)

            if j == 0:
                ax.set_title('Semi-supervised {}'.format(model))
                ax.set_xlabel('')
            if i == 1:
                ax.set_ylabel('')
            if (i, j) != (0, 0):
                ax.get_legend().remove()

    #axs[0,0].legend(prop={'size': 13})
    plot_or_save(filename)


def plot_unsupervised_scores(scores, filename=None):
    scores = scores.copy()

    metrics = ['test_SVM_f1_score', 'test_cluster_acc']
    models = sorted(scores.model_name.unique())
    palette = sns.color_palette('tab10', n_colors=len(models))
    color_mapping = {m: palette[i] for i, m in enumerate(models)}
    colors = scores['model_name'].map(color_mapping)
    scores.loc[scores.unsupervised, 'examples_per_class'] = 5
    def get_size(epc): return (np.log10(epc) + 1) * 20

    fig, ax = plt.subplots(1, 1, figsize=(12, 8))
    points_collection = [
        ax.scatter(scores.loc[scores.model_name == model, metrics[0]],
                   scores.loc[scores.model_name == model, metrics[1]],
                   color=color_mapping[model],
                   alpha=0.8,
                   s=scores.loc[scores.model_name == model, 'examples_per_class'].map(get_size),
                   label=model)
        for model in models
    ]

    model_legend = plt.legend(handles=points_collection,
                              #frameon=False,
                              ncol=2,
                              bbox_to_anchor=(0.18, 1),
                              loc=2,
                              title='Models')


    ax.add_artist(model_legend)

    sizes_collection = [
        ax.scatter([], [], c='k', alpha=0.3, s=get_size(epc), label=str(epc))
        for epc in [1, 10, 100, 1000]
    ]

    size_legend = plt.legend(handles=sizes_collection,
                             scatterpoints=1,
                             #frameon=False,
                             loc=2,
                             #labelspacing=1,
                             title='Labels/class')

    for label, func in zip(metrics, [ax.set_xlabel, ax.set_ylabel]):
        func(label.replace('_', ' '))

    plot_or_save(filename)


def make_all_plots(save=False):
    #get_results('grid_search_results')
    #get_results('baseline_results')

    # Compare models test f1 scores
    # compare_fc_ae('f1score_comparison.pdf' if save else None)
    final_results = load_final_scores("final_eval.csv")
    supervised_scores = final_results[~final_results.unsupervised].copy()
    plot_epc_vs_target(supervised_scores, 'test_f1_score', 'f1score_comparison.pdf' if save else None)

    # Plot effect of alpha
    # alpha_effect_plot('alpha_effect.eps' if save else None)

    # Compare models svm and kmeans scores
    unsupervised_scores = final_results[final_results.model_name != 'fc_classifier']
    plot_unsupervised_scores(unsupervised_scores, 'unsup_scores.pdf' if save else None)


def visuals(df):

    # Score distribution
    f1 = plt.figure()
    sns.violinplot(x='examples_per_class', y=TARGET, data=df, ax=f1.add_subplot())

    # EPC vs ALPHA VS F1
    f2 = plt.figure()
    sns.boxplot(x='examples_per_class', y=TARGET, hue='alpha', data=df, ax=f2.add_subplot())

    plt.show()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--target', type=str)
    parser.add_argument('--mode', choices=['generate', 'transform'])
    args = parser.parse_args()

    if args.mode:
        if args.mode == 'generate':
            get_results(args.target)
        else:
            df = postprocessing(args.target)
            df.info()


