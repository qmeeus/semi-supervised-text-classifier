import ipywidgets as widgets
from IPython.display import display
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from pprint import pprint
import datetime as dt
import os.path as p
import warnings

from options import MODEL_VARIABLES, HYPERPARAMETERS, METRICS, DATE_PARAM

from data import (get_result_files,
                  get_eval_files,
                  load_gs_results,
                  load_eval_results,
                  filter_dataframe,
                  get_list_of_files,
                  get_best_hyperparameters,
                  filter_params)


pd.set_option('display.float_format', '{:.4f}'.format)

ALL = 'ALL'
MODES = [GS_MODE, EVAL_MODE] = 'Gridsearch', 'Evaluation'
GS_DIR = 'grid_search_results'
EVAL_DIR = 'eval_results'
LEVELS = ['load', 'gridsearch', 'evaluation']
AGGFUNCS = ['mean', 'std', 'count', 'min', 'max']
MAX_WIDGETS_PER_ROW = 3
MAX_METRICS = 3

warnings.filterwarnings("ignore")


class Dashboard:

    def __init__(self):
        self.mode = None
        self.files_selection = []
        self.data, self._data = None, None
        self.widgets, self.outputs = {}, {}
        self.layout = None
        self.build_widgets()
        self.make_layout()

    def build_widgets(self):

        # --------------------------  TOP LEVEL  --------------------------

        mode_selector = widgets.RadioButtons(
            options=MODES, value=None)
        model_selector = widgets.SelectMultiple(options=[], value=[])
        date_selector = widgets.SelectMultiple(options=[], value=[])
        file_selector = widgets.Dropdown(options=[], value=None)
        load_button = widgets.Button(description='Load', button_style='success')
        save_button = widgets.Checkbox(description='Save', value=False)
        latest_button = widgets.Checkbox(description='Load latest', value=True)

        self.widgets[LEVELS[0]] = {'mode': mode_selector,
                                   'model': model_selector,
                                   'date': date_selector,
                                   'file': file_selector,
                                   'save': save_button,
                                   'latest': latest_button,
                                   'load_button': load_button}

        self.outputs[LEVELS[0]] = {'text': widgets.Output()}

        # --------------------------  GS LEVEL  --------------------------
        gs_widgets = {}
        for name, value in dict(**MODEL_VARIABLES, **HYPERPARAMETERS).items():
            selector = widgets.SelectMultiple(options=value)
            gs_widgets[name] = selector

        gs_widgets['metrics'] = widgets.SelectMultiple(options=METRICS)

        gs_widgets['display_cols'] = widgets.SelectMultiple(
            options=list(HYPERPARAMETERS))

        gs_widgets['agg'] = widgets.Checkbox(value=True, description='Aggregate')
        gs_widgets['best'] = widgets.Checkbox(value=False, description='Best')
        gs_widgets['save'] = widgets.Button(description='Save', button_style='success', disabled=True)
        gs_widgets['aggfunc'] = widgets.SelectMultiple(
            options=AGGFUNCS, value=AGGFUNCS[:2])

        self.widgets[LEVELS[1]] = gs_widgets

        self.outputs[LEVELS[1]] = {'table': widgets.Output(),
                                   'plot': widgets.Output()}

        # --------------------------  EVAL LEVEL  --------------------------
        eval_widgets = {}
        for name, value in dict(**MODEL_VARIABLES).items():
            selector = widgets.SelectMultiple(options=value)
            eval_widgets[name] = selector

        eval_widgets['dataset'] = widgets.RadioButtons(options=['test', 'val'], value='test')
        eval_widgets['metrics'] = widgets.SelectMultiple(options=METRICS)

        eval_widgets['agg'] = widgets.Checkbox(value=True, description='Aggregate')

        eval_widgets['aggfunc'] = widgets.SelectMultiple(
            options=AGGFUNCS, value=AGGFUNCS[:2])

        self.widgets[LEVELS[2]] = eval_widgets

        self.outputs[LEVELS[2]] = {'table': widgets.Output(),
                                   'plot': widgets.Output()}

        # -------------------------  EVENT HANDLERS  ------------------------

        mode_event_handler = self._switch_mode_event_handler(
            model=model_selector, date=date_selector, file=file_selector)
        mode_selector.observe(mode_event_handler, names='value')

        model_dropdown_event_handler = self._model_dropdown_event_handler(
            date=date_selector)
        model_selector.observe(model_dropdown_event_handler, names='value')

        latest_switch_event_handler = self._latest_switch_event_handler(
            date=date_selector)
        latest_button.observe(latest_switch_event_handler, names='value')

        load_event_handler = self._load_button_event_handler(
            mode=mode_selector, model=model_selector, date=date_selector,
            file=file_selector, save=save_button, latest=latest_button)
        load_button.on_click(load_event_handler)

        gs_widgets['best'].observe(
            lambda ch: setattr(gs_widgets['save'], 'disabled', not ch.new),
            'value')

        for level in LEVELS[1:]:
            for name, widget in self.widgets[level].items():
                linked = dict(filter(lambda t: t[0] not in (name, 'save'),
                                     self.widgets[level].items()))
                event_handler = self._make_common_filter_handler(
                    level, **linked)
                widget.observe(event_handler, names='value')

    def make_layout(self):

        layout = widgets.Layout(margin='10px 5px 10px 5px')

        # --------------------------  TOP LEVEL  --------------------------

        checkboxes = widgets.VBox(
            [self.widgets[LEVELS[0]][key] for key in ['save', 'latest']],
            layout=layout)

        top_options = widgets.HBox([
            self.format_widget(self.widgets[LEVELS[0]]['mode'], 'mode'),
            checkboxes], layout=layout)

        load_section = widgets.VBox([
            top_options,
            widgets.HBox([
                self.format_widget(self.widgets[LEVELS[0]][wn], wn, layout=layout)
                for wn in ['file', 'model', 'date']
            ]),
            self.widgets[LEVELS[0]]['load_button'],
            self.outputs[LEVELS[0]]['text']
        ])

        # --------------------------  GS LEVEL  --------------------------

        widgets_dict = self.widgets[LEVELS[1]]

        selectors = []
        for option_list in [MODEL_VARIABLES, HYPERPARAMETERS]:
            options = [[]]
            for i, opt_name in enumerate(option_list):
                widget = widgets_dict[opt_name]
                options[-1].append(self.format_widget(widget, opt_name, layout=layout))

                full_capacity = len(options[-1]) == MAX_WIDGETS_PER_ROW
                last_element = len(option_list) == i + 1
                if full_capacity or last_element:
                    options[-1] = widgets.HBox(options[-1], layout=layout)
                    if not last_element:
                        options.append([])
                    else:
                        selectors.append(options)

        var_opts, hp_opts = selectors

        checkboxes = widgets.VBox(
            [widgets_dict[key] for key in ['agg', 'best', 'save']])

        display_opts = [widgets.HBox([checkboxes, widgets_dict['aggfunc']]),
                        widgets.HBox([widgets_dict['display_cols'],
                                      widgets_dict['metrics']])]

        options_container = widgets.Accordion([
            widgets.VBox(var_opts, layout=layout),
            widgets.VBox(hp_opts, layout=layout),
            widgets.VBox(display_opts, layout=layout)
        ])

        options_container.set_title(0, 'Models')
        options_container.set_title(1, 'Hyperparameters')
        options_container.set_title(2, 'Display')

        output_container = widgets.Tab(list(self.outputs[LEVELS[1]].values()), layout=layout)
        output_container.set_title(0, 'Dataset Exploration')
        output_container.set_title(1, 'Plot')

        gs_section = widgets.VBox([options_container, output_container],
                                  layout=layout)

        # --------------------------  EVAL LEVEL  --------------------------

        widgets_dict = self.widgets[LEVELS[2]]
        options = [[]]
        for i, opt_name in enumerate(list(MODEL_VARIABLES)):
            widget = widgets_dict[opt_name]
            options[-1].append(self.format_widget(widget, opt_name, layout=layout))

            full_capacity = len(options[-1]) == MAX_WIDGETS_PER_ROW
            last_element = len(MODEL_VARIABLES) == i + 1
            if full_capacity or last_element:
                options[-1] = widgets.HBox(options[-1], layout=layout)
                if not last_element:
                    options.append([])

        checkboxes = widgets.HBox([widgets_dict['dataset'], widgets_dict['agg']])

        display_opts = [widgets.HBox([checkboxes, widgets_dict['aggfunc']]),
                        widgets.HBox([widgets_dict['metrics']])]

        options_container = widgets.Accordion([
            widgets.VBox(options, layout=layout),
            widgets.VBox(display_opts, layout=layout)
        ])

        options_container.set_title(0, 'Models')
        options_container.set_title(1, 'Display')

        output_container = widgets.Tab(list(self.outputs[LEVELS[2]].values()), layout=layout)
        output_container.set_title(0, 'Dataset Exploration')
        output_container.set_title(1, 'Plot')

        eval_section = widgets.VBox([options_container, output_container],
                                  layout=layout)

        sections = [load_section, gs_section, eval_section]

        self.layout = widgets.Accordion(sections, layout=layout)
        self.layout.set_title(0, 'Load Data')
        self.layout.set_title(1, 'Grid Search')
        self.layout.set_title(2, 'Evaluation')
        display(self.layout)

    def _switch_mode_event_handler(self, **linked):

        def switch_mode_event_handler(change):
            linked_widgets = linked.copy()
            self.mode = change.new
            file_dropdown = linked_widgets.pop('file')
            if self.mode == GS_MODE:
                file_dropdown.options = [''] + get_list_of_files(GS_DIR, prefix='gs_results')
                self.files_selection = get_result_files()
                linked_widgets['model'].disabled = False
                for key, value in linked_widgets.items():
                    options = sorted(self.files_selection[key].unique())
                    if key == 'date':
                        options = self.format_dates(options)
                    value.options = options

            else:
                file_dropdown.options = [''] + get_list_of_files(EVAL_DIR, prefix='eval_results')
                self.files_selection = get_eval_files()
                linked_widgets['model'].disabled = True
                options = sorted(self.files_selection['date'].unique())
                linked_widgets['date'].options = self.format_dates(options)

        return switch_mode_event_handler

    def _make_common_filter_handler(self, level, **linked):

        widgets_dict = self.widgets[level]
        update_func = self._update_gs_outputs if level == LEVELS[1] else self._update_eval_outputs

        #if not (len(linked) == len(widgets_dict) - 1
        #        and any(k in linked for k in widgets_dict)):
        #    raise TypeError("Invalid argument")

        def dropdown_evenhandler(change):
            filters = {name: widget.value for name, widget in linked.items() if name != 'save'}
            widget_name = [n for n in list(widgets_dict) if n not in filters][0]
            filters[widget_name] = change.new
            self.widgets[LEVELS[1]]['best'].disabled = not filters['agg']
            print(list(filters))
            print(widget_name)
            print(filters['aggfunc'])
            update_func(**filters)

        return dropdown_evenhandler

    def _model_dropdown_event_handler(self, date):

        def model_dropdown_event_handler(change):
            df = self.files_selection.copy()
            if not change.new:
                return
            options = sorted(df.loc[df['model'].isin(change.new), 'date'].unique())
            date.options = self.format_dates(options)

        return model_dropdown_event_handler

    def _latest_switch_event_handler(self, date):

        def latest_switch_event_handler(change):
            date.disabled = change.new

        return latest_switch_event_handler

    def _load_button_event_handler(self, mode, model, date, file, save, latest):

        def load_button_event_handler(obj):
            self.clear_outputs(LEVELS[0])
            if file.value:
                target = file.value
            else:
                files = self.files_selection.copy()
                selected_keys, selected_filters = ['model'], [model.value]
                if not latest.value:
                    selected_keys += ['date']
                    selected_filters += [pd.to_datetime(date.value)]
                for name, filters in zip(selected_keys, selected_filters):
                    if len(filters):
                        files = files[files[name].isin(filters)]
                if latest.value:
                    files = files.sort_values('date').groupby('model').tail(1)
                target = files['path']

            with self.outputs[LEVELS[0]]['text']:
                display(widgets.HTML('<h4>Loading {} files:</h4>'.format(len(target))))
                items = ['<li>{}</li>'.format(path) for path in target.values]
                display(widgets.HTML('<ul>' + ''.join(items) + '</ul>'))

            self._load_data(target, mode=mode.value, save=save.value)
            self.layout.selected_index = 1 if mode.value == GS_MODE else 2

        return load_button_event_handler

    def _save_button_event_handler(self):

        def save_button_event_handler(self, obj):
            fn = "best_params_{%Y%m%d_%H%M}.csv".format(dt.datetime.today())
            path = p.join(GS_DIR, fn)
            if isinstance(self._data, pd.DataFrame) and len(self._data):
                self._data.to_csv(path, index=False)
                log = "Saving best params to {}".format(path)
            else:
                log = "An error occured while saving {}".format(path)

            with self.outputs[LEVELS[0]]['text']:
                display(widgets.HTML('<p>{}</p>'.format(log)))

        return save_button_event_handler

    def _load_data(self, target, mode=GS_MODE, save=False):
        if type(target) == str:
            directory = GS_DIR if mode == GS_MODE else EVAL_DIR
            self.data = pd.read_csv(p.join(directory, target))
            return

        load_func = load_gs_results if mode == GS_MODE else load_eval_results
        self.data = load_func(target, save=save)

    def _update_eval_outputs(self,
                             agg=True,
                             metrics=None,
                             aggfunc=None,
                             dataset='test',
                             **filters):
        self.clear_outputs(LEVELS[2])
        df = self.data.loc[:, pd.IndexSlice[:, ['', dataset]]].copy()
        df.columns = df.columns.droplevel(-1)

        df = filter_dataframe(df, **filters)

        if metrics:
            metrics = [col for col in df.columns if col in metrics]
        else:
            metrics = [col for col in df.columns if col in METRICS]

        df = df.sort_values(list(MODEL_VARIABLES) + metrics).dropna(how='all', axis=1)
        metrics = [col for col in metrics if col in df]
        display_cols = list(MODEL_VARIABLES)

        with self.outputs[LEVELS[2]]['table']:
            table = df.copy()
            if not len(table):
                display(self.no_results())
                return

            if agg:
                table = table.groupby(display_cols).agg(aggfunc).reset_index()

            self._data = table

            if len(table) > 0:
                display(table[display_cols + metrics])
            else:
                display(self.no_results())

        with self.outputs[LEVELS[2]]['plot']:
            metrics = metrics if len(metrics) < MAX_METRICS else metrics[:MAX_METRICS]
            if isinstance(df.columns, pd.MultiIndex):
                df = df.xs(aggfunc[0], axis=1, level=1)
            fig, ax = plt.subplots(1, 1, figsize=(12,8))

            data = filter_params(df, table[display_cols])
            sns.barplot(x=list(MODEL_VARIABLES)[1],
                        y=metrics[0],
                        hue=list(MODEL_VARIABLES)[0],
                        data=data,
                        palette='Blues',
                        saturation=0.6,
                        errwidth=1.2,
                        errcolor='#3d424b',
                        capsize=0.05,
                        ax=ax)

            plt.show()

    def _update_gs_outputs(self,
                           agg=True,
                           best=False,
                           metrics=None,
                           aggfunc=None,
                           display_cols=None,
                           **filters):

        self.clear_outputs(LEVELS[1])
        df = filter_dataframe(self.data, **filters)

        if display_cols:
            display_cols = [col for col in df.columns if col in display_cols]
        else:
            display_cols = list(MODEL_VARIABLES)

        for col in reversed(list(MODEL_VARIABLES)):
            if not col in display_cols:
                display_cols.insert(0, col)

        if metrics:
            metrics = [col for col in df.columns if col in metrics]
        else:
            metrics = [col for col in df.columns if col in METRICS]

        df = df.sort_values(list(MODEL_VARIABLES) + metrics).dropna(how='all', axis=1)

        display_cols = [col for col in display_cols if col in df]
        metrics = [col for col in metrics if col in df]

        with self.outputs[LEVELS[1]]['table']:
            table = df.copy()
            if not len(table):
                display(self.no_results())
                return

            sort_cols = list(MODEL_VARIABLES) + [metrics[0]]
            sort_order = [True] * len(MODEL_VARIABLES) + [False]
            if agg and len(aggfunc) > 1:
                sort_cols = [(col, '') for col in sort_cols]
                sort_cols[-1] = (sort_cols[-1][0], aggfunc[0])

            if agg:
                table = table.groupby(display_cols).agg(aggfunc).reset_index()

                if best:
                    table = (table.sort_values(sort_cols, ascending=sort_order)
                             .groupby(list(MODEL_VARIABLES)).head(1))

            self._data = table

            if len(table) > 0:
                display(table[display_cols + metrics].sort_values(sort_cols, ascending=sort_order))
            else:
                display(self.no_results())

        with self.outputs[LEVELS[1]]['plot']:
            metrics = metrics if len(metrics) < MAX_METRICS else metrics[:MAX_METRICS]
            if isinstance(df.columns, pd.MultiIndex):
                df = df.xs(aggfunc[0], axis=1, level=1)
            fig, ax = plt.subplots(1, 1, figsize=(12,8))

            if agg:
                data = filter_params(df, table[display_cols])
                sns.barplot(x=list(MODEL_VARIABLES)[1],
                            y=metrics[0],
                            hue=list(MODEL_VARIABLES)[0],
                            data=data,
                            palette='Blues',
                            saturation=0.6,
                            errwidth=1.2,
                            errcolor='#3d424b',
                            capsize=0.05,
                            ax=ax)

            else:
                for metric in metrics:
                    sns.kdeplot(df[metric], shade=True, ax=ax)

            plt.show()

    def clear_outputs(self, level=LEVELS[1]):
        assert level in self.outputs
        for output in self.outputs[level].values():
            output.clear_output()

    @staticmethod
    def format_dates(array):
        def format_date(ts):
            ts = str(ts)[:16]
            return ts.replace('T', ' ') if ts[-5:] != '00:00' else ts[:10]
        return list(map(format_date, array))

    @staticmethod
    def format_widget(widget, name, horizontal=False, layout=None):
        Box = widgets.HBox if horizontal else widgets.VBox
        descriptor = widgets.HTML("<b>{}:</b>".format(name.replace('_', ' ').capitalize()))
        args = {'layout': layout} if layout else {}
        return Box([descriptor, widget], **args)

    @staticmethod
    def no_results():
        return widgets.HTML('<b>No Results !</b>')























#
