import ipywidgets as widgets
from IPython.display import display
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from pprint import pprint

from options import MODEL_VARIABLES, HYPERPARAMETERS, METRICS, DATE_PARAM

ALL = 'ALL'
MAX_WIDGETS_PER_ROW = 3


class Dashboard:

    widget_names = [*MODEL_VARIABLES, *HYPERPARAMETERS, 'threshold', 'metric']

    def __init__(self):
        self.data = self.load_data()
        self.hyperparameters = [param for param in HYPERPARAMETERS
                                if param in self.data.columns]

        if not self.hyperparameters:
            raise TypeError("#nofilter")

        self.widgets, self.outputs = {}, {}
        self.container = None
        self.build_widgets()
        self.make_layout()

    def build_widgets(self):

        for name in MODEL_VARIABLES + self.hyperparameters:
            opts = sorted(self.data[name].dropna().unique())
            dropdown = widgets.SelectMultiple(options=opts, values=opts)
            self.widgets[name] = dropdown

        for i in (0, 1):
            widget_name = ('display_' if i else '') + 'metrics'
            self.widgets[widget_name] = widgets.SelectMultiple(
                options=METRICS,
                values=METRICS)

        display_cols_opts = [col for col in self.data if col not in METRICS]
        self.widgets['display_cols'] = widgets.SelectMultiple(
            options=display_cols_opts,
            values=display_cols_opts)

        self.widgets['threshold'] = widgets.BoundedFloatText(
            min=0, max=1, value=.5, step=.1, description='Threshold:')

        self.widgets['aggregate'] = widgets.Checkbox(value=True,
                                                     description='Aggregate')

        self.outputs['table'] = widgets.Output()
        self.outputs['plot'] = widgets.Output()

        for name, dropdown in self.widgets.items():
            event_handler = self._make_common_filter_handler(
                **dict(filter(lambda t: t[0] != name, self.widgets.items())))
            dropdown.observe(event_handler, names='value')

    def make_layout(self):

        layout = widgets.Layout(margin='0 0 50px 0')

        dropdowns = []
        for option_list in [MODEL_VARIABLES, self.hyperparameters]:
            options = [[]]
            for i, opt_name in enumerate(option_list):
                widget = self.widgets[opt_name]
                options[-1].append(widgets.VBox(
                    [self.format_title(opt_name), widget],
                    layout=layout))

                full_capacity = len(options[-1]) == MAX_WIDGETS_PER_ROW
                last_element = len(option_list) == i + 1
                if full_capacity or last_element:
                    options[-1] = widgets.HBox(options[-1], layout=layout)
                    if not last_element:
                        options.append([])
                    else:
                        dropdowns.append(options)

        var_opts, hp_opts = dropdowns

        display_opts = [widgets.HBox([self.widgets['threshold'],
                                      self.widgets['aggregate']]),
                        widgets.HBox([self.widgets['display_cols'],
                                      self.widgets['display_metrics']])]

        options_container = widgets.Accordion([
            widgets.VBox(var_opts, layout=layout),
            widgets.VBox(hp_opts, layout=layout),
            self.widgets['metrics'],
            widgets.VBox(display_opts, layout=layout)
        ])

        options_container.set_title(0, 'Models')
        options_container.set_title(1, 'Hyperparameters')
        options_container.set_title(2, 'Metrics')
        options_container.set_title(3, 'Display')

        output_container = widgets.Tab(list(self.outputs.values()), layout=layout)
        output_container.set_title(0, 'Dataset Exploration')
        output_container.set_title(1, 'KDE Plot')

        main_frame = widgets.VBox([options_container, output_container],
                                  layout=layout)
        display(main_frame)

    def format_dataframe(self, **options):
        self.clear_outputs()
        df = self.data.copy()
        threshold = options.pop('threshold')
        metrics = list(options.pop('metrics')) or METRICS
        aggregate = options.pop('aggregate')

        display_cols = []
        for wn in ('cols', 'metrics'):
            columns = options.pop('display_' + wn)
            columns = (columns if columns
                       else (metrics if wn == 'metrics'
                             else MODEL_VARIABLES + self.hyperparameters))

            display_cols.extend(columns)

        for name, values in options.items():
            if values:
                df = df[df[name].isin(values)]

        columns = MODEL_VARIABLES + metrics + self.hyperparameters
        columns = [col for col in columns if col in df]
        df = (df[columns]
              .sort_values(MODEL_VARIABLES + metrics)
              .dropna(how='all', axis=1))

        metrics = [col for col in metrics if col in display_cols and col in df]
        columns = [col for col in columns if col in display_cols and col in df]

        with self.outputs['table']:
            table = df.copy()
            if aggregate:
                table = table.groupby(MODEL_VARIABLES).agg(['mean', 'std']).reset_index()
                columns = MODEL_VARIABLES + metrics
            table = table[columns]
            display(table.style.applymap(
                lambda x: self.recolor_numbers(x, threshold),
                subset=metrics))

        with self.outputs['plot']:
            fig, ax = plt.subplots(1, 1, figsize=(12,8))
            for metric in metrics:
                sns.kdeplot(df[metric], shade=True, ax=ax)

            plt.show()

    def _make_common_filter_handler(self, **linked):

        if not (len(linked) == len(self.widgets) - 1
                and any(k in linked for k in self.widgets)):
            raise TypeError("Invalid argument")

        def dropdown_evenhandler(change):
            filters = {name: widget.value for name, widget in linked.items()}
            widget_name = [n for n in list(self.widgets) if n not in filters][0]
            filters[widget_name] = change.new
            self.format_dataframe(**filters)

        return dropdown_evenhandler

    def clear_outputs(self):
        for output in self.outputs.values():
            output.clear_output()

    def load_data(self):
        filename = "all_results.csv"
        return pd.read_csv(filename)

    @staticmethod
    def recolor_numbers(value, thres):
        return 'color: red' if value <= thres else 'color: black'

    @staticmethod
    def format_title(title):
        return widgets.HTML("<b>{}:</b>".format(
            title.replace('_', ' ').capitalize()))
