import os
import os.path as p
import subprocess
import pandas as pd
import numpy as np
import re
import datetime as dt

GS_RESULTS_PATTERN = '(?:grid_search_)?(?P<model>(?:\w+\+?\-?)+)_(?P<date>\d{8})'

DATE_FORMAT = "%Y%m%d"
TIME_FORMAT = "%H%M"


def sh(*args):
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        out = proc.stdout.read()
    return out.decode('utf-8')


class EvalResults:

    metadata_pattern = 'final_eval(?P<model>)_(?P<date>\d{8}_\d{4})\.csv'
    dt_format = '%Y%m%d_%H%M'

    def __init__(self, data=None, file=None):
        if not any(data, file):
            raise TypeError('Must specify data or file')

    @classmethod
    def get_candidate_files(cls, directory='eval_results', pattern=None):
        import ipdb; ipdb.set_trace()
        regex = re.compile(pattern or cls.metadata_pattern)
        file_list = cls.get_list_of_files(directory, regex)
        files = pd.DataFrame(file_list, columns=['path'])
        metadata = files['path'].map(cls.pattern_scope).str.extract(regex)
        files = pd.concat([files, metadata], axis=1)
        if not all(col in files for col in ('model', 'date')):
            raise TypeError('Pattern must specify named groups model and date')
        files['date'] = pd.to_datetime(files['date'], format=cls.dt_format)
        files['select'] = True
        return files

    @classmethod
    def get_list_of_files(cls, directory, filename=None, pattern=None):
        cmd = 'find {}/ -type f'.format(directory)
        if filename:
            cmd += ' -name {}'.format(filename)
        file_list = sh(*cmd.split()).strip().split('\n')
        if pattern:
            file_list = [fn for fn in file_list
                         if pattern.match(cls.pattern_scope(fn))]
        return file_list

    @staticmethod
    def pattern_scope(path):
        return p.basename(path)


class GSResults(EvalResults):

    metadata_pattern = '(?:grid_search_)?(?P<model>(?:\w+\+?\-?)+)_(?P<date>\d{8})'
    dt_format = '%Y%m%d'

    @classmethod
    def get_list_of_files(cls, directory='grid_search_results', filename='results.txt'):
        args = 'find {}/ -type f -name {}'.format(directory, filename).split()
        return sh(*args).strip().split('\n')

    @staticmethod
    def pattern_scope(path):
        return p.basename(p.dirname(path))



def get_eval_files(directory='eval_results', prefix='final_eval', extension='csv'):
    result_files = pd.DataFrame(get_list_of_files(directory, extension, prefix), columns=['path'])
    result_files['model'] = None
    result_files['path'] = result_files['path'].map(lambda s: p.join(directory, s))
    result_files['date'] = result_files['path'].str.extract(
        re.compile(prefix + '_(\d{8}_\d{4})\.' + extension))
    result_files['date'] = pd.to_datetime(result_files['date'], format=DATE_FORMAT + "_" + TIME_FORMAT)
    result_files['select'] = True
    return result_files

def get_result_files(directory='grid_search_results', filename='results.txt'):
    args = 'find {}/ -type f -name {}'.format(directory, filename).split()
    result_files = pd.DataFrame(sh(*args).strip().split('\n'), columns=['path'])
    result_files['dirname'] = result_files['path'].map(lambda s: p.basename(p.dirname(s)))
    filename_pattern = re.compile(GS_RESULTS_PATTERN)
    metadata = result_files['dirname'].str.extract(filename_pattern)
    result_files = pd.concat([result_files, metadata], axis=1)
    result_files['date'] = pd.to_datetime(result_files['date'], format=DATE_FORMAT)
    result_files['select'] = True
    return result_files


def get_list_of_files(directory, extension='csv', prefix=None):

    def condition(filename):
        return_value = True
        if extension:
            return_value = filename.endswith('.' + extension)
        if prefix:
            return_value = return_value and filename.startswith(prefix)
        return return_value

    return sorted([f for f in os.listdir(directory) if condition(f)])

def load_results(files, processing_func=None, save_as=None, ts=True):

    results = pd.DataFrame()
    for result_file in files:
        df = parse_results(result_file)
        results = pd.concat([results, df], axis=0, sort=False).reset_index(drop=True)
    if processing_func:
        results = processing_func(results)

    if save_as:
        save_as = (save_as
                   + ('_{0:%Y%m%d}_{0:%H%M}'.format(dt.datetime.today()) if ts else '')
                   + '.csv')
        results.to_csv(save_as, index=False)

    return results

def load_gs_results(files, save=False):
    filename = 'grid_search_results/gs_results' if save else None
    return load_results(files, process_gs_results, filename)

def load_eval_results(files, save=False):
    filename = 'eval_results/eval_results' if save else None
    results = load_results(files, process_eval_results, filename)
    return results

def process_eval_results(df):
    df = df.where(df['unsupervised'].notnull()).dropna(how='all')
    df.loc[df['model'] == 'fc_classifier', 'unsupervised'] = 'false'
    df.loc[df['unsupervised'] == 'true', 'examples_per_class'] = 0
    df = df.drop('unsupervised', axis=1)
    columns = df.columns.str.extract(re.compile('(?:(val|test)_)?(\w+)'))
    columns.loc[columns.isnull().any(axis=1), 0] = ''
    df.columns = pd.MultiIndex.from_frame(columns[[1, 0]])
    for col in df.columns[df.dtypes == 'object']:
        if col[1] != '':
            df[col] = pd.to_numeric(df[col].str.replace(',', ''))
    return df

def process_gs_results(df):
    df = df.copy()

    if 'alpha' in df:
        df.loc[df['model'] == 'classifier', 'alpha'] = 0.
    if 'amplification' in df:
        df.loc[(df['amplification'].isnull()) & (df['model'].str.startswith('k')), 'amplification'] = 1.
        df.loc[(df['amplification'].isnull()), 'amplification'] = 0.
    if 'dropout_rate' in df:
        df.loc[df['dropout_rate'].isnull(), 'dropout_rate'] = 0.
    if 'competitive_topk' in df:
        df.loc[df['competitive_topk'].isnull(), 'competitive_topk'] = 0
    if 'contractive' in df:
        df.loc[df['contractive'].isnull(), 'contractive'] = 0
    if 'layers' in df:
        df.loc[(df['model'] == 'classifier') & (df['layers'].isnull()), 'layers'] = '[128]'
        df['depth'] = df['layers'].map(lambda s: len(eval(s)))
        df['encoding_dim'] = df['layers'].map(lambda s: eval(s)[-1])
        df.loc[df['model'] == 'classifier', 'encoding_dim'] = 0
    if 'penalty_weight' in df:
        penalty = df['penalty_weight'].str.extract(re.compile('\((?P<l1>0\.\d+)\,\s(?P<l2>0\.\d+)\)'))
        df = pd.concat([df, penalty], axis=1)
        df[['l1', 'l2']] = df[['l1', 'l2']].astype(float).fillna(0.)
        if 'penalty' in df:
            for pen in ('l1', 'l2'):
                mask = df['penalty'] == pen
                df.loc[mask, pen] = pd.to_numeric(df.loc[mask, 'penalty_weight'])
            df = df.drop('penalty', axis=1)
        df = df.drop('penalty_weight', axis=1)

    df['model'] = df['model'].replace({'ssae': 'autoencoder', 'classifier': 'fc_classifier'})
    return df


def get_all_results(directory='.'):
    result_files = get_result_files(directory)
    results = load_gs_results(result_files['path'])
    results.to_csv(p.join(directory, 'all_results.csv'), index=False)


def parse_results(filename):
    dirname = p.basename(p.dirname(filename))
    start_date = re.search('(\d{8})', filename)
    model_name = filename[:start_date.start(0)].split('_')[-2]
    start_date = pd.Timestamp(start_date.groups()[0]) if start_date else pd.NaT
    # model_name = attributes[0] if len(attributes) == 1 else attributes.pop(-1)
    with open(filename) as f:
        lines = f.readlines()

    if not lines[0].startswith('run_id='):
        run_id, rest = zip(*[line.strip().split(' ', 1) for line in lines])
    else:
        rest = lines

    def parse(line):
        items = line.split(',')
        i = 0
        while True:
            i += 1
            if i == len(items):
                break
            if not '=' in items[i]:
                i -= 1
                items[i] = ','.join([items[i], items.pop(i+1)])
        return dict(map(lambda s: tuple(s.split('=')), items))

    results = pd.DataFrame(list(map(parse, rest)))
    if 'model_name' not in results.columns:
        results['model_name'], results['date'] = model_name, start_date
    return format_dataframe(results)


def format_dataframe(df):
    df = df.applymap(lambda x: x if x != 'None' else np.NaN)
    columns = list(df.columns)
    df = df[columns[-2:] + columns[:-2]]

    df = infer_objects(df)
    df = df.dropna(how='all', axis=1)
    return df.rename(columns={'model_name': 'model',
                            'epc': 'examples_per_class',
                            'final_score': 'f1_score',
                            'fc_layers': 'layers',
                            'encoder_dims': 'layers'})

def infer_objects(df):
    for name in df.columns:
        column = df[name]
        if column.dtype == 'object':
            for dtype in ['int', 'float']:
                try:
                    df[name] = column.astype(dtype)
                    break
                except:
                    pass
    return df


def filter_dataframe(df, **filters):
    df = df.copy()
    for column, values in filters.items():
        if values:
            df = df[df[column].isin(values)]
    return df


def get_best_hyperparameters(df, variables, params, criteria='f1_score', agg='mean'):
    groups = variables + params
    scores = df[groups + [criteria]].groupby(groups).agg(agg).reset_index()
    sort_column = criteria if type(agg) != list else (criteria, agg[0])
    best = scores.sort_values(sort_column, ascending=False).groupby(variables).head(1)
    return best.sort_values(variables).reset_index(drop=True)


def filter_params(df, params):
    while isinstance(params.columns, pd.MultiIndex):
        params.columns = params.columns.droplevel(-1)

    keys = list(params.columns)
    return df[df.set_index(keys).index.isin(params.set_index(keys).index)].reset_index()
