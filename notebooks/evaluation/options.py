
DATE_PARAM = 'date'

MODEL_VARIABLES = {
    'model': ['autoencoder', 'cae', 'ksparse', 'kate', 'fc_classifier'],
    'examples_per_class': [0, 1, 10, 100, 1000]
}

HYPERPARAMETERS = {
    'alpha': [0., 0.2, 0.5, 0.8, 1.],
    'competitive_topk': [16, 32, 64],
    'contractive': [1e-4, 1e-3, 1e-2],
    'encoding_dim': [128, 512],
    'depth': [1, 2, 3],
    'l1': [0., 1e-3, 1e-2],
    'l2': [0., 1e-3, 1e-2],
    'amplification': [1, 10, 100],
    'dropout_rate': [0.0, 0.2, 0.5],
    'input_dropout': [0.2, 0.5, 0.8, 1.0],
}

METRICS = [
    'f1_score',
    'accuracy',
    'AMI',
    'NMI',
    'cluster_acc',
    'mse',
    'runtime',
    'SVM_f1_score',
    'train_accuracy',
    'test_accuracy',
    'train_f1_score',
    'test_f1_score',
]
