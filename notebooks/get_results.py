import os.path as p
import subprocess
import pandas as pd
import datetime as dt
import numpy as np


def sh(*args):
    with subprocess.Popen(args, stdout=subprocess.PIPE) as proc:
        out = proc.stdout.read()
    return out.decode('utf-8')


def infer_objects(df):
    for name in df.columns:
        column = df[name]
        if column.dtype == 'object':
            for dtype in ['int', 'float']:
                try:
                    df[name] = column.astype(dtype)
                    break
                except:
                    pass
    return df


def parse_results(filename):
    dirname = p.basename(p.dirname(filename))
    attributes = dirname.split('_')
    start_date = pd.NaT if len(attributes) == 1 else pd.Timestamp(attributes.pop(-1))
    model_name = attributes[0] if len(attributes) == 1 else attributes.pop(-1)

    with open(filename) as f:
        lines = f.readlines()

    run_id, rest = zip(*[line.strip().split(' ', 1) for line in lines])
    
    def parse(line):
        items = line.split(',')
        i = 0
        while True:
            i += 1
            if i == len(items):
                break
            if not '=' in items[i]:
                i -= 1
                items[i] = ','.join([items[i], items.pop(i+1)])
        return dict(map(lambda s: tuple(s.split('=')), items))
    
    results = list(map(parse, rest))
#     results = [dict(tuple(group.split('=')) for group in line.split(',') if group) for line in rest]
    df = pd.DataFrame(results).applymap(lambda x: x if x != 'None' else np.NaN)
    df['model_name'], df['start_date'] = model_name, start_date
    columns = list(df.columns)
    df = df[columns[-2:] + columns[:-2]]
    return infer_objects(df)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--root', type=str, default='.')
    parser.add_argument('--filename', type=str, default='results.txt')
    args = parser.parse_args()
    cmd = 'find {} -type f -name {}'.format(args.root, args.filename).split()
    result_files = sh(*cmd).strip().split('\n')
    results = pd.DataFrame()
    for result_file in result_files:
        df = parse_results(result_file)
        results = pd.concat([results, df], axis=0).reset_index(drop=True)

    results.to_csv('all_results.csv', index=False)

if __name__ == '__main__':
    main()

