import os
import os.path as p
import datetime as dt
import types
import warnings
import yaml
import pickle, json
from pprint import pformat
from operator import itemgetter


def load_config(master_config):
    if not p.exists(master_config):
        raise FileNotFoundError(master_config)

    def parse_config(d):
        for k, v in d.items():
            if type(v) is dict:
                parse_config(v)
            if v == 'none':
                d[k] = None
            if v == 'true':
                d[k] = True
            if v == 'false':
                d[k] = False

        return d

    def merge_configs(parent, child):
        if parent is None:
            return dict()
        for k, v in child.items():
            if k not in parent:
                parent[k] = v
            elif type(v) is dict:
                parent[k] = merge_configs(parent[k], v)
            else:
                continue

        return parent

    with open(master_config) as yaml_file:
        cfg = parse_config(yaml.load(yaml_file))

    for config_file in cfg.pop('dependencies'):
        child = load_config(p.join(p.dirname(master_config), config_file))
        cfg = merge_configs(cfg, child)

    return cfg


class Config:

    settings = 'settings'

    types = [


        ('data_home', 'dir'),
        ('preprocessed_data', 'dir'),
        ('_dir', 'dir'),
        ('tokenizer_config', str),
        ('_options', dict),
        ('log_level', ['NOTSET', 'DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']),
        ('n_classes', int),
        ('examples_per_class', [float, int]),
        ('validation_split', float),
        ('max_sequence_length', lambda x: type(x) == int and x > 0),
        ('max_documents', int),
        ('max_examples_per_doc', int),
        ('negative_samples', [float, int]),
        ('preprocess', bool),
        ('stopwords', 'file'),
        ('window_size', int),
        ('training_mode', ['supervised', 'unsupervised', 'semi-supervised']),
        ('bow_features', ['binary', 'count', 'tfidf', 'freq']),
        ('categorical', bool),
        ('categories', list),
        ('_layers', list),
        ('_dim', int),
        ('embedding_weights', 'file'),
        ('pretrain_embedding', bool),
        ('train_embedding', bool),
        ('dropout_rate', [float, list]),
        ('batch_size', int),
        ('batches_to_prefetch', int),
        ('epochs', int),
        ('loss', ['categorical_crossentropy', 'binary_crossentropy']),
        ('alpha', lambda x: 0 <= x <= 1),
        ('optimizer', ['adam', 'rmsprop', 'sgd', 'adagrad', 'adadelta']),
        ('metrics', list),

    ]

    def __init__(self, config_file: str, overwrite=None):
        self.settings = self.absolute_path(self.settings, config_file)
        config = load_config(self.settings)
        self.run_id = "{:%Y%m%d_%H%M}".format(dt.datetime.now())

        sections = list(config.keys())

        for section in sections:
            for k, v in config[section].items():
                if hasattr(self, k):
                    raise TypeError("{} was defined in multiple sections. Conflicting values: {} and {}"
                                    .format(k, getattr(self, k), v))
                setattr(self, k, v)

        # Custom override (only existing settings)
        if type(overwrite) is dict:
            for k, v in overwrite.items():
                if hasattr(self, k):
                    setattr(self, k, v)

        # Outputs
        self.run_id = "{:%Y%m%d_%H%M}".format(dt.datetime.now())
        self.output_dir = p.join(self.output_dir, self.run_id)

        for attr in ('output_dir', 'data_home', 'preprocessed_data'):
            setattr(self, attr, self.absolute_path(getattr(self, attr)))

        assert hasattr(self, 'preprocessed_data'), 'Path to preprocessed data not specified'

        preprocessed_data = getattr(self, 'preprocessed_data')
        for directory in (self.output_dir, preprocessed_data):
            os.makedirs(directory, exist_ok=True)

        for attr in ['tokenizer_config', 'vectorizer_config']:
            if hasattr(self, attr):
                setattr(self, attr, p.join(preprocessed_data, getattr(self, attr)))

        if hasattr(self, 'preprocessing_options'):
            opts = getattr(self, 'preprocessing_options')
            if 'preprocessed_datasets' in opts:
                datasets = opts['preprocessed_datasets'].copy()
                for k, v in datasets.items():
                    opts['preprocessed_datasets'][k] = p.join(preprocessed_data, v)
            if 'stopwords' in opts:
                opts['stopwords'] = p.join(preprocessed_data, opts['stopwords'])
            setattr(self, 'preprocessing_options', opts)

        if hasattr(self, 'callbacks'):
            callbacks = getattr(self, 'callbacks')
            for k, opts in callbacks.items():
                for opt_name in opts:
                    if opt_name == 'log_dir':
                        opts[opt_name] = p.join(p.dirname(self.output_dir), opts[opt_name], self.run_id)
                    elif opt_name.endswith('file') or opt_name.endswith('dir'):
                        opts[opt_name] = p.join(self.output_dir, opts[opt_name])
            setattr(self, 'callbacks', callbacks)

        self.check()

    def check(self):

        for attr, typ in self.types:
            self.raise_on_type_mismatch(attr, typ)

        for attr in dir(self):
            is_valid = not (attr.startswith('_') or callable(getattr(self, attr)))
            is_necessary = attr not in ('types', 'settings', 'run_id')
            check_missing = attr not in map(itemgetter(0), self.types)
            check_pattern = "_" + attr.split('_')[-1] not in map(itemgetter(0), self.types)
            if is_valid and is_necessary and check_missing and check_pattern:
                warnings.warn("No check for {}".format(attr))

    def __str__(self):
        def exclude(attr): return not(attr in ['types'] or attr.startswith('_'))
        names = list(filter(exclude, dir(self)))

        def is_callable(t): return not callable(t[1])
        # attributes = list(filter(is_callable, zip(names, map(lambda a: getattr(self, a), names))))
        attributes = list(filter(is_callable, map(lambda a: (a, getattr(self, a)), names)))
        return "\n".join(["{:<25}{}".format(key, pformat(value, indent=4)) for key, value in attributes])

    def get(self, name, default=None):
        return getattr(self, name) if hasattr(self, name) else default

    def raise_on_type_mismatch(self, attr, t):
        if attr.startswith('_'):
            attrs = [a for a in dir(self) if (not a.startswith('_') and a.endswith(attr))]
            for a in attrs:
                self.raise_on_type_mismatch(a, t)
        if not hasattr(self, attr):
            return
        value = getattr(self, attr)
        error = ""
        if value is None:
            pass
        elif t in ('dir', 'file'):
            if not p.exists(value):
                error = "{} does not exist: {}".format(attr, value)
            if t == 'dir' and not p.isdir(value):
                error = "{} is not a directory: {}".format(attr, value)
            if t == 'file' and not p.isfile(value):
                error = "{} is not a file: {}".format(attr, value)

        elif type(t) == list:
            if (type(t[0]) == type and type(value) in t) or value in t:
                return
            error = "Accepted values for {}: {}. Current: {}".format(attr, t, value)
        elif isinstance(t, types.FunctionType):
            if t(value):
                return
            error = "Failed check: {} with value {}".format(attr, value)
        elif not type(value) == t:
            error = "{} should be of type {}, not {}. Value: {}".format(attr, t, type(value), value)

        if error:
            raise TypeError(error)

    def to_pickle(self):
        with open(p.join(self.output_dir, 'config.pkl'), 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def from_pickle(cls, filename):
        with open(filename, 'rb') as f:
            return pickle.load(f)

    @staticmethod
    def absolute_path(*paths):
        return p.abspath(p.join(p.dirname(__file__), '../..', *paths))

