from copy import deepcopy
from keras import callbacks
from keras.models import load_model
from tensorflow_models.utils.nn_utils import CustomModelCheckpoint


class BaseModel:

    """Base class for model"""

    def __init__(self, generator, config, logger):
        self.generator = generator
        self.logger = logger
        self.output_dir = config.output_dir
        self.epochs = config.epochs
        self.batch_size = config.batch_size
        self.optimizer = config.optimizer
        self.optimizer_options = config.get('optimizer_options') or dict()
        self.callback_flags = config.callback_flags
        self.callback_options = config.callbacks
        self.build()
        self.built = True
        self.compile()

    def build(self):
        # # ==================================== Example ====================================
        # input_layer = Input(input_size)
        # logits, predictions = self.build_classifier(input_layer)
        # self.classifier = Model(inputs=input_layer, outputs=logits)
        # self.classifier.summary()
        raise NotImplementedError

    def train(self):
        # # ==================================== Example ====================================
        # self.compile()
        # data = self.get_network_inputs()
        #
        # history = self.classifier.fit(data['inputs'], data['outputs'],
        #                                epochs=self.epochs,
        #                                batch_size=self.batch_size,
        #                                shuffle=True,
        #                                validation_data=data['validation'],
        #                                callbacks=self.get_callbacks())
        #
        # metrics_names = self.autoencoder.metrics_names
        # self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])
        raise NotImplementedError

    def get_network_inputs(self):
        # # ==================================== Example ====================================
        # X_train, y_train, _ = self.generator.train
        # X_val, y_val, _ = self.generator.validation
        # return dict(inputs=X_train, outputs=y_train, validation=(X_val, y_val))
        raise NotImplementedError

    def build_loss(self):
        # # ==================================== Example ====================================
        # return 'binary_crossentropy'
        raise NotImplementedError

    def compile(self):
        # # ==================================== Example ====================================
        # optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)
        #
        # self.classifier.compile(
        #     optimizer=optimizer,
        #     loss=self.build_loss())
        raise NotImplementedError

    def get_callbacks(self):
        cbks = []
        for name, options in deepcopy(self.callback_options).items():
            if not self.callback_flags['use_' + name]:
                continue
            if name == 'CustomModelCheckpoint':
                options['model'] = getattr(self, options['model'])
                cbks.append(CustomModelCheckpoint(**options))
            else:
                cbks.append(getattr(callbacks, name)(**options))
        return cbks

    def load_weights(self, weight_file, model_name=None):
        assert self.built, 'The model must be built in order to load weights'
        model_name = model_name or self.callback_options['CustomModelCheckpoint']['model']
        self.logger.info("Loading weights of {}".format(model_name))
        assert type(model_name) is str
        getattr(self, model_name).load_weights(weight_file)

    def load(self, model_checkpoint, model_name=None):
        model_name = model_name or self.callback_options['CustomModelCheckpoint']['model']
        setattr(self, model_name, load_model(model_checkpoint))

    def evaluate(self, subset='test'):
        # # ==================================== Example ====================================
        # X, y = self.generator.get_dataset(2, subset)
        # predictions = self.classifier.predict(X).argmax(1)
        # y_true = y.argmax(1)
        # cr = classification_report(y_true, predictions, target_names=self.generator.target_names)
        # final_score = f1_score(y_true, predictions, average='macro')
        # for line in cr:
        #     self.logger.info(line)
        # return {'f1_score': final_score}
        raise NotImplementedError
