import tensorflow as tf

from tensorflow_models.data_loaders import Generator
from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import get_optimizer

from .model import BaseModel
from .config import Config

import warnings

# TODO: Kept for compatibility but should be removed --> train function implemented in model
warnings.warn('The train function should be implemented as a method of the model', DeprecationWarning)


class BaseTrainer:

    def __init__(self, generator: Generator, model: BaseModel, config: Config, logger: Logger):
        self.check_config(config)
        self.logger = logger
        self.data = None
        self.generator = generator
        self.init_attr(config)
        self.graph = tf.Graph().as_default()
        self.init_datasets()
        self.build_model(model)

    def init_attr(self, config):
        self.sess = None
        self.output_dir = config.output_dir
        self.checkpoint_file = config.output_dir + "/model_checkpoint.ckpt"
        self.log_dir = config.log_dir
        self.training_mode = config.training_mode
        self.epochs = config.epochs
        self.batch_size = config.batch_size
        self.optimizer_name = config.optimizer
        self.optimizer_options = config.optimizer_options
        self.global_step = tf.train.get_or_create_global_step()
        self.is_training = tf.placeholder_with_default(False, [], name='phase')

    def init_datasets(self):
        with tf.variable_scope('dataset'):
            self.train_steps, self.valid_steps, self.test_steps = self.generator.get_n_steps()
            self.batch_size = self.generator.batch_size
            self.train_set = self.generator.make_dataset()
            self.valid_set = self.generator.make_dataset('valid')
            self.test_set = self.generator.make_dataset('test')

            # self.dataset_handle = tf.placeholder(tf.string, shape=[], name='dataset_handle')
            # self.iterator = tf.data.Iterator.from_string_handle(self.dataset_handle, self.train_set.output_types)
            # self.inputs = self.iterator.get_next()

            self.iterator = tf.data.Iterator.from_structure(
                output_shapes=self.train_set.output_shapes,
                output_types=self.train_set.output_types)
            self.inputs = self.iterator.get_next()
            self.train_init = self.iterator.make_initializer(self.train_set)
            self.valid_init = self.iterator.make_initializer(self.valid_set)
            self.test_init = self.iterator.make_initializer(self.test_set)


    def build_model(self, model):
        self.outputs, self.losses, self.metrics = model(*self.inputs)

        self.logger.analyze_vars(tf.trainable_variables(), print_info=True, tensorboard=True)

        with tf.variable_scope('train'):
            self.optimizer = get_optimizer(self.optimizer_name)(**self.optimizer_options)
            self.training_op = self.optimizer.minimize(self.losses['loss'])
            self.init_ops = [tf.global_variables_initializer(), tf.local_variables_initializer()]

        self.summary = tf.summary.merge_all()
        self.saver = tf.train.Saver()
        self.train_fetch, self.val_fetch = self.fetch_tensors()

    def train(self):
        self.logger.info('Start training')
        summary_writer = tf.summary.FileWriter(self.log_dir, graph=tf.get_default_graph())

        with tf.Session() as self.sess:
            [init_op.run() for init_op in self.init_ops]
            for epoch in range(1, self.epochs + 1):
                tf.train.get_global_step()
                self.train_one_epoch(epoch, summary_writer)

    def train_one_epoch(self, epoch, summary_writer):
        # ============================= Example: ============================= 
        # loss, rloss = 0, 0
        # self.train_init.run()
        # for _ in tqdm(range(self.train_steps)):
        #     _, ploss, prloss, summary = self.sess.run(self.train_fetch)
        #     loss += ploss / self.train_steps
        #     rloss += prloss / self.train_steps

        # val_loss, val_rloss = 0, 0
        # self.valid_init.run()
        # for _ in tqdm(range(self.valid_steps)):
        #     ploss, prloss, summary = self.sess.run(self.val_fetch)
        #     val_loss += ploss / self.valid_steps
        #     val_rloss += prloss / self.valid_steps

        # self.logger.info("{}/{}".format(epoch, self.epochs))
        # self.saver.save(self.sess, self.output_dir + "/autoencoder.ckpt")
        # summary_writer.add_summary(summary, epoch)
        raise NotImplementedError

    def evaluate(self):
        raise NotImplementedError

    @staticmethod
    def check_config(config):
        pass

    def fetch_tensors(self):
        raise NotImplementedError
