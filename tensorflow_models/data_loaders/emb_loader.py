import numpy as np
from keras.layers import Embedding
from gensim.models.keyedvectors import Word2VecKeyedVectors


class EmbLoader:

    def __init__(self, word_index, max_sequence_length, config, logger_fn=None):
        self.logger = logger_fn or print
        self.weights_file = config.embedding_weights
        self.embedding_dim = config.embedding_dim
        self.max_sequence_length = max_sequence_length
        self.word_index = word_index
        self.vocab_size = len(word_index)
        self.embedding_shape = self.vocab_size + 1, self.embedding_dim
        self.build()

    def build(self):
        embeddings_index = self.load()
        embedding_dim = len(embeddings_index[list(embeddings_index.keys())[0]])
        if not embedding_dim == self.embedding_dim:
            raise TypeError("Wrong dimensions: {}, loaded embedding has {}".format(self.embedding_dim, embedding_dim))
        embeddings_array = np.stack([ar for ar in embeddings_index.values()])
        mean, std = embeddings_array.mean(axis=0), embeddings_array.std(axis=0)
        embedding_matrix = np.random.normal(mean, std, self.embedding_shape)

        tries, hits = 0, 0
        for word, i in self.word_index.items():
            tries += 1
            embedding_vector = embeddings_index.get(word)
            if embedding_vector is not None:
                hits += 1
                embedding_matrix[i] = embedding_vector

        self.logger('Hit rate: {:.2%} Missed: {}'.format(hits / tries, tries - hits))
        self.embedding_matrix = embedding_matrix

    def load(self):
        self.logger("Loading embedding from {}".format(self.weights_file))
        # Binary file
        if self.weights_file.endswith('bin'):
            return self.load_embedding_from_binary_file(self.weights_file)
        # Text file (default)
        return self.load_embedding_from_text_file(self.weights_file)

    def get_keras_layer(self, trainable=False):
        return Embedding(
            *self.embedding_shape,
            weights=[self.embedding_matrix],
            input_length=self.max_sequence_length,
            trainable=trainable)

    @staticmethod
    def load_embedding_from_binary_file(weights_file):
        vector = Word2VecKeyedVectors.load_word2vec_format(weights_file, binary=True)
        embedding_index = dict()
        for word in vector.vocab:
            embedding_index[word] = vector[word]
        return embedding_index

    @staticmethod
    def load_embedding_from_text_file(weights_file):
        embeddings_index = dict()
        with open(weights_file) as f:
            for line in f:
                values = line.split()
                word = values[0]
                coefs = np.asarray(values[1:], dtype='float32')
                embeddings_index[word] = coefs
        return embeddings_index
