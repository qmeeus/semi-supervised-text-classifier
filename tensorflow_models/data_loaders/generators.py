import os.path as p
import numpy as np
import tensorflow as tf
import pickle
from keras_preprocessing.text import Tokenizer, tokenizer_from_json
from keras_preprocessing.sequence import pad_sequences, skipgrams, make_sampling_table
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.utils.validation import check_is_fitted

from .emb_loader import EmbLoader
from tensorflow_models.utils.np_utils import vecnorm, add_some_noise


class Generator:

    def __init__(self, load_func, config, logger=None):
        # Initialise the attributes
        self.logger = logger if callable(logger) else (logger.debug if hasattr(logger, 'debug') else print)
        self.data_home = config.data_home
        self.examples_per_class = config.examples_per_class
        self.validation_split = config.validation_split
        self.preprocessing_options = config.preprocessing_options

        # Load the data
        train, validation, test = self.load(load_func)

        # Preprocess the data
        self.train = self.preprocess(train)
        self.validation, self.test = [self.preprocess(d, subset='test') for d in (validation, test)]

        # Initialise training attributes
        self.training_mode = config.training_mode
        self.n_outputs = 2 - ('un' in self.training_mode) + ('semi-' in self.training_mode)

        func = sum if self.n_outputs == 2 else len
        self.batch_size = min(config.batch_size, func(self.train[2]))
        self.batches_to_prefetch = config.batches_to_prefetch
        self.epochs = config.epochs

        self.output_types = ['object', 'int32', 'bool']
        self.output_shapes = [[], self.n_classes, []]

    def load(self, load_func):

        train, validation, test, self.target_names = \
            load_func(
                data_home=self.data_home,
                examples_per_class=self.examples_per_class,
                validation_split=self.validation_split,
                print_fn=self.logger,
                **self.preprocessing_options)

        self.n_classes = len(self.target_names)
        self.n_examples = len(train[0])
        self.n_labels = (train[1] != -1).sum()

        return train, validation, test

    def make_dataset(self, subset='train', one_shot=False):
        raise NotImplementedError

    def _make_dataset(self, generator, subset, one_shot):
        dataset = (tf.data.Dataset
                   .from_generator(generator, *self.output_attributes())
                   .batch(self.batch_size, drop_remainder=True)
                   .prefetch(self.batches_to_prefetch * self.batch_size))
        if subset == 'train':
            dataset = dataset.shuffle(self.n_examples)
        if subset == 'test':
            return dataset
        if one_shot:
            return dataset.repeat(self.epochs)
        return dataset

    def get_dataset(self, n_outputs, subset='train', shuffle=False):
        dataset_name = subset + ('ation' if subset == 'valid' else '')
        assert 0 < n_outputs < 4, "Invalid number of outputs: {}".format(n_outputs)

        dataset = getattr(self, dataset_name)
        if shuffle: self.shuffle(dataset_name)

        out = [texts, labels, mask] = list(dataset)
        if n_outputs == 2 and self.training_mode == 'supervised':
            return texts[mask], labels[mask]

        return tuple(out[:n_outputs])

    def output_attributes(self):
        output_shapes = tuple([tf.TensorShape(dims) for dims in self.output_shapes[:self.n_outputs]])
        output_types = tuple(self.output_types[:self.n_outputs])
        return output_types, output_shapes

    def get_n_steps(self):
        steps = []
        func = sum if self.training_mode == 'supervised' else len
        for subset in ('train', 'valid', 'test'):
            dataset = getattr(self, subset + ('' if subset[0] == 't' else 'ation'))
            steps.append(max(1, int(func(dataset[-1]) // self.batch_size)))
        return steps

    def preprocess(self, dataset, subset='train'):
        texts, labels, mask = dataset
        return texts, labels, mask

    def shuffle(self, dataset_name):
        dataset = getattr(self, dataset_name)
        idx = np.arange(len(dataset[0]))
        np.random.shuffle(idx)
        dataset = tuple([d[idx] for d in dataset])
        setattr(self, dataset_name, dataset)

    @staticmethod
    def to_dense(array, mask_value=-1):
        dense = np.argmax(array, axis=1)
        mask = (array.sum(axis=1) == 0)
        dense[mask] = mask_value
        return dense


class SequenceGenerator(Generator):

    def __init__(self, load_func, config, logger=None):
        self.max_sequence_length = config.max_sequence_length
        self.fixed_seq_size = bool(self.max_sequence_length)
        self.pad_sequences = self.max_sequence_length is not None
        self.tokenizer_file = config.tokenizer_config
        self.tokenizer_options = config.tokenizer_options

        super(SequenceGenerator, self).__init__(load_func, config, logger)
        self.num_words = self.tokenizer.num_words
        if config.get('embedding_weights'):
            self._emb_loader = EmbLoader(self.get_word_index(), self.max_sequence_length, config, self.logger)

        self.output_types[0] = 'int32'
        self.output_shapes[0] = [self.max_sequence_length]

    @property
    def emb_loader(self):
        if hasattr(self, '_emb_loader'):
            return self._emb_loader
        else:
            raise AttributeError('emb_loader')

    def make_dataset(self, subset='train', one_shot=False):

        def generator():
            for inputs in zip(*self.get_dataset(self.n_outputs, subset)):
                yield inputs

        return self._make_dataset(generator, subset, one_shot)

    def load(self, load_func):
        train, validation, test = super(SequenceGenerator, self).load(load_func)
        filename = self.tokenizer_file
        if filename and p.exists(filename):
            with open(filename, 'r') as json_file:
                self.tokenizer = tokenizer_from_json(json_file.read())
                self.tokenizer.is_trained = True
            self.logger('Tokenizer loaded from {}'.format(filename))
        else:
            self.tokenizer = Tokenizer(**self.tokenizer_options)
            self.tokenizer.is_trained = False
            self.logger("An empty tokenizer was instantiated.")
        return train, validation, test

    def preprocess(self, dataset, subset='train'):
        dataset = super(SequenceGenerator, self).preprocess(dataset, subset=subset)
        texts, labels, mask = dataset

        if subset is 'train':
            self.train_tokenizer(texts)

        sequences = self.tokenizer.texts_to_sequences(texts)
        if not self.fixed_seq_size and subset == 'train':
            maxlen = self.max_sequence_length or 0
            self.max_sequence_length = max(maxlen, max(map(len, sequences)))

        if self.pad_sequences:
            sequences = pad_sequences(sequences, maxlen=self.max_sequence_length)

        return sequences, labels, mask

    def train_tokenizer(self, texts):
        if not self.tokenizer.is_trained:
            self.logger('Train tokenizer on {} examples'.format(len(texts)))
            self.tokenizer.fit_on_texts(texts)
            self.tokenizer.is_trained = True
        if self.tokenizer_file and not p.exists(self.tokenizer_file):
            self.logger("Saving tokenizer config to {}".format(self.tokenizer_file))
            with open(self.tokenizer_file, 'w') as f:
                f.write(self.tokenizer.to_json())
        self.vocabulary_size = len(self.tokenizer.word_index) + 1

    def get_word_index(self):
        if not self.tokenizer.word_index:
            raise Exception("Tokenizer is not trained")
        return self.tokenizer.word_index


class BOWGenerator(SequenceGenerator):

    def __init__(self, load_func, config, logger=None):
        self.bow_features = config.bow_features
        super(BOWGenerator, self).__init__(load_func, config, logger)
        self.output_types[0] = 'float32'
        self.output_shapes[0] = [self.num_words]

    def make_dataset(self, subset='train', one_shot=False):

        def generator():
            for inputs in zip(*self.get_dataset(self.n_outputs, subset)):
                yield inputs

        return self._make_dataset(generator, subset, one_shot)

    def preprocess(self, dataset, subset='train'):
        sequences, labels, mask = super(BOWGenerator, self).preprocess(dataset, subset=subset)

        features = self.tokenizer.sequences_to_matrix(sequences.tolist(), mode=self.bow_features)
        return features, labels, mask


class CountGenerator(Generator):

    Vectorizer = CountVectorizer

    def __init__(self, load_func, config, logger=None):
        self.normalization = config.get('normalization')
        self.vectorizer_file = config.get('vectorizer_config')
        self.vectorizer_options = config.vectorizer_options

        super(CountGenerator, self).__init__(load_func, config, logger)
        self.num_words = len(self.vectorizer.vocabulary_)
        self.output_types[0] = 'float32'
        self.output_shapes[0] = [self.num_words]

    def make_dataset(self, subset='train', one_shot=False):

        def generator():
            for inputs in zip(*self.get_dataset(self.n_outputs, subset)):
                yield inputs

        return self._make_dataset(generator, subset, one_shot)

    def load(self, load_func):
        train, validation, test = super(CountGenerator, self).load(load_func)

        filename = self.vectorizer_file
        if filename and p.exists(filename):
            with open(filename, 'rb') as pickle_file:
                self.vectorizer = pickle.load(pickle_file)
            self.logger('Vectorizer loaded from {}'.format(filename))
        else:
            self.vectorizer = self.Vectorizer(**self.vectorizer_options)
            self.logger("An empty vectorizer was instantiated.")

        return train, validation, test        

    def train_vectorizer(self, texts):
        self.vectorizer.fit(texts)
        if self.vectorizer_file and not p.exists(self.vectorizer_file):
            self.logger("Saving vectorizer config to {}".format(self.vectorizer_file))
            with open(self.vectorizer_file, 'wb') as pickle_file:
                pickle.dump(self.vectorizer, pickle_file)

    def preprocess(self, dataset, subset='train'):
        texts, labels, mask = super(CountGenerator, self).preprocess(dataset, subset=subset)

        if subset is 'train':
            self.train_vectorizer(texts)
            self.logger('Num features: {}'.format(len(self.vectorizer.vocabulary_)))

        features = self.vectorizer.transform(texts).toarray()
        if self.normalization:
            features = vecnorm(features, axis=1, **self.normalization)

        return features, labels, mask

    def get_word_index(self):
        check_is_fitted(self.vectorizer, ['vocabulary_'])
        return self.vectorizer.get_feature_names()


class TfIdfGenerator(CountGenerator):

    Vectorizer = TfidfVectorizer


class NoisyBOWGenerator(BOWGenerator):

    def __init__(self, load_func, config, logger=None):
        self.normalization = config.get('normalization', dict())
        self.noise_type = config.get('noise_type')
        self.noise_options = config.get('noise_options')
        if config.training_mode not in ['unsupervised', 'semi-supervised']:
            raise TypeError('Supported mode are unsupervised or semi-supervised')
        super(NoisyBOWGenerator, self).__init__(load_func, config, logger)
        self.n_outputs += 1
        self.output_types.insert(0, 'float32')
        self.output_shapes.insert(0, [self.num_words])

    def get_dataset(self, n_outputs, subset='train', shuffle=True):
        dataset_name = subset + ('ation' if subset[0] == 'v' else '')
        assert n_outputs in [2, 4], "Invalid number of outputs: {}".format(n_outputs)

        dataset = [_, _, labels, mask] = getattr(self, dataset_name)
        if shuffle: self.shuffle(dataset_name)

        return tuple(dataset[:n_outputs])

    def preprocess(self, dataset, subset='train'):
        features, labels, mask = super(NoisyBOWGenerator, self).preprocess(dataset, subset)
        normalized = vecnorm(features, **self.normalization, axis=1)
        noisy = add_some_noise(normalized, self.noise_type, self.noise_options)
        return noisy, normalized, labels, mask


class SkipgramGenerator(SequenceGenerator):

    def __init__(self, load_func, config, logger=None):
        self.window_size = config.window_size
        self.negative_samples = config.negative_samples
        self.max_documents = config.max_documents
        self.max_examples_per_doc = config.max_examples_per_doc
        config.max_sequence_length = None
        super(SkipgramGenerator, self).__init__(load_func, config, logger)
        self.n_outputs = 2 if self.negative_samples == 0 else 3

        self.sampling_table = make_sampling_table(self.num_words)

        self.output_types = ['int32', 'int32', 'int32']
        self.output_shapes = [[], [], []]

    def make_dataset(self, subset='train', one_shot=False):

        def generator():

            for j, (seq,) in enumerate(zip(*self.get_dataset(1, subset, shuffle=False))):
                if self.max_documents and j > self.max_documents:
                    return
                X, y = skipgrams(
                    sequence=seq,
                    vocabulary_size=self.num_words,
                    window_size=self.window_size,
                    negative_samples=self.negative_samples,
                    shuffle=False,
                    sampling_table=self.sampling_table)

                if X:
                    w, c = [np.array(X) for X in zip(*X)]
                    y = np.array(y, dtype=np.int32)

                    for i, (wi, ci, yi) in enumerate(zip(w, c, y)):
                        if self.max_examples_per_doc and i > self.max_examples_per_doc:
                            break
                        out = [wi, ci, yi]
                        yield tuple(out[:self.n_outputs])

        return self._make_dataset(generator, subset, one_shot)

    def shuffle(self, dataset_name):
        """
        Shuffle does not work because it needs to convert X to an array, which is not allowed
        for non fixed dimensions
        """
        raise NotImplementedError
