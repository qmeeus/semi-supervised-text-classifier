import os.path as p
import pandas as pd
import numpy as np
import pickle
import re
from time import time
from operator import itemgetter

from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.porter import PorterStemmer


def one_hot(array, n_classes):
    out = np.zeros((array.shape[0], n_classes), dtype='int32')
    for i, x in enumerate(array):
        if x == -1:
            continue
        out[i, x] = 1
    return out


def calculate_supervised_mask(df, examples_per_class, n_classes, subset='train'):
    thres = examples_per_class
    if thres * n_classes > len(df):
        thres = 1.

    if subset == 'test' or thres <= 0 or (thres == 1 and type(thres) is float):
        return df.assign(mask=thres > 0)

    df['mask'] = False
    if 0 < thres < 1:
        df.loc[df.sample(frac=thres, replace=False).index, 'mask'] = True

    if type(thres) == int:
        def get_sample(df): return df.sample(thres)
        labelled_data = (df.reset_index()[['label', 'index']]
                         .groupby('label', as_index=False)
                         .apply(get_sample)
                         ['index'].values)

        df.loc[labelled_data, 'mask'] = True

    df.loc[~df['mask'], 'label'] = -1
    return df

def remove_header(text):
    text = text.strip()
    n_lines = 'Lines\: \d+\n'
    org = 'Organization\: .*?\n'
    replyto = 'Reply-To\: .*?\n'
    xnews = 'X-Newsreader: .*?\n'
    nntp = 'NNTP-Posting-Host\: .*?\n'
    news_soft = 'News-Software\: .*?\n'
    patterns = [n_lines, org, replyto, xnews, nntp]
    flag = False
    for pattern in patterns:
        match = re.search(pattern, text, re.IGNORECASE)
        if match:
            text = text[match.span()[1]:].strip()
            flag = True
    return text


def get_vocab_features(X, min_df=10, max_df=0.8, stopwords='english'):
    vect = TfidfVectorizer(min_df=10, max_df=0.8, stop_words=stopwords).fit(X)
    vocabulary = vect.vocabulary_
    word_index = list(map(itemgetter(0), sorted(vocabulary.items(), key=itemgetter(1))))
    default_stopwords = vect.get_stop_words() or set()
    return dict(vocabulary=vocabulary, word_index=word_index, stopwords=vect.stop_words_ | default_stopwords)


def filter_stopwords(documents, stopwords):
    
    def is_not_stopword(word):
        return word not in stopwords
    
    documents = documents.copy()
    for i in range(len(documents)):
        doc_as_list = documents[i].split()
        documents[i] = ' '.join(list(filter(is_not_stopword, doc_as_list)))
    return documents

def clean_texts(texts, stopwords=None, stem=False, filters=None, rm_header=False):
    if rm_header:
        texts = texts.map(remove_header)

    if filters:
        regular_expressions = [
            ('email', r"(\w\S*@(?:[\w\-]+\.)+[\w\-]{2,4})"),
            ('url', r"(https?\:\/\/)?(www\.)?(\w+\.)+\w{2,3}"),
            ('from_field', r"(From\:.*?\n)"),
            ('digits', r'(\d+)'),
            ('nonwords', r"[^a-z0-9\s]"),
         ]
        
        rgx = re.compile('|'.join([r for t, r in regular_expressions if t in filters]))
        texts = texts.str.lower().str.replace(rgx, ' ')

    if stopwords:
        if p.exists(stopwords):
            with open(stopwords) as f:
                stopwords = set(stopwords.readlines())
        texts = filter_stopwords(texts, stopwords)
                       
    if stem:
        stemmer = PorterStemmer()

        def stem(text):
            cleaned = []
            for w in text.split():
                try:
                    cleaned.append(stemmer.stem(w))
                except:
                    cleaned.append(w)
            return " ".join(cleaned)

        texts = texts.map(stem)

    return texts


def load_newsgroups(
        data_home=None,
        preprocessed_datasets=None,
        stopwords=None,
        categories=None,
        categorical=True,
        clean=False,
        stem=False,
        filters=None,
        rm_header=False,
        examples_per_class=1.,
        validation_split=.2,
        print_fn=None):

    """
    data_home::str          Where to find the data (if None, download them)
    preprocessed_data::dict If not None, the dict must contain 3 elements with the keys matching 'training_set',
                            'testing_set' and 'target_names' and the values pointing to corresponding pickle
                            files in the preprocessed_data folder.
    categories::list        Which categories to load
    examples_per_class      Either int of float. The number of supervised examples in the dataset
                            If an integer, the number of examples per class (size = nclasses * threshold)
                            If a floating point number, the proportion of the dataset (size ~ threshold * m)
    validation_split::float The size of the validation split compared to the full training set

    returns: tuple          The output is a 4-elements tuple of the form ((X,y,m), (Xv,yv,mv), (Xt,yt,mt), tn)
                            where the first three tuples are the training, validation and testing sets and
                            tn are the target names. 
    """

    print_fn = print_fn or print

    datasets = []
    t0 = time()
    for i, subset in enumerate(['train', 'test']):
        t1 = time()
        subset_name = subset + 'ing_set'
        if preprocessed_datasets and p.exists(preprocessed_datasets[subset_name]):
            with open(preprocessed_datasets[subset_name], 'rb') as f:
                df = pickle.load(f)
            with open(preprocessed_datasets['target_names'], 'rb') as f:
                target_names = pickle.load(f)

        else:

            # Load the dataset and convert to dataframe
            dataset = fetch_20newsgroups(
                data_home=data_home,
                subset=subset,
                categories=categories)

            target_names = dataset.target_names

            df = pd.DataFrame(
                list(zip(dataset.data, dataset.target)),
                columns=["text", "label"])

            if clean:
                df['text'] = clean_texts(df['text'], stopwords, stem, filters, rm_header)

            if preprocessed_datasets:
                with open(preprocessed_datasets[subset + 'ing_set'], 'wb') as f:
                    pickle.dump(df, f)
                with open(preprocessed_datasets['target_names'], 'wb') as f:
                    pickle.dump(target_names, f)

        def format_dataset(df):
            labels = one_hot(df['label'], len(target_names)) if categorical else df['labels'].values
            return df['text'].values, labels, df['mask'].values

        if subset == 'test':
            df['mask'] = examples_per_class != 0
            datasets.append(format_dataset(df))
            print_fn('Testing set loaded in {:.2f} s: {} testing examples'.format(time() - t1, len(df)))
            continue

        pivot = -round(len(df) * validation_split) if 0 < validation_split < 1 else len(df) - validation_split
        train, val = df.iloc[:pivot].copy(), df.iloc[pivot:].copy()
        val['mask'] = examples_per_class != 0
        train = calculate_supervised_mask(train, examples_per_class, len(target_names))

        [datasets.append(format_dataset(df)) for df in (train, val)]

        print_fn('Training set loaded in {:.2f} s: {} training examples, {} validation examples'
                 .format(time() - t1, len(train), len(val)))
        print_fn('Supervised examples: {} out of {}'.format(len(train[train['label'] != -1]), len(train)))

    print_fn('Total time: {:.2f} s'.format(time() - t0))
        
    datasets.append(target_names)
    return datasets
