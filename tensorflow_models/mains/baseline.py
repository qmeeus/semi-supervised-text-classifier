import importlib
import datetime as dt
import os.path as p
import timeit

from tensorflow_models.base import Config
from .train import train, REGISTERED_MODELS
from .evaluate import evaluate

RUNS = 3
EPOCHS = 100
EXAMPLES_PER_CLASS = [1, 2, 5, 10, 20, 50, 100, 1000]

MODELS = [
    # 'fc_classifier',
    'seq_cnn',
    'parallel_cnn'
]  # 'gru',


def main():

    root = 'outputs/baseline_{:%Y%m%d}'.format(dt.datetime.today())
    start = timeit.default_timer()

    for i in range(RUNS):

        for model_name in MODELS:

            output_dir = p.join(root, model_name)
            model_env = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name][0])

            for epc in EXAMPLES_PER_CLASS:

                optim_options = dict(lr=1e-3, beta_1=0.9)
                bs = 32 if epc > 50 else 2
                optim_options['lr'] = 1e-2 if epc > 50 else 1e-3
                optim_options = optim_options if model_name != 'gru' else None

                config_file = REGISTERED_MODELS[model_name][1] + '.yaml'
                config = Config(config_file, overwrite=dict(examples_per_class=epc,
                                                            output_dir=output_dir,
                                                            optimizer_options=optim_options,
                                                            batch_size=bs,
                                                            epochs=EPOCHS))

                del config.callbacks['TensorBoard']

                train_scores = train(config, **model_env.__dict__)

                checkpoint_file = config.callbacks['CustomModelCheckpoint']['checkpoint_file']
                test_scores = evaluate(checkpoint_file, config_file, **model_env.__dict__)

                with open(p.join(root, 'results.txt'), 'a') as logfile:
                    line = 'run_id={},model={},examples_per_class={},'.format(config.run_id, model_name, epc)
                    line += ''.join(['train_{}={},'.format(key, value) for key, value in train_scores.items()])
                    line += ','.join(['test_{}={}'.format(key, value) for key, value in test_scores.items()])
                    line += '\n'
                    logfile.write(line)

    print('Task completed in {}'.format(timeit.default_timer() - start))


if __name__ == '__main__':
    main()
