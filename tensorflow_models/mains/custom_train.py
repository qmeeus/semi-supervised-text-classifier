import argparse
import os.path as p
import importlib
import h5py
from operator import itemgetter

from tensorflow_models.base import Config
from tensorflow_models.mains.train import train, REGISTERED_MODELS
from tensorflow_models.mains.evaluate import evaluate

"""
This script trains a semi-supervised model in two steps:
    1) Pretrain the model unsupervisingly by setting alpha to 0.
    2) Update the value of alpha and retrain the network in a 
       semi-supervised setting
    3) Evaluate the model on the test set
"""


def custom_train(model_name, config_file):

    model_env = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name][0])

    train_config = Config(config_file)
    pretrain_output_dir = p.join(train_config.output_dir, 'pretrain')
    pretrain_config = Config(config_file, overwrite={'output_dir': pretrain_output_dir, 'alpha': 0.})

    pretrain_scores = train(pretrain_config, **model_env.__dict__)
    weights_file = pretrain_config.callbacks['CustomModelCheckpoint']['checkpoint_file']

    train_scores = train(train_config, **model_env.__dict__, checkpoint_file=weights_file)
    weights_file = train_config.callbacks['CustomModelCheckpoint']['checkpoint_file']
    test_scores = evaluate(weights_file, train_config, **model_env.__dict__)

    print('_' * 80)
    score_names = sorted(pretrain_scores.keys())
    print("\t\t" + "\t".join([k.split('_')[0] for k in score_names]))
    print("-" * 80)
    for name, scores in zip(['pretrain', 'train', 'test'], [pretrain_scores, train_scores, test_scores]):
        print("{:8s}\t".format(name) + "\t".join(["{:.4f}".format(scores[k]) for k in score_names]))


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model', choices=list(REGISTERED_MODELS.keys()))
    parser.add_argument('--config', type=str, required=False)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    config_file = args.config
    if not config_file:
        config_file = REGISTERED_MODELS[args.model][1] + '.yaml'
        print('Using default config: {}'.format(args.config))

    custom_train(args.model, config_file)
