from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import reset_graph
from tensorflow_models.base import Config

import warnings

warnings.warn('Deprecated in favor of mains.train', DeprecationWarning)


def run(Generator, Model, Trainer, load_data, settings):
    reset_graph()
    cfg = Config(settings)
    logger = Logger(cfg)
    logger.log_config(cfg)
    data = Generator(load_data, cfg)
    model = Model(cfg)
    trainer = Trainer(data, model, cfg, logger)
    trainer.train()
    trainer.evaluate()


