import os.path as p
import argparse
import importlib
import timeit

from tensorflow_models.base import Config
from .train import build, REGISTERED_MODELS


def evaluate(checkpoint_file, config, **kwargs):

    output_dir = p.join(p.dirname(p.abspath(checkpoint_file)), 'evaluate')

    if isinstance(config, Config):
        config.output_dir = output_dir
    else:
        config = Config(config, {'output_dir': output_dir})

    model, generator, logger = build(config, checkpoint_file=checkpoint_file, **kwargs)

    start = timeit.default_timer()
    results = model.evaluate('test')
    logger.info('runtime: {:.2f}s'.format(timeit.default_timer() - start))
    logger.info('; '.join(['{}: {:.4f}'.format(k, v) for k, v in results.items()]))
    logger.info('results saved in {}'.format(config.output_dir))

    return results


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model', choices=list(REGISTERED_MODELS.keys()))
    parser.add_argument('checkpoint_file', type=Path)
    parser.add_argument('--config', type=str, required=False)
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    if not args.config:
        args.config = REGISTERED_MODELS[args.model][1] + '.yaml'
        print('Using default config: {}'.format(args.config))

    model_env = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[args.model][0])

    evaluate(args.checkpoint_file, args.config, **model_env.__dict__)
