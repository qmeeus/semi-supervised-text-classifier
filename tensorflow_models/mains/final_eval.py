import os.path as p

import pandas as pd
import importlib

from tensorflow_models.base import Config

from .train import train, REGISTERED_MODELS
from .evaluate import evaluate

N_RUNS = 3
OUTPUT_DIR = "outputs/feval"


def load_best_params(filename):
    best_params = pd.read_csv(filename)
    best_params = (best_params
                   .assign(
                        competitive_topk=best_params['competitive_topk'].astype(int),
                        encoder_dims=best_params['encoding_dim'].fillna(0).astype(int).map(lambda d: [d]),
                        penalty_weight=best_params.apply(lambda row: (row['l1'], row['l2']), axis=1))
                   .drop(['encoding_dim', 'f1_score', 'l1', 'l2'], axis=1))

    if 'run' in best_params:
        best_params = best_params[best_params.run]

    return best_params


def main(best_params):
    best_params = load_best_params(best_params)

    for _ in range(N_RUNS):

        for is_unsupervised in (False, True):

            for i, row in best_params.iterrows():

                row = row.copy()
                model_name, epc = row['model_name'], row['examples_per_class']
                row['output_dir'] = OUTPUT_DIR

                if is_unsupervised and (epc > 1 or model_name == 'fc_classifier'):
                    continue

                model_env = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name][0])
                config_file = REGISTERED_MODELS[model_name][1] + '.yaml'

                config = Config(config_file, overwrite=row.to_dict())

                if is_unsupervised:
                    config.alpha = 0.
                    config.save_predictions = False

                if not is_unsupervised and epc < 100:
                    config.batch_size = 2

                train_scores = train(config, **model_env.__dict__)

                checkpoint_file = config.callbacks['CustomModelCheckpoint']['checkpoint_file']
                test_scores = evaluate(checkpoint_file, config, **model_env.__dict__)

                with open('{}/final_eval.csv'.format(OUTPUT_DIR), 'a') as f:
                    line = "run_id={},model_name={},examples_per_class={},".format(config.run_id, model_name, epc)
                    line += "unsupervised={},".format(str(is_unsupervised).lower())
                    line += "".join(["val_{}={},".format(key, value) for key, value in train_scores.items()])
                    line += ",".join(["test_{}={}".format(key, value) for key, value in test_scores.items()])
                    line += "\n"
                    f.write(line)


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('best_params', type=str)
    args = parser.parse_args()

    main(args.best_params)
