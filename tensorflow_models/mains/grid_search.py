import importlib
import argparse
import datetime as dt
import timeit
import itertools
import pandas as pd
from copy import deepcopy

from tensorflow_models.base import Config
from tensorflow_models.mains.train import train, REGISTERED_MODELS


N_RUNS = 1
EPOCHS = 30

MODEL_NAMES = [

    'ssae',
    'cae',
    'kate',
    'ksparse',
    # 'fc_classifier',
    # 'seq_cnn',
    # 'parallel_cnn',

]

PARAM_GRID = {
    'all': [
        {'examples_per_class': [0, 1, 10, 100, 1000]}, 
        {'alpha': [0.0, 0.2, 0.5, 0.8, 1.0]},
        {'encoder_dims': [[128], [256]]},
        {'input_dropout': [0.5, 0.8, 1.0]},  # !!! keep_prob'
    ],
    'ssae': [
        {'l1_penalty': [0., 1e-3, 1e-2]},
        {'l2_penalty': [0., 1e-3, 1e-2]}
    ],
    'cae': [
        {'contractive': [1e-4, 1e-3, 1e-2]}
    ],
    'ksparse': [
        {'competitive_topk': [32, 64]},
    ],
    'kate': [
        {'competitive_topk': [32, 64]},
        {'amplification': [1, 10, 100]}
    ]
}


def grid_search(shuffle=False, maxsize=None, dry_run=False):

    search_space = generate_search_space(shuffle, maxsize)

    print('{} models to train'.format(len(search_space) * N_RUNS))

    if dry_run:
        import ipdb; ipdb.set_trace()
        print(search_space)
        return

    for model_name in MODEL_NAMES:
        config_file = REGISTERED_MODELS[model_name][1] + '.yaml'

        print('Config file: {}'.format(config_file))

        m = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name][0])

        output_dir = 'outputs/grid_search_' + model_name + '_' + '{:%Y%m%d}'.format(dt.datetime.now())

        counter = 0
        start = timeit.default_timer()
        for _ in range(N_RUNS):
            
            selection = (search_space[search_space.model_name == model_name]
                         .drop('model_name', axis=1))

            for i, row in selection.iterrows():

                counter += 1
                settings = row.to_dict()
                config = Config(config_file, dict(output_dir=output_dir, epochs=EPOCHS, **settings))

                del config.callbacks['TensorBoard']
                config.save_encodings = False
                config.topic_strength = False

                run_start = timeit.default_timer()
                results = train(config, **m.__dict__)
                runtime = timeit.default_timer() - run_start

                with open('{}/results.txt'.format(output_dir), 'a') as f:
                    line = "run_id={},".format(config.run_id)
                    line += ",".join(["{}={}".format(k, v) for k, v in settings.items()])
                    line += ",runtime={:.2f},".format(runtime)
                    line += ','.join(['{}={:.4f}'.format(k, v) for k, v in results.items()]) + "\n"
                    f.write(line)

                print('Completed run {}. Accumulated time: {:.2f}s'.format(counter, timeit.default_timer() - start))

        print('Grid search completed in {:.0f}s'.format(timeit.default_timer() - start))


def check_params(search_space):
    if not isinstance(search_space, pd.DataFrame):
        raise ValueError('DataFrame expected')

    params = search_space.copy()
    
    # Define some masks for later
    is_ssae = lambda df: df.model_name == 'ssae'
    is_kcomp = lambda df: df.model_name.str.startswith('k')
    is_supervised = lambda df: df.alpha == 1
    is_unsupervised = lambda df: df.examples_per_class == 0
    has_alpha = lambda df: df.alpha != 0

    # Define function to extract the encoding dim
    encoding_dim = lambda df: df.encoder_dims.map(lambda l: l[-1])

    # Filter out supervised models that are not SSAE
    params = params[~((~is_ssae(params)) & is_supervised(params))]

    # Filter out unsupervised models with alpha > 0 and semi-supervised models with alpha == 0
    params = params[~(is_unsupervised(params) & has_alpha(params))]
    params = params[~(~is_unsupervised(params) & ~has_alpha(params))]

    # Filter out k-competitive models with the wrong dimensions vs. top k
    params = params[~(is_kcomp(params) & ((encoding_dim(params) / params.competitive_topk) != 4))]

    # Merge l1 and l2 penalties
    merge_l1_l2 = lambda row: {"l1": row['l1_penalty'], "l2": row['l2_penalty']}
    params['penalty_weight'] = params.apply(merge_l1_l2, axis=1)
    params = params.drop(['l1_penalty', 'l2_penalty'], axis=1)

    return params

def generate_search_space(shuffle=False, maxsize=None):
    search_space = pd.DataFrame()
    for model_name in MODEL_NAMES:
        param_grid = {}
        for d in [{'model_name': [model_name]}] + PARAM_GRID['all'] + PARAM_GRID[model_name]:
            param_grid.update(d)

        param_names = list(param_grid)
        model_params = pd.DataFrame(
            list(itertools.product(*list(param_grid.values()))), 
            columns=param_names)

        search_space = pd.concat([search_space, model_params], axis=0, sort=False)

    search_space = check_params(search_space)

    if shuffle:
        search_space = search_space.sample(frac=1.)

    if maxsize and maxsize // N_RUNS < len(search_space):
        return (search_space
                .groupby(['model_name', 'examples_per_class'], as_index=False)
                .apply(lambda g: g.sample(min(len(g), maxsize // N_RUNS)))
                .reset_index(drop=True))

    return search_space

def parse_args():
    parser = argparse.ArgumentParser(description='Grid search script')
    parser.add_argument('--maxsize', type=int, help='max size of search space')
    parser.add_argument('--shuffle', action='store_true', help='random search')
    parser.add_argument('--dry-run', action='store_true')
    args = parser.parse_args()
    return args


if __name__ == '__main__':

    args = parse_args()
    grid_search(args.shuffle, args.maxsize, args.dry_run)



# # PARAMETER SEARCH SPACE
# epc_params = {'examples_per_class': [1, 10, 100, 1000]}
# alpha_params = {'alpha': [None, 0.0, 0.2, 0.5, 0.8, 1.0]}
# rloss_params = {'reconstruction_loss': ['kld']}
# closs_params = {'classification_loss': ['categorical_crossentropy']}
# optim_params = {'optimizer': ['adam']}
# lr_params = {'optimizer_options': {'lr': lr for lr in [0.001, 0.01]}}
# # edim_params = {'encoder_dims': [[32], [64], [128], [512], [512,128], [128,64], [64,32], [512,128,64], [128,64,32]]}
# # edim_params = {'encoder_dims': [None, [64], [128], [512]]}
# edim_params = {'encoder_dims': [None, [128], [256]]}
# contr_params = {'contractive': [None, 1e-4, 1e-3, 1e-2]}
# topk_params = {'competitive_topk': [0, 16, 32, 64]}
# ampl_params = {'amplification': [None, 1, 10, 100]}
# # penalty_params = {'penalty': [None, 'l1', 'l2', 'elasticnet']}
# penalty_params = {'penalty': [None, 'elasticnet']}
# l1_params = {'l1_penalty': [None, 0., 1e-3, 1e-2]}
# l2_params = {'l2_penalty': [None, 0., 1e-3, 1e-2]}
# # pweights_params = {'penalty_weight': [None, 0.001, 0.01, 0.1, (0.001, 0.001), (0.01, 0.01), (0.01, 0.001)]}
# dropout_params = {'dropout_rate': [None, 0.2, 0.5, 0.8, 1.0]}  # !!! keep_prob
# input_dropout_params = {'input_dropout': [0.5, 0.8, 1.0]}  # !!! keep_prob
# # fc_units_params = {'fc_layers': [None, [64], [128], [128, 64], [512], [512, 128], [512, 128, 64]]}
# fc_units_params = {'fc_layers': [None, [64], [128], [256], [512]]}

# PARAM_GRID = dict(

#     **epc_params,
#     **alpha_params,
#     **rloss_params,
#     # **closs_params,
#     # **optim_params,
#     # **lr_params,
#     **edim_params,
#     **contr_params,
#     **topk_params,
#     **ampl_params,
#     **penalty_params,
#     **l1_params,
#     **l2_params,
#     # **pweights_paradef check

#     **dropout_params,def check

#     **input_dropout_params,
#     **fc_units_params,

# )

# def check_params(search_space):
#     assert isinstance(search_space, pd.DataFrame)

#     df = search_space.copy()

#     encoding_dim = df.encoder_dims.map(lambda l: l[-1] if l else l)
#     is_clf = df.model_name.isin(['fc_classifier', 'parallel_cnn', 'seq_cnn'])

#     ssae_mask = (
#         ((df.model_name == 'ssae') & ((df.penalty.notnull()) & (df.l1_penalty.notnull()) & (df.l2_penalty.notnull())))
#         | ((df.model_name != 'ssae') & ((df.penalty.isnull()) & (df.l1_penalty.isnull()) & (df.l2_penalty.isnull())) & df.alpha > 0)
#     )

#     cae_mask = (
#         (((df.model_name == 'cae') & (df.contractive.notnull()))
#          | ((df.model_name != 'cae') & (df.contractive.isnull())))
#     )

#     kcomp_mask = (
#         ((df.model_name.str.startswith('k')) & (df.competitive_topk > 0)
#             & (encoding_dim / 4 >= df.competitive_topk))
#         | ((~df.model_name.str.startswith('k')) & (df.competitive_topk == 0))
#     )

#     kate_mask = (
#         ((df.model_name == 'kate') & (df.amplification.notnull()))
#         | ((df.model_name != 'kate') & (df.amplification.isnull()))
#     )

#     clf_mask = (
#         (is_clf & (df.dropout_rate.notnull()) & (df.alpha.isnull())
#             & (df.encoder_dims.isnull()) & df.fc_layers.notnull())
#         | ((~is_clf) & (df.dropout_rate.isnull()) & (df.alpha.notnull())
#            & (df.encoder_dims.notnull()) & df.fc_layers.isnull())
#     )

#     mask = ssae_mask & cae_mask & kcomp_mask & kate_mask & clf_mask
#     return df.where(mask).dropna(how='all', axis=0)


# def generate_search_space(shuffle=False, maxsize=None):

#     param_grid = deepcopy(PARAM_GRID)
#     param_grid['model_name'] = MODEL_NAMES

#     search_space = list(itertools.product(*list(param_grid.values())))
#     param_names = list(param_grid)
#     search_space = pd.DataFrame([{k: v for k, v in zip(param_names, param_values)} for param_values in search_space])
#     search_space = check_params(search_space)
#     assert len(search_space), "Something wrong append: no parameter space to search"

#     search_space['penalty_weight'] = search_space.apply(lambda row: (row['l1_penalty'], row['l2_penalty']), axis=1)
#     search_space = search_space.drop(['l1_penalty', 'l2_penalty'], axis=1)

#     search_space['competitive_topk'] = search_space['competitive_topk'].astype(int)
#     search_space['examples_per_class'] = search_space['examples_per_class'].astype(int)

#     print('Search space: {} models'.format(len(search_space)))

#     if shuffle:
#         search_space = search_space.sample(frac=1.)

#     if maxsize and maxsize // N_RUNS < len(search_space):
#         return (search_space
#                 .groupby(['model_name', 'examples_per_class'], as_index=False)
#                 .apply(lambda g: g.sample(min(len(g), maxsize // N_RUNS)))
#                 .reset_index(drop=True))

#     return search_space