import tensorflow as tf

from tensorflow_models.semi_supervised.ssvae import *
from tensorflow_models.base import Config
from tensorflow_models.utils import Logger
from tests.helper_functions import _start_shell


def main():
    # Define model parameters and options in dictionary of flags
    config = Config("ssvae.yaml")
    logger = Logger(config)

    # Initialize model
    model = Model(config, logger)

    _start_shell(locals())

    # Specify number of training steps
    training_steps = model.generator.training_steps

    # Define feed dictionary and loss name for EarlyStoppingHook
    loss_name = "loss_stopping:0"
    start_step = config.early_stopping['early_stopping_start']
    stopping_step = config.early_stopping['early_stopping_step']
    tolerance = config.early_stopping['early_stopping_tol']

    # Define saver which only keeps previous 3 checkpoints (default=10)
    scaffold = tf.train.Scaffold(saver=tf.train.Saver(max_to_keep=3))

    # Initialize TensorFlow monitored training session
    with tf.train.MonitoredTrainingSession(
            checkpoint_dir=config.checkpoints['checkpoint_dir'],
            hooks=[tf.train.StopAtStepHook(last_step=training_steps),
                   EarlyStoppingHook(loss_name, tolerance=tolerance, stopping_step=stopping_step,
                                     start_step=start_step)],
            save_summaries_steps=None, save_summaries_secs=None, save_checkpoint_secs=None,
            save_checkpoint_steps=config.checkpoints['checkpoint_step'], scaffold=scaffold) as sess:
        # Set model session
        model.set_session(sess)

        # Train model
        model.train()

    logger.info("[ TRAINING COMPLETE ]")

    # Create new session for model evaluation
    with tf.Session() as sess:
        # Restore network parameters from latest checkpoint
        saver = tf.train.Saver()
        saver.restore(sess, tf.train.latest_checkpoint(config.checkpoints['checkpoint_dir']))

        # Set model session using restored sess
        model.set_session(sess)

        # # Plot final predictions
        # model.plot_predictions("final")

        # Reinitialize dataset handles
        model.reinitialize_handles()

        # Evaluate model
        print("[ Evaluating Model ]")
        t_loss, v_loss = model.evaluate()

        print("\n\n[ Final Evaluations ]")
        print("Training loss: %.5f" % (t_loss))
        print("Validation loss: %.5f\n" % (v_loss))


# Run main() function when called directly
if __name__ == '__main__':
    main()
