from tensorflow_models.unsupervised.symae import *
from .default_main import run


if __name__ == '__main__':
    run(Generator, Model, Trainer, load_data, settings)
