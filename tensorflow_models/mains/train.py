import os
import argparse
import importlib
import timeit
import keras.backend as K
from pathlib import Path

from tensorflow_models.utils import Logger
from tensorflow_models.base import Config

#! TODO: Find the commit before everything failed...
# TODO: Implement pretraining when the weights are not specified
# TODO: This means building an unsupervised version of the model
# TODO: and training it, then clear the session and build the real
# TODO: model with the pretrained weights 

REGISTERED_MODELS = {

    # model_name: (path, default_config_name)
    'fc_classifier': ('supervised.fc_classifier', 'fc_classifier'),
    'seq_cnn': ('supervised.seq_cnn', 'seq_cnn'),
    'parallel_cnn': ('supervised.parallel_cnn', 'parallel_cnn'),
    'lstm': ('supervised.gated_rnn', 'lstm'),
    'gru': ('supervised.gated_rnn', 'gru'),

    'kate': ('semi_supervised.kate', 'sskate'),
    'cae': ('semi_supervised.kate', 'cae'),
    'ksparse': ('semi_supervised.kate', 'ksparse'),
    'attention': ('semi_supervised.kate', 'attention'),
    'ssae': ('semi_supervised.kate', 'ssae'),

    'autoencoder': ('unsupervised.autoencoder', 'autoencoder'),
    'tied_ae': ('unsupervised.tied_ae', 'autoencoder'),
    'ukate': ('unsupervised.kate', 'kate'),
    'dcae': ('unsupervised.deep_clustering_ae2', 'deep_clustering_ae'),

}


def build(config, checkpoint_file=None, pretrain=False, pretrain_weights=None, **kwargs):

    K.clear_session()

    try:
        load_data = kwargs.pop('load_data')
        Generator = kwargs.pop('Generator')
        Model = kwargs.pop('Model')
    except Exception as e:
        raise AttributeError('Did not find {} in the model\'s __init__ file'.format(e))

    logger = Logger(config)
    logger.log_config(config)
    generator = Generator(load_data, config, logger)

    model = Model(generator, config, logger)

    if checkpoint_file:
        logger.info('Loading from checkpoint: {}'.format(checkpoint_file))
        load_func = model.load_weights if checkpoint_file.endswith('h5') else model.load
        load_func(checkpoint_file)
    elif pretrain and pretrain_weights:
        logger.info('Loading pretrained weights: {}'.format(pretrain_weights))
        model.load_weights(pretrain_weights, "unsupervised_ae")
    elif pretrain:
        raise NotImplementedError('Must give pretrain weights because I am'
            'too lazy to implement the full pipe at the moment')

    return model, generator, logger


def train(config, **kwargs):

    if not isinstance(config, Config):
        config = Config(config)

    model, _, logger = build(config, **kwargs)

    try:
        start = timeit.default_timer()
        model.train()
        logger.info('runtime: {:.2f}s'.format(timeit.default_timer() - start))
    except KeyboardInterrupt:
        pass
    finally:
        # import ipdb; ipdb.set_trace()
        results = model.evaluate(subset='validation')
        logger.info('; '.join(['{}: {:.4f}'.format(k, v) for k, v in results.items()]))
        logger.info('results saved in {}'.format(config.output_dir))

    return results


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model', choices=list(REGISTERED_MODELS.keys()))
    parser.add_argument('-o', '--checkpoint-file', type=lambda p: Path(p).absolute(), required=False)
    parser.add_argument('-c', '--config', type=str, required=False)
    parser.add_argument('-p', '--pretrain', action='store_true')
    parser.add_argument('-w', '--pretrain_weights', type=lambda p: Path(p).absolute())
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    args = parse_args()

    config_file = args.config or REGISTERED_MODELS[args.model][1] + '.yaml'
    print('Using config file: {}'.format(config_file))
    model_env = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[args.model][0])
    train(config_file, **model_env.__dict__, 
          checkpoint_file=args.checkpoint_file, 
          pretrain=args.pretrain, 
          pretrain_weights=args.pretrain_weights)
