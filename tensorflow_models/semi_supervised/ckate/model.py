import numpy as np
import keras.backend as K
from keras.layers import Dense, Lambda, Conv1D, MaxPooling1D, UpSampling1D, Reshape, multiply
from keras.layers import GlobalAveragePooling1D, GlobalMaxPooling1D
from tensorflow_models.utils.custom_layers import KCompetitive
from tensorflow_models.semi_supervised.kate import Model as BaseModel


class AutoEncoder(BaseModel):
    """
    TODO
    Convolutional auto-encoder that processes documents as sequences
    """

    def __init__(self, generator, config, logger):
        self.encoder_filters = config.encoder_filters
        self.encoder_kernels = config.encoder_kernels
        self.strides = config.strides
        self.encoder_pool_sizes = config.encoder_pool_sizes
        self.reduction_type = config.reduction_type
        super(AutoEncoder, self).__init__(generator, config, logger)

    def build_encoder(self, X, activation):

        X = K.expand_dims(X, -1)

        self.encoder_layers = []
        conv_args = [self.encoder_filters, self.encoder_kernels, self.encoder_pool_sizes]
        for i, (nf, ks, ps) in enumerate(zip(*conv_args)):
            self.encoder_layers.append(
                Conv1D(filters=nf, kernel_size=ks, strides=self.strides,
                       # input_shape=(self.input_size,1) if i == 0 else None,
                       activation=activation,
                       padding='same',
                       kernel_initializer='glorot_normal',
                       name='encoder_layer{}'.format(i + 1)))

            X = self.encoder_layers[-1](X)

            self.encoder_layers.append(
                MaxPooling1D(pool_size=ps, padding='same'))

            X = self.encoder_layers[-1](X)

        import ipdb; ipdb.set_trace()
        # flat_dim = np.prod(X.shape.as_list()[1:])
        # X = Reshape((flat_dim,))(X)

        if self.reduction_type == 'average':
            X = GlobalAveragePooling1D()(X)
        elif self.reduction_type == 'max':
            X = GlobalMaxPooling1D()(X)
        else:
            raise TypeError('Unknown reduction type: {}'.format(self.reduction_type))

        if self.use_attention:
            self.logger.debug('add attention layer')
            attention_probs = Dense(int(X.shape[1]), activation='softmax', name='attention_vec')(X)
            X = multiply([X, attention_probs], name='attention_mul')

        if self.competitive_topk:
            self.logger.debug('add k-competitive layer')
            X = KCompetitive(self.competitive_topk, self.competitive_type)(X)

        X = Lambda(lambda x: x, name="encoding")(X)
        return X

    def build_decoder(self, X, activation):
        X = K.expand_dims(X, -1)

        self.decoder_layers = []
        conv_args = [self.encoder_filters, self.encoder_kernels, self.encoder_pool_sizes]
        for i, (nf, ks, ps) in enumerate(reversed(list(zip(*conv_args)))):
            self.decoder_layers.append(
                Conv1D(filters=nf, kernel_size=ks, strides=self.strides,
                       activation=activation,
                       name='decoder_layer{}'.format(i + 1)))

            X = self.decoder_layers[-1](X)

            self.decoder_layers.append(
                UpSampling1D(size=ps))

            X = self.decoder_layers[-1](X)

        flat_dim = np.prod(X.shape.as_list()[1:])
        X = Reshape((flat_dim,))(X)
        X = Lambda(lambda x: x, name="reconstruction")(X)

        return X
