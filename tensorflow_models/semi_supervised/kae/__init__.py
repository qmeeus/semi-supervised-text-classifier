from .model import AutoEncoder as Model
from tensorflow_models.data_loaders import TfIdfGenerator as Generator
from tensorflow_models.data_loaders import load_newsgroups as load_data

import warnings

warnings.warn('Dev was paused (see unsupervised deep clustering AE for semi-working clustering model)')


__all__ = [
    'Model',
    'Generator',
    'load_data',
]