from keras.layers import Input, Lambda
from keras.models import Model
from keras import optimizers
from keras import callbacks
from sklearn.cluster import KMeans

from tensorflow_models.unsupervised.kate import Model as BaseModel
from tensorflow_models.utils.nn_utils import CustomModelCheckpoint
from tensorflow_models.utils.custom_layers import KMeansLayer
from tensorflow_models.utils.np_utils import target_distribution


class AutoEncoder(BaseModel):
    """
    Auto-encoder for topic modeling
    """

    def __init__(self, generator, config, logger):
        super(AutoEncoder, self).__init__(generator, config, logger)

    def kmeans(self, X):
        self.kmeans_layer = KMeansLayer(self.generator.n_classes)
        clusters = self.kmeans_layer(X)
        clusters = Lambda(lambda x: x, name='clusters')(clusters)
        return clusters

    def build(self):
        # this is our input placeholder
        input_layer = Input(shape=(self.input_size,), name='document_input')

        activations = {'kcomp': 'tanh', 'ksparse': 'linear', 'default': 'relu'}

        try:
            activation = activations[self.competitive_type or 'default']
            self.logger.debug('Activation for encoder layer: {}'.format(activation))
        except KeyError:
            raise Exception('unknown ctype: %s' % self.competitive_type)

        encoded = self.encoder(input_layer, activation=activation)
        decoded = self.decoder(encoded, activation='sigmoid')
        clusters = self.kmeans(encoded)

        # autoencoder model: input --> reconstruction
        self.autoencoder = Model(inputs=input_layer, outputs=[decoded, clusters])

        # encoder model: input --> encoded representation
        self.encoder = Model(inputs=input_layer, outputs=encoded)

        self.clustering = Model(inputs=input_layer, outputs=clusters)

        self.autoencoder.summary(print_fn=self.logger.info)

    def train(self):

        update_interval = 1
        self.alpha = 0.1

        X_train, y_train, _ = self.generator.train
        X_val, y_val, _ = self.generator.validation

        optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)

        self.autoencoder.compile(
            optimizer=optimizer,
            loss=self.build_loss(),
            loss_weights=[1 - self.alpha, self.alpha])

        initial_encoding = self.encoder.predict(X_train)
        initial_weights = KMeans(n_clusters=self.generator.n_classes, n_init=20).fit(initial_encoding).cluster_centers_
        self.kmeans_layer.set_weights([initial_weights])

        for epoch in range(self.epochs):
            if epoch % update_interval == 0:
                q = self.clustering.predict(X_train)
                p = target_distribution(q)

            history = self.autoencoder.fit(X_train, [X_train, p],
                                           epochs=1,
                                           batch_size=self.batch_size,
                                           shuffle=True,
                                           validation_data=(X_val, [X_val, y_val]),
                                           callbacks=self.get_callbacks(),
                                           verbose=0)

            metrics_names = self.autoencoder.metrics_names
            self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])

    def build_loss(self):
        self.logger.debug('Using semi-supervised loss, alpha: {}'.format(self.alpha))
        reconstruction_loss = super(AutoEncoder, self).build_loss()

        # cluster_loss = cluster_distance(self.kmeans_layer.clusters)
        cluster_loss = 'kld'
        return [reconstruction_loss, cluster_loss]

    def get_callbacks(self):
        cbks = []
        for name, options in self.callback_options.items():
            if name == 'CustomModelCheckpoint':
                options['model'] = self.autoencoder
                cbks.append(CustomModelCheckpoint(**options))
            else:
                cbks.append(getattr(callbacks, name)(**options))
        return cbks

    def evaluate(self):

        # X_test, y_test, _ = self.generator.test
        #
        # predictions = self.classifier.predict(X_test, batch_size=self.batch_size)
        #
        # y_pred = np.argmax(predictions, 1)
        # y_test = np.argmax(y_test, 1)
        # cr = metrics.classification_report(y_test, y_pred, target_names=self.generator.target_names)
        # [self.logger.info(line) for line in cr.split('\n')]
        #
        # if self.topic_strength:
        #     save_topics_strength(
        #         self.autoencoder,
        #         self.generator.vectorizer.get_feature_names(),
        #         self.output_dir + "/topics.txt",
        #         self.words_per_topic)
        #
        # return metrics.f1_score(y_test, y_pred, average='macro')
        return {}
