from keras.layers import Input, Dense, Lambda, Activation
from keras.models import Model
from keras import optimizers

from tensorflow_models.utils.ops import masked_supervised_loss, masked_accuracy as accuracy
from tensorflow_models.utils.evaluation import evaluate_autoencoder, evaluate_classifier, evaluate_clusters
from tensorflow_models.unsupervised.kate import Model as BaseModel


class AutoEncoder(BaseModel):
    """
    Auto-encoder for semi-supervised topic modeling
    """

    def __init__(self, generator, config, logger):
        self.classification_loss = config.classification_loss
        self.alpha = config.get('alpha')
        self.n_classes = generator.n_classes
        self.save_predictions = config.get('save_predictions', False)
        self.save_clusters = config.get('save_clusters', False)
        super(AutoEncoder, self).__init__(generator, config, logger)

    def build_classifier(self, X):
        self.classifier_layer = Dense(self.n_classes,
                                      activation=None,
                                      kernel_initializer='glorot_normal',
                                      name='classifier_layer')

        logits = self.classifier_layer(X)
        logits = Lambda(lambda x: x, name="logits")(logits)
        predictions = Activation('softmax')(logits)
        return logits, predictions

    def build(self, print_summary=True):
        # this is our input placeholder
        input_layer = Input(shape=(self.input_size,), name='document_input')

        encoded = self.build_encoder(input_layer)
        decoded = self.build_decoder(encoded)

        # encoder model: input --> encoded representation
        self.encoder = Model(inputs=input_layer, outputs=encoded, name="encoder")

        encoded = self.encoder.get_layer('encoder_layer{}'.format(len(self.encoder_dims))).output
        logits, predictions = self.build_classifier(encoded)

        self.unsupervised_ae = Model(inputs=input_layer, outputs=decoded, name="autoencoder")

        # autoencoder model: input --> reconstruction, logits
        self.autoencoder = Model(inputs=input_layer, outputs=[decoded, logits])

        # classifier: input --> predictions
        self.classifier = Model(inputs=input_layer, outputs=predictions, name="classifier")

        self.autoencoder.summary(print_fn=self.logger.info)

    def get_network_inputs(self, subset='train'):
        # X_train, y_train, _ = (self.generator.make_dataset('train', one_shot=True)
        #                        .make_one_shot_iterator()
        #                        .get_next('train'))
        #
        # X_val, y_val, _ = (self.generator.make_dataset('valid', one_shot=True)
        #                    .make_one_shot_iterator()
        #                    .get_next('valid'))
        #
        # trs, vas, _ = self.generator.get_n_steps()
        X, y, _ = getattr(self.generator, subset)
        return dict(inputs=X, outputs=[X, y])

    def build_loss(self):
        self.logger.debug('Using semi-supervised loss, alpha: {}'.format(self.alpha))
        reconstruction_loss = super(AutoEncoder, self).build_loss()
        supervised_loss = masked_supervised_loss(self, self.classification_loss)
        return [reconstruction_loss, supervised_loss]

    def compile(self):
        optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)

        self.autoencoder.compile(
            optimizer=optimizer,
            loss=self.build_loss(),
            loss_weights=[1 - self.alpha, self.alpha],
            metrics=self.get_metrics()
        )

    def get_metrics(self):
        output_names = self.autoencoder.output_names

        cmetrics = [accuracy]
        rmetrics = []
        return {name: metrics_list for name, metrics_list in zip(output_names, [rmetrics, cmetrics])}

    def evaluate(self, subset='test'):

        data = self.get_network_inputs(subset)
        X, (_, y) = data['inputs'], data['outputs']

        unsupervised_scores = evaluate_autoencoder(self, X, self.save_encodings, self.topic_strength,
                                                   self.save_clusters, labels=y)

        supervised_scores = evaluate_classifier(self, X, y, self.save_predictions)

        return dict(**supervised_scores, **unsupervised_scores)
