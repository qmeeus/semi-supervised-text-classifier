from tensorflow_models.data_loaders import load_newsgroups as load_data
from tensorflow_models.data_loaders import BOWGenerator as Generator

from .trainer import Trainer
from .model import Model

import warnings

warnings.warn('Deprecated model (uses old version of tensorflow)', DeprecationWarning)

settings = 'ssae.yaml'

__all__ = [
    "Trainer",
    "Model",
    "Generator",
    "load_data",
    "settings",
]