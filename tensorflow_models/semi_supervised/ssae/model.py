import tensorflow as tf

from tensorflow_models.supervised.fc_classifier_tf import Model as Classifier
from tensorflow_models.unsupervised.symae import Model as AutoEncoder


class Model(AutoEncoder, Classifier):

    def __init__(self, config):
        Classifier.__init__(self, config)
        AutoEncoder.__init__(self, config)
        self.alpha = tf.constant(config.alpha)

    def __call__(self, X, y, mask):

        n_outputs = y.get_shape().as_list()[1]

        with tf.variable_scope('semi_supervised_autoencoder'):
            encoding = self.encoder(X)
            decoded = self.decoder(encoding)
            clf_outputs = self.classifier(encoding, n_outputs)

            losses = self.build_loss(X, decoded, clf_outputs['logits'], y, mask)
            metrics = self.get_metrics(X, decoded, mask)

        outputs = dict(encoding=encoding, reconstruction=decoded, **clf_outputs)

        for family, d in zip(['loss', 'metrics'], [losses, metrics]):
            for k, v in d.items():
                tf.summary.scalar(k, v, family=family)

        for k, v in outputs.items():
            tf.summary.histogram(k, v, family='output')

        return outputs, losses, metrics

    def build_loss(self, X, decoded, labels, logits, mask):

        encoder_loss = AutoEncoder.build_loss(self, X, decoded)
        unsupervised_loss = encoder_loss.pop('loss')

        classifier_loss = Classifier.build_loss(self, logits, labels, mask)
        supervised_loss = classifier_loss.pop('loss')

        with tf.variable_scope('loss', reuse=tf.AUTO_REUSE):
            total_loss = unsupervised_loss + self.alpha * supervised_loss

        losses = dict(**classifier_loss, **encoder_loss)
        losses['loss'] = total_loss
        losses['supervised_loss'] = supervised_loss
        losses['unsupervised_loss'] = unsupervised_loss

        return losses

    def get_metrics(self, labels, predictions, mask):
        return Classifier.get_metrics(self, labels, predictions, mask)
