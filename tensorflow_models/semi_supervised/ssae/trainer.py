import numpy as np
import tensorflow as tf
from tqdm import tqdm
from sklearn.metrics import classification_report

from tensorflow_models.utils.plotting import confusion_matrix
from tensorflow_models.unsupervised.autoencoder_tf import Trainer as AETrainer
from tensorflow_models.supervised.fc_classifier_tf import Trainer as CTrainer


class Trainer(AETrainer, CTrainer):

    def fetch_tensors(self):
        train = [
            self.training_op, 
            self.losses['loss'],
            self.losses['supervised_loss'],
            self.losses['reconstruction_loss'],
            self.metrics['accuracy'],
            self.summary]
        return train, train[1:]

    def train_one_epoch(self, epoch, summary_writer):
        loss, sloss, rloss, summaries = 0, 0, 0, []
        self.train_init.run()
        for _ in tqdm(range(self.train_steps)):
            _, ploss, psloss, prloss, acc, summary = self.sess.run(self.train_fetch)
            loss += ploss / self.train_steps
            sloss += psloss / self.train_steps
            rloss += prloss / self.train_steps
            summary_writer.add_summary(summary, epoch)

        val_loss, val_sloss, val_rloss, val_summaries = 0, 0, 0, []
        self.valid_init.run()
        for _ in tqdm(range(self.valid_steps)):
            ploss, psloss, prloss, vacc, summary = self.sess.run(self.val_fetch)
            val_loss += ploss / self.valid_steps
            val_sloss += psloss / self.train_steps
            val_rloss += prloss / self.valid_steps
            summary_writer.add_summary(summary, epoch)

        line = "{}/{}".format(epoch, self.epochs)
        line += "[Train Loss: {:.4f} CE: {:.4f} RL: {:.4f} Acc: {:.4f}] ".format(loss, sloss, rloss, acc)
        line += "[Val Loss: {:.4f} CE: {:.4f} RL: {:.4f} Acc: {:.4f}] ".format(val_loss, val_sloss, val_rloss, vacc)
        self.logger.info(line)
        self.saver.save(self.sess, self.checkpoint_file)

    def evaluate(self):

        with tf.Session() as self.sess:
            self.saver.restore(self.sess, self.checkpoint_file)
            self.test_init.run()
            fetch_test = [*self.inputs, self.outputs['predictions']]
            labels, predictions = [], []
            for _ in tqdm(range(self.test_steps)):
                _, batch_labels, _, batch_predictions = self.sess.run(fetch_test)
                labels.extend(batch_labels)
                predictions.extend(batch_predictions)
                
            labels = np.argmax(labels, 1)
            predictions = np.argmax(predictions, 1)
            confusion_matrix(labels, predictions, save_directory=self.output_dir)
            cr = classification_report(labels, predictions)
            [self.logger.info(line) for line in cr.split('\n')]

    @staticmethod
    def check_config(config):
        if config.training_mode != 'semi-supervised':
            raise TypeError("Invalid config for this trainer")
