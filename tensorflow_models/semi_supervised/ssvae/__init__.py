from tensorflow_models.base import EarlyStoppingHook
from .model import Model

import warnings

warnings.warn('Deprecated model (uses old version of tensorflow)', DeprecationWarning)


__all__ = [
    "EarlyStoppingHook",
    "Model",
]