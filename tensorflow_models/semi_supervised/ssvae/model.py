import tensorflow as tf
# from tensorflow.keras import layers
import numpy as np
import os

from tensorflow_models.data_loaders import BOWGenerator, load_newsgroups
from tensorflow_models.utils.nn_utils import get_optimizer


class Model:
    
    def __init__(self, config, logger):

        self.logger = logger
        self.log_dir = config.log_dir

        self.summary_step = 100
        self.display_step = 1

        self.epochs = config.epochs
        self.batch_size = config.batch_size
        self.learning_rate = config.learning_rate
        self.lr_options = config.lr_options
        self.optimizer = get_optimizer(config.optimizer)
        self.optimizer_options = config.optimizer_options

        self.encoding_dim = config.encoding_dim

        self.weight_init = tf.truncated_normal_initializer(stddev=0.02)

        self.generator = BOWGenerator(load_newsgroups, config)
        self.initialize_datasets()
        self.global_step = tf.train.get_or_create_global_step()
        self.build_model()

    def initialize_datasets(self, stopping_size=14000):

        self.dataset = (self.generator.make_dataset('train')
                             .batch(self.batch_size)
                             .repeat(self.epochs)
                             .prefetch(self.batch_size * 5)
                             .make_one_shot_iterator())

        self.vdataset = (self.generator.make_dataset('valid')
                             .batch(self.batch_size)
                             .repeat(self.epochs)
                             .prefetch(self.batch_size * 5)
                             .make_one_shot_iterator())

        self.edataset = (self.generator.make_dataset('test')
                             .batch(self.batch_size)
                             .repeat(stopping_size)
                             .prefetch(self.batch_size * 5)
                             .make_one_shot_iterator())

    def set_session(self, sess):
        self.sess = sess

    def reinitialize_handles(self):
        self.training_handle = self.sess.run(self.dataset.string_handle())
        self.validation_handle = self.sess.run(self.vdataset.string_handle())

    def encoder(self, x, training=True, reuse=None, name='encoder'):
        import ipdb; ipdb.set_trace()

        x = tf.expand_dims(x, -1)

        # [None, 28, 28, 1]  -->  [None, 14, 14, 64]
        h = tf.layers.conv1d(x, 32, kernel_size=4, strides=2, activation=tf.nn.leaky_relu, kernel_initializer=self.weight_init, reuse=reuse, name='e_conv_1')

        # [None, 14, 14, 64] -->  [None, 7, 7, 128]
        h = tf.layers.conv1d(h, 64, kernel_size=4, strides=2, activation=tf.nn.leaky_relu, kernel_initializer=self.weight_init, reuse=reuse, name='e_conv_2')
        h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='e_bn_1')
        h = tf.nn.leaky_relu(h)

        # h = tf.layers.conv1d(h, 128, kernel_size=4, strides=2, activation=tf.nn.leaky_relu, kernel_initializer=self.weight_init, reuse=reuse, name='e_conv_3')
        # h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='e_bn_1')
        # h = tf.nn.leaky_relu(h)

        # # [None, 7, 7, 128]  -->  [None, 7*7*128]
        h = tf.reshape(h, [-1, np.prod(h.shape.as_list()[1:])])

        # [None, 7*7*128] -->  [None, 1024]
        h = tf.layers.dense(h, 1024, activation=None, kernel_initializer=self.weight_init, reuse=reuse, name='e_dense_1')
        h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='e_bn_2')
        h = tf.nn.leaky_relu(h)

        # [None, 1024] -->  [None, 2*self.encoding_dim]
        h = tf.layers.dense(h, 2 * self.encoding_dim, activation=None, kernel_initializer=self.weight_init, reuse=reuse, name='e_dense_2')

        # Assign names to final outputs
        mean = tf.identity(h[:, :self.encoding_dim], name=name+"_mean")
        log_sigma = tf.identity(h[:, self.encoding_dim:], name=name+"_log_sigma")
        return mean, log_sigma

    # Decoder component of VAE model
    def decoder(self, z, training=True, reuse=None, name='decoder'):

        # [None, encoding_dim]  -->  [None, 1024]
        h = tf.layers.dense(z, 1024, activation=None, kernel_initializer=self.weight_init, reuse=reuse, name='d_dense_1')
        h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='d_bn_1')
        h = tf.nn.relu(h)
        
        # [None, 1024]  -->  [None, 7*7*128]
        h = tf.layers.dense(h, 12500, activation=None, kernel_initializer=self.weight_init, reuse=reuse, name='d_dense_2')
        h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='d_bn_2')
        h = tf.nn.relu(h)

        # # [None, 7*7*128]  -->  [None, 7, 7, 128]
        # h = tf.reshape(h, [-1, self.min_res, self.min_res, self.min_chans])

        # [None, 7, 7, 128]  -->  [None, 14, 14, 64]
        h = tf.layers.conv2d_transpose(h, 64, kernel_size=(4,1), strides=2, padding='same', activation=None, kernel_initializer=self.weight_init, reuse=reuse, name='d_tconv_1')
        h = tf.layers.batch_normalization(h, scale=True, momentum=0.9, epsilon=1e-5, training=training, reuse=reuse, name='d_bn_3')
        h = tf.nn.relu(h)
                        
        # [None, 14, 14, 64]  -->  [None, 28, 28, 1]
        h = tf.layers.conv2d_transpose(h, 1, kernel_size=(4,1), strides=2, padding='same', activation=tf.nn.sigmoid, kernel_initializer=self.weight_init, reuse=reuse, name='d_tconv_2')
                        
        # Assign name to final output
        return tf.identity(h, name=name)

    # Sample from multivariate Gaussian
    def sample_gaussian(self, mean, log_sigma, name=None):
        epsilon = tf.random_normal(tf.shape(log_sigma))
        return tf.identity(mean + epsilon * tf.exp(log_sigma), name=name)

    # Define sampler for generating self.z values
    def sample_z(self, batch_size):
        return np.random.normal(size=(batch_size, self.encoding_dim))

    # Compute marginal likelihood loss
    def compute_ml_loss(self, data, pred, name=None):
        ml_loss = -tf.reduce_mean(tf.reduce_sum(data*tf.log(pred) + (1 - data)*tf.log(1 - pred), [1, 2, 3]), name=name)
        return ml_loss

    # Compute Kullback–Leibler (KL) divergence
    def compute_kl_loss(self, mean, log_sigma, name=None):
        kl_loss = 0.5*tf.reduce_mean(tf.reduce_sum(tf.square(mean) + \
                                                   tf.square(tf.exp(log_sigma)) - \
                                                   2.*log_sigma - 1., axis=[-1]), name=name)
        return kl_loss

    # Evaluate model on specified batch of data
    def evaluate_model(self, data, reuse=None, training=True, suffix=None):
        
        # Encode input images
        mean, log_sigma = self.encoder(data, training=training, reuse=reuse, name="encoder" + ("_" + suffix if suffix else ""))

        # Sample latent vector
        z_sample = self.sample_gaussian(mean, log_sigma, name="latent_vector" + ("_" + suffix if suffix else ""))

        # Decode latent vector back to original image
        pred = self.decoder(z_sample, training=training, reuse=reuse, name="pred" + ("_" + suffix if suffix else ""))

        # Compute marginal likelihood loss
        ml_loss = self.compute_ml_loss(data, pred, name="ml_loss" + ("_" + suffix if suffix else ""))
        
        # Compute Kullback–Leibler (KL) divergence
        kl_loss = self.compute_kl_loss(mean, log_sigma, name="kl_loss" + ("_" + suffix if suffix else ""))
                
        # Define loss according to the evidence lower bound objective (ELBO)
        loss = tf.add(ml_loss, kl_loss, name="loss" + ("_" + suffix if suffix else ""))

        return pred, loss, ml_loss, kl_loss
        
    # Define graph for model
    def build_model(self):
        """
        Network model adapted from VAE.py file in GitHub repo by 'hwalsuklee':
        https://github.com/hwalsuklee/tensorflow-generative-model-collections/blob/master/VAE.py
        """
        # Define placeholder for noise vector
        self.z = tf.placeholder(tf.float32, [None, self.encoding_dim], name='z')

        # Define placeholder for dataset handle (to select training or validation)
        self.dataset_handle = tf.placeholder(tf.string, shape=[], name='dataset_handle')
        self.iterator = tf.data.Iterator.from_string_handle(self.dataset_handle, self.dataset.output_types, self.dataset.output_shapes)
        self.data, _, _ = self.iterator.get_next()

        # Define learning rate with exponential decay
        self.learning_rt = tf.train.exponential_decay(self.learning_rate, self.global_step, **self.lr_options)

        # Define placeholder for training status
        self.training = tf.placeholder(tf.bool, name='training')

        # Compute predictions and loss for training/validation datasets
        self.pred, self.loss, self.ml_loss, self.kl_loss = self.evaluate_model(self.data, training=self.training)

        # Compute predictions and loss for early stopping checks
        self.epred, self.eloss, _, _ = self.evaluate_model(self.edataset.get_next(), reuse=True, training=False, suffix="_stopping")

        # Define optimizer for training
        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            self.optim = self.optimizer(self.learning_rt, **self.optimizer_options).minimize(self.loss, global_step=self.global_step)
        
        # Define summary operations
        loss_sum = tf.summary.scalar("loss", self.loss)
        kl_loss_sum = tf.summary.scalar("kl_loss", self.kl_loss)
        ml_loss_sum = tf.summary.scalar("ml_loss", self.ml_loss)
        self.merged_summaries = tf.summary.merge([loss_sum, kl_loss_sum, ml_loss_sum])
        
        # Compute predictions from random samples in latent space
        self.pred_sample = self.decoder(self.z, training=False, reuse=True, name="sampling_decoder")

        # Resize original images and predictions for plotting
        # self.resized_data = tf.image.resize_images(self.data, [self.plot_res, self.plot_res])
        # self.resized_pred = tf.image.resize_images(self.pred, [self.plot_res, self.plot_res])
        # self.resized_imgs = tf.image.resize_images(self.pred_sample, [self.plot_res, self.plot_res])

    def train(self):

        # Define summary writer for saving log files (for training and validation)
        self.writer = tf.summary.FileWriter(os.path.join(self.log_dir, 'training/'), graph=tf.get_default_graph())
        self.vwriter = tf.summary.FileWriter(os.path.join(self.log_dir, 'validation/'), graph=tf.get_default_graph())

        # Show list of all variables and total parameter count
        self.logger.analyze_vars(tf.trainable_variables(), print_info=True)
        self.logger.info("[ Initializing Variables ]")
        
        # Get handles for training and validation datasets
        self.reinitialize_handles()

        # Iterate through training steps
        while not self.sess.should_stop():

            # Update global step            
            step = tf.train.global_step(self.sess, self.global_step)

            # Break if early stopping hook requests stop after sess.run()
            if self.sess.should_stop():
                break

            # Specify feed dictionary
            fd = {self.dataset_handle: self.training_handle, self.training: True,
                  self.z: np.zeros([self.batch_size, self.encoding_dim])}

            # Save summaries, display progress, and update model
            if (step % self.summary_step == 0) and (step % self.display_step == 0):
                fetches = [self.merged_summaries, self.kl_loss, self.ml_loss, self.loss, self.optim]
                summary, kl_loss, ml_loss, loss, _ = self.sess.run(fetches, feed_dict=fd)
                self.logger.info("Step %d:  %.10f [kl_loss]   %.10f [ml_loss]   %.10f [loss] " %(step, kl_loss, ml_loss, loss))
                self.writer.add_summary(summary, step)
                self.writer.flush()
            # Save summaries and update model
            elif step % self.summary_step == 0:
                summary, _ = self.sess.run([self.merged_summaries, self.optim], feed_dict=fd)
                self.writer.add_summary(summary, step)
                self.writer.flush()
            # Display progress and update model
            elif step % self.display_step == 0:
                fetches = [self.kl_loss, self.ml_loss, self.loss, self.optim]
                kl_loss, ml_loss, loss, _ = self.sess.run(fetches, feed_dict=fd)
                self.logger.info("Step %d:  %.10f [kl_loss]   %.10f [ml_loss]   %.10f [loss] " %(step, kl_loss, ml_loss, loss))
            # Update model
            else:
                self.sess.run([self.optim], feed_dict=fd)

            # # Plot predictions
            # if step % self.plot_step == 0:
            #     self.plot_predictions(step)
            #     self.plot_comparisons(step)

            # Break if early stopping hook requests stop after sess.run()
            if self.sess.should_stop():
                break

            # Save validation summaries
            if step % self.summary_step == 0:
                fd = {self.dataset_handle: self.validation_handle, self.z: np.zeros([self.batch_size, self.encoding_dim]),
                      self.training: False}
                vsummary = self.sess.run(self.merged_summaries, feed_dict=fd)
                self.vwriter.add_summary(vsummary, step)
                self.vwriter.flush()
            
    # # Define method for computing model predictions
    # def predict(self, random_samples=False):
    #     if random_samples:
    #         fd = {self.z: self.sample_z(self.batch_size), self.training: False}
    #         return self.sess.run(self.resized_imgs, feed_dict=fd)
    #     else:
    #         fd = {self.dataset_handle: self.validation_handle, self.z: np.zeros([self.batch_size, self.encoding_dim]),
    #               self.training: False}
    #         data, pred = self.sess.run([self.resized_data, self.resized_pred], feed_dict=fd)
    #         return data, pred
    #
    # # Plot generated images for qualitative evaluation
    # def plot_predictions(self, step):
    #     plot_subdir = self.plot_dir + str(step) + "/"
    #     checkFolders([self.plot_dir, plot_subdir])
    #     resized_imgs = self.predict(random_samples=True)
    #     for n in range(0, self.batch_size):
    #         plot_name = 'plot_' + str(n) + '.png'
    #         plt.imsave(os.path.join(plot_subdir, plot_name), resized_imgs[n,:,:,0], cmap='gray')
    #
    # # Plot true and predicted images
    # def plot_comparisons(self, step):
    #     plot_subdir = self.plot_dir + str(step) + "/"
    #     checkFolders([self.plot_dir, plot_subdir])
    #     resized_data, resized_pred = self.predict()
    #     for n in range(0, self.batch_size):
    #         data_name = 'data_' + str(n) + '.png'; pred_name = 'pred_' + str(n) + '.png'
    #         plt.imsave(os.path.join(plot_subdir, data_name), resized_data[n,:,:,0], cmap='gray')
    #         plt.imsave(os.path.join(plot_subdir, pred_name), resized_pred[n,:,:,0], cmap='gray')

    # Compute cumulative loss over multiple batches
    def compute_cumulative_loss(self, loss, loss_ops, dataset_handle, batches):
        for n in range(0, batches):
            fd = {self.dataset_handle: dataset_handle, self.training: False}
            current_loss = self.sess.run(loss_ops, feed_dict=fd)
            loss = np.add(loss, current_loss)
            self.logger.info('Batch {0} of {1}\r'.format(n+1, batches))
        return loss
            
    # Evaluate model
    def evaluate(self):
        t_batches = int(self.generator.n_examples / self.batch_size)
        v_batches = int(len(self.generator.validation[0]) / self.batch_size)
        self.logger.info("Training dataset:")
        training_loss = self.compute_cumulative_loss([0.], [self.loss], self.training_handle, t_batches)

        self.logger.info("Validation dataset:")
        validation_loss = self.compute_cumulative_loss([0.], [self.loss], self.validation_handle, v_batches)

        training_loss = training_loss / t_batches
        validation_loss = validation_loss / v_batches
        return training_loss, validation_loss
