from keras.layers import Dense, Dropout, Input, Activation, BatchNormalization
from keras.models import Model
from keras import optimizers
import keras.backend as K

from tensorflow_models.utils.plotting import plot_history
from tensorflow_models.utils.evaluation import evaluate_classifier
from tensorflow_models.base import BaseModel


class Classifier(BaseModel):

    """
    Fully connected supervised classifier
    """

    name = 'fc_classifier'

    def __init__(self, generator, config, logger):
        self.output_dim = generator.n_classes
        self.input_shape = tuple(generator.output_shapes[0])
        self.loss = config.loss
        self.fc_layers = config.fc_layers
        self.return_logits = config.get('return_logits', False)
        self.fc_activation = config.get('fc_activation')
        self.dropout_rate = config.get('dropout_rate')
        self.batch_norm = config.get('batch_norm')
        self.bn_options = config.get('bn_options', {})
        self.save_predictions = config.get('save_predictions', False)
        super(Classifier, self).__init__(generator, config, logger)

    def build_classifier(self, X):

        # Fully connected layers
        for i, units in enumerate(self.fc_layers):
            X = Dense(units, activation=None, use_bias=not self.batch_norm, name='dense_{}'.format(i))(X)

            # Batch normalisation
            if self.batch_norm:
                X = BatchNormalization(**self.bn_options, name='bn_{}'.format(i))(X)

            # If the dropout rate is a list, dropout is applied to all layers (if it evaluates to true)
            # If the dropout rate is a float, then it is applied to the last layer only
            dropout = self.dropout_rate if not type(self.dropout_rate) == list else self.dropout_rate[i]
            if dropout and (type(self.dropout_rate) == list or i == len(self.fc_layers) - 1):
                X = Dropout(dropout, name='dropout_{}'.format(i))(X)

            if self.fc_activation:
                X = Activation(self.fc_activation, name=self.fc_activation + '_' + str(i))(X)

        output = Dense(self.output_dim, activation=None, name='logits')(X)

        if not self.return_logits:
            output = Activation('softmax')(output)

        return output

    def build(self):

        input_layer = Input(shape=self.input_shape, dtype='float32')
        output = self.build_classifier(input_layer)

        self.classifier = Model(inputs=input_layer, outputs=output)

        self.classifier.summary()

    def train(self):

        train_data = self.get_network_inputs('train')
        val_data = self.get_network_inputs('validation')

        self.logger.debug('Start training...')
        history = self.classifier.fit(train_data['inputs'], train_data['outputs'],
                                      epochs=self.epochs,
                                      batch_size=self.batch_size,
                                      shuffle=True,
                                      validation_data=(val_data['inputs'], val_data['outputs']),
                                      callbacks=self.get_callbacks())

        metrics_names = self.classifier.metrics_names
        self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])
        plot_history(history.history, save_directory=self.output_dir)
        return history

    def compile(self):
        optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)

        self.classifier.compile(
            optimizer=optimizer,
            loss=self.loss,
            metrics=['acc'])

        self.logger.debug('Model was compiled successfully')

    def get_network_inputs(self, subset='train'):
        X, y = self.generator.get_dataset(2, subset)
        return dict(inputs=X, outputs=y)

    def evaluate(self, subset='test'):
        data = self.get_network_inputs(subset)
        return evaluate_classifier(self, data['inputs'], data['outputs'], self.save_predictions)

