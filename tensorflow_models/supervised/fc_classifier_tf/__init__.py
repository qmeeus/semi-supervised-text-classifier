from tensorflow_models.data_loaders import load_newsgroups as load_data
from tensorflow_models.data_loaders import BOWGenerator as Generator

from .model import Model
from .trainer import Trainer

import warnings

warnings.warn('This model is deprecated (old tensorflow)', DeprecationWarning)

settings = "fc_classifier.yaml"


__all__ = [
    "Model",
    "Trainer",
    "Generator",
    "load_data",
    "settings",
]