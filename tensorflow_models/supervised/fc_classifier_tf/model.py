import tensorflow as tf
import tensorflow.contrib.layers as tcl

from tensorflow_models.base import BaseModel


class Model(BaseModel):

    name = 'classifier'

    def __init__(self, config):

        self.fc_layers = config.fc_layers
        
        self.activation = tf.nn.relu
        self.initializer = tcl.variance_scaling_initializer()

    def __call__(self, X, y, mask=None):

        with tf.variable_scope(self.name, reuse=tf.AUTO_REUSE):

            output_dim = int(y.get_shape()[1])

            outputs = self.classifier(X, output_dim)
            losses = self.build_loss(y, outputs['logits'])
            metrics = self.get_metrics(y, outputs['predictions'])

        for family, d in zip(['loss', 'metrics'], [losses, metrics]):
            for k, v in d.items():
                tf.summary.scalar(k, v, family=family)

        for k, v in outputs.items():
            tf.summary.histogram(k, v, family='output')

        return outputs, losses, metrics

    def build_loss(self, labels, logits, mask=None):

        with tf.variable_scope('loss'):

            if mask is None:
                mask = tf.ones_like(labels[:, 0])

            loss = tf.losses.softmax_cross_entropy(labels, logits, weights=mask)

        return {'loss': loss}

    def get_metrics(self, labels, predictions, mask=None):
        _, accuracy = tf.metrics.accuracy(tf.argmax(labels, 1), tf.argmax(predictions, 1), weights=mask)
        return {'accuracy': accuracy}
    
    def classifier(self, previous, output_dim):
        with tf.variable_scope('network'):

            for i, neurons in enumerate(self.fc_layers):
                previous = tf.layers.dense(
                    previous, neurons,
                    activation=self.activation,
                    kernel_initializer=self.initializer,
                    name='layer_{}'.format(i+1))

            logits = tf.layers.dense(
                previous, output_dim,
                activation=None,
                kernel_initializer=self.initializer,
                name='logits')

            pred = tf.nn.softmax(logits, name='predictions')

        return {'logits': logits, 'predictions': pred}
