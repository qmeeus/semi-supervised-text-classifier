import numpy as np
import tensorflow as tf
from tqdm import tqdm
from sklearn.metrics import classification_report

from tensorflow_models.utils.plotting import confusion_matrix
from tensorflow_models.base import BaseTrainer


# TODO: check how to handle train and validation summaries


class Trainer(BaseTrainer):

    def fetch_tensors(self):
        train = [
            self.training_op, 
            self.losses['loss'],
            self.metrics['accuracy'],
            self.summary]
        return train, train[1:]

    def train_one_epoch(self, epoch, summary_writer):
        loss, acc, summaries = 0, 0, []
        self.train_init.run()
        for i in tqdm(range(self.train_steps)):
            _, ploss, pacc, summary = self.sess.run(self.train_fetch)
            loss += ploss / self.train_steps
            acc = pacc # / self.train_steps
            summary_writer.add_summary(summary, i)

        val_loss, val_acc, val_summaries = 0, 0, []
        self.valid_init.run()
        for i in tqdm(range(self.valid_steps)):
            ploss, pacc, summary = self.sess.run(self.val_fetch)
            val_loss += ploss / self.valid_steps
            val_acc = pacc # / self.valid_steps
            summary_writer.add_summary(summary, i)

        line = "{}/{}".format(epoch, self.epochs)
        line += "[Train Total loss: {:.4f} acc: {:.4f}] ".format(loss, acc)
        line += "[Val Total loss: {:.4f} acc: {:.4f}] ".format(val_loss, val_acc)
        self.logger.info(line)
        self.saver.save(self.sess, self.checkpoint_file)
        # summary_writer.add_summary(summary, epoch)

    def evaluate(self):

        with tf.Session() as self.sess:
            self.saver.restore(self.sess, self.checkpoint_file)
            self.test_init.run()
            fetch_test = [*self.inputs, self.outputs['predictions']]
            labels, predictions = [], []
            for _ in tqdm(range(self.test_steps)):
                _, batch_labels, batch_predictions = self.sess.run(fetch_test)
                labels.extend(batch_labels)
                predictions.extend(batch_predictions)

            labels = np.argmax(labels, 1)
            predictions = np.argmax(predictions, 1)
            confusion_matrix(labels, predictions, save_directory=self.output_dir)
            cr = classification_report(labels, predictions)
            [self.logger.info(line) for line in cr.split('\n')]

    @staticmethod
    def check_config(config):
        if config.examples_per_class == 0 or config.training_mode != 'supervised':
            raise TypeError("Invalid config for this trainer")
