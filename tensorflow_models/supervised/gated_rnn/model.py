from keras.layers import Input, TimeDistributed, Dense, Bidirectional, Activation
from keras.layers import CuDNNGRU as GRU, CuDNNLSTM as LSTM
from keras.models import Model

from tensorflow_models.supervised.fc_classifier import Model as BaseModel

# TODO Bidirectional, TimeDistributed, classifier layers
# TODO Check this: https://machinelearningmastery.com/develop-bidirectional-lstm-sequence-classification-python-keras/
# TODO and this: https://machinelearningmastery.com/sequence-classification-lstm-recurrent-neural-networks-python-keras/


class GatedRNN(BaseModel):

    """
    Gated recurrent classifier with memory units (LSTM or GRU)
    """

    name = 'gated_rnn'

    def __init__(self, generator, config, logger):
        self.train_embeddings = config.train_embeddings
        self.memory_type = config.memory_type
        self.recurrent_units = config.recurrent_units
        self.recurrent_dropout = config.recurrent_dropout
        self.return_sequences = config.return_sequences
        self.bidirectional = config.bidirectional
        super(GatedRNN, self).__init__(generator, config, logger)

    def build_classifier(self, X):

        # Recurrent layer
        MemoryCell = LSTM if self.memory_type == 'lstm' else GRU

        recurrent_layer = MemoryCell(self.recurrent_units,
                                     # recurrent_dropout=self.recurrent_dropout,
                                     # return_sequences=self.return_sequences
                                     )

        if self.bidirectional:
            recurrent_layer = Bidirectional(recurrent_layer)

        X = recurrent_layer(X)

        output_layer = Dense(self.generator.n_classes)

        # if self.return_sequences:
        #     output_layer = TimeDistributed(output_layer)

        output = output_layer(X)

        if not self.return_logits:
            output = Activation('softmax')(output)

        return output

    def build(self):
        input_layer = Input(shape=self.input_shape, dtype='int32')
        embedded_sequence = self.generator.emb_loader.get_keras_layer(self.train_embeddings)(input_layer)
        predictions = self.build_classifier(embedded_sequence)

        self.classifier = Model(inputs=input_layer, outputs=predictions)

        self.classifier.summary(print_fn=self.logger.info)
