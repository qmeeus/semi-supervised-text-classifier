from keras.layers import Conv1D, MaxPooling1D, Concatenate

from tensorflow_models.supervised.seq_cnn import Model as BaseModel


class ParallelCNN(BaseModel):

    """
    Parallel CNN
    """

    name = 'parallel_cnn'

    def __init__(self, generator, config, logger):
        self.parallel_filters = config.concat_filters
        self.parallel_fs = config.concat_filter_sizes
        self.parallel_ps = config.concat_pool_size

        super(ParallelCNN, self).__init__(generator, config, logger)

    def build_classifier(self, X):
        # Parallel CNN with multiple filter sizes
        convs = []
        for pflist, fslist, pslist in zip(self.parallel_filters, self.parallel_fs, self.parallel_ps):
            convs.append(X)
            for n_filters, filter_size, pool_size in zip(pflist, fslist, pslist):
                l_conv = Conv1D(n_filters, filter_size, activation='relu', padding=self.padding)(convs[-1])
                l_pool = MaxPooling1D(pool_size, padding=self.padding)(l_conv)
                convs[-1] = l_pool

        # Merge layer
        X = Concatenate(axis=1)(convs)

        return super(ParallelCNN, self).build_classifier(X)


