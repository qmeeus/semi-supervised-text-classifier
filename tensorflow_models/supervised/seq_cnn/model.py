from keras.layers import Conv1D, MaxPooling1D, Flatten, Input
from keras.models import Model

from tensorflow_models.supervised.fc_classifier import Model as BaseModel


class SeqCNN(BaseModel):

    """
    Sequence convolutional classifier
    """

    name = "seq_cnn"

    def __init__(self, generator, config, logger):
        self.train_embeddings = config.train_embeddings
        self.conv_filters = config.conv_filters
        self.filter_sizes = config.filter_sizes
        self.pool_sizes = config.pool_sizes
        self.padding = config.padding
        super(SeqCNN, self).__init__(generator, config, logger)

    def build_classifier(self, X):

        # Convolutional layers
        for n_filters, filter_size, pool_size in zip(self.conv_filters, self.filter_sizes, self.pool_sizes):
            X = Conv1D(n_filters, filter_size, activation='relu', padding=self.padding)(X)
            X = MaxPooling1D(pool_size, padding=self.padding)(X)

        X = Flatten()(X)
        return super(SeqCNN, self).build_classifier(X)

    def build(self):

        input_layer = Input(shape=self.input_shape, dtype='int32')
        embedded_sequence = self.generator.emb_loader.get_keras_layer(self.train_embeddings)(input_layer)
        predictions = self.build_classifier(embedded_sequence)

        self.classifier = Model(inputs=input_layer, outputs=predictions)
        self.classifier.summary(print_fn=self.logger.info)
