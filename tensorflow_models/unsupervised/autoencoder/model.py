from keras.layers import Input, Dense, Dropout
from keras.models import Model
from keras import optimizers, losses, regularizers

from tensorflow_models.utils.ops import orthogonal_regularizer, build_contractive_loss
from tensorflow_models.base import BaseModel
from tensorflow_models.utils.evaluation import evaluate_autoencoder


class AutoEncoder(BaseModel):
    """
    Auto-encoder for topic modeling
    """

    def __init__(self, generator, config, logger):
        self.training_mode = config.training_mode
        self.input_size = generator.num_words
        self.encoder_dims = config.encoder_dims
        self.input_dropout = config.get('input_dropout')
        self.dropout = config.get('dropout')
        self.reconstruction_loss = config.reconstruction_loss

        self.penalty = config.get('penalty')
        self.penalty_weight = config.get('penalty_weight')
        if self.penalty not in (None, 'elasticnet', 'orthogonal', 'contractive'):
            raise TypeError('Invalid parameter: {}'.format(self.penalty))
        if self.penalty and not self.penalty_weight:
            raise TypeError('Must specify penalty_weight if penalty is not none')
        if self.penalty == 'elasticnet' and type(self.penalty_weight) is not dict:
            raise TypeError(
                'Elasticnet takes two arguments, one for l1 and one for l2. Got {}'
                .format(self.penalty_weight))

        # FIXME: ugly solution - checks if the children class defines it (for KATE)
        # IDEA: In own method self.get_activation?
        if not hasattr(self, 'activation'):
            self.activation = config.activation
        if self.penalty == 'contractive' and self.activation != 'sigmoid':
            raise TypeError('Contractive loss expects a sigmoid activation in the encoding layer')

        self.output_activation = config.output_activation

        self.topic_strength = config.get('topic_strength')
        self.words_per_topic = config.get('words_per_topic')
        self.save_encodings = config.get('save_encodings')
        self.save_clusters = config.get('save_clusters')

        super(AutoEncoder, self).__init__(generator, config, logger)

    def build_encoder(self, input_layer):

        encoded = input_layer if not self.input_dropout else Dropout(self.input_dropout)(input_layer)

        kernel_regulariser = self.get_regularizer()

        self.encoder_layers = []
        for i, units in enumerate(self.encoder_dims):
            is_coding = i + 1 == len(self.encoder_dims)

            self.encoder_layers.append(
                Dense(units,
                      activation=self.activation,
                      kernel_initializer='glorot_normal',
                      kernel_regularizer=kernel_regulariser,
                      activity_regularizer=regularizers.l1() if (is_coding and self.activation == 'relu') else None,
                      name='encoding' if is_coding else 'encoder_layer{}'.format(i + 1)))

            encoded = self.encoder_layers[-1](encoded)

            if self.dropout:
                encoded = Dropout(self.dropout)(encoded)

        return encoded

    def build_decoder(self, encoded_layer):

        Xhat = encoded_layer
        self.decoder_layers = []
        for i, units in enumerate(reversed([self.input_size] + self.encoder_dims[:-1])):
            is_output = i + 1 < len(self.encoder_dims)
            self.decoder_layers.append(
                Dense(units,
                      activation=self.output_activation if is_output else self.activation,
                      kernel_initializer='glorot_normal',
                      name='reconstruction' if is_output else 'decoder_layer{}'.format(i + 1)))

            Xhat = self.decoder_layers[-1](Xhat)

        return Xhat

    def build(self):
        input_layer = Input(shape=(self.input_size,), name='document_input')
        encoded_input = Input(shape=(self.encoder_dims[-1],), name='encoded_input')

        # encoder model: input --> encoded representation
        encoded_document = self.build_encoder(input_layer)
        self.encoder = Model(input_layer, encoded_document, name="encoder")

        # decoder model: encoded representation --> reconstruction
        reconstruction = self.build_decoder(encoded_input)
        self.decoder = Model(encoded_input, reconstruction, name="decoder")

        # autoencoder model: input --> reconstruction
        reconstruction = self.decoder(encoded_document)
        self.autoencoder = Model(inputs=input_layer, outputs=reconstruction, name="autoencoder")

        self.autoencoder.summary(print_fn=self.logger.info)

    def train(self):
        training_data = self.get_network_inputs('train')
        validation_data = self.get_network_inputs('validation')

        history = self.autoencoder.fit(training_data['inputs'], training_data['outputs'],
                                       epochs=self.epochs,
                                       batch_size=self.batch_size,
                                       shuffle=True,
                                       validation_data=(validation_data['inputs'], validation_data['outputs']),
                                       callbacks=self.get_callbacks())

        metrics_names = self.autoencoder.metrics_names
        self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])

    def get_network_inputs(self, subset='train'):
        X, _, _ = getattr(self.generator, subset)
        return dict(inputs=X, outputs=X)

    def build_loss(self):
        if self.penalty == 'contractive':
            self.logger.debug('Using contractive loss, lambda: {}'.format(self.penalty_weight))
            reconstruction_loss = build_contractive_loss(self, self.reconstruction_loss, self.penalty_weight)
        else:
            reconstruction_loss = losses.get(self.reconstruction_loss)

        return reconstruction_loss

    def compile(self):
        optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)

        self.autoencoder.compile(
            optimizer=optimizer,
            loss=self.build_loss())

    def evaluate(self, subset='test'):

        X, y = self.generator.get_dataset(2, subset)
        results = evaluate_autoencoder(self, X, self.save_encodings, self.topic_strength, self.save_clusters, labels=y)
        return results

    def get_regularizer(self):
        if self.penalty == 'orthogonal':
            return orthogonal_regularizer(self.penalty_weight)
        elif self.penalty == 'elasticnet':
            return regularizers.l1_l2(**self.penalty_weight)
        else:
            return
