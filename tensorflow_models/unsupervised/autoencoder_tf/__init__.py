from tensorflow_models.data_loaders import load_newsgroups as load_data
from tensorflow_models.data_loaders import BOWGenerator as Generator

from tensorflow_models.unsupervised.autoencoder_tf.trainer import Trainer
from .model import Model

import warnings

warnings.warn('This model is deprecated (old tensorflow)', DeprecationWarning)

settings = 'autoencoder_old.yaml'

__all__ = [
    "Trainer",
    "Model",
    "Generator",
    "load_data",
    "settings",
]

