import tensorflow as tf
import tensorflow.contrib.layers as tcl

from tensorflow_models.base import BaseModel


class Model(BaseModel):

    name = 'autoencoder'

    def __init__(self, config):
        self.encoder_layers = config.encoder_layers
        self.encoding_dim = config.encoding_dim
        self.keep_prob = config.keep_prob

        self.activation = tf.nn.elu
        self.initializer = tcl.variance_scaling_initializer()
        self.regularizer = tcl.l2_regularizer(**config.regularizer_options)

    def __call__(self, X):

        with tf.variable_scope(self.name):

            n_outputs = X.get_shape().as_list()[1]
            encoding = self.encoder(X)
            decoded = self.decoder(encoding, n_outputs)

            losses = self.build_loss(X, decoded)
            metrics = self.get_metrics(X, decoded)

        outputs = {'encoding': encoding, 'reconstruction': decoded}

        for family, d in zip(['loss', 'metrics'], [losses, metrics]):
            for k, v in d.items():
                tf.summary.scalar(k, v, family=family)

        for k, v in outputs.items():
            tf.summary.histogram(k, v, family='output')

        return outputs, losses, metrics

    def encoder(self, previous):
        
        with tf.variable_scope('encoder'):
            previous = tf.nn.dropout(previous, keep_prob=self.keep_prob)
            for neurons in self.encoder_layers + [self.encoding_dim]:
                previous = tf.layers.dense(
                    previous, neurons, 
                    activation=self.activation, 
                    kernel_initializer=self.initializer, 
                    kernel_regularizer=self.regularizer)
        
        return previous

    def decoder(self, previous, n_outputs):
    
        with tf.variable_scope('decoder'):
            for neurons in reversed(self.encoder_layers):
                previous = tf.layers.dense(
                    previous, neurons, 
                    activation=self.activation, 
                    kernel_initializer=self.initializer, 
                    kernel_regularizer=self.regularizer)

            y = tf.layers.dense(
                previous, n_outputs,
                activation=None,  
                kernel_initializer=self.initializer, 
                kernel_regularizer=self.regularizer)
        
        return y

    def build_loss(self, X, decoded):
        with tf.variable_scope('loss', reuse=tf.AUTO_REUSE):
            with tf.variable_scope('mean_squared_error'):
                reconstruction_loss = tf.reduce_mean(tf.square(decoded - X))
            
            with tf.variable_scope('regularization_loss'):
                reg_loss = tf.add_n(tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES))
    
            loss = reconstruction_loss + reg_loss
            
        return {'loss': loss, 'reconstruction_loss': reconstruction_loss, 'reg_loss': reg_loss}

    def get_metrics(self, inputs, reconstruction):
        return {}
        