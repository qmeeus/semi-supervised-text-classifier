import tensorflow as tf
from tqdm import tqdm

from tensorflow_models.base import BaseTrainer


class Trainer(BaseTrainer):

    def fetch_tensors(self):
        train = [
            self.training_op, 
            self.losses['loss'], 
            self.losses['reconstruction_loss'], 
            self.summary]
        return train, train[1:]

    def train_one_epoch(self, epoch, summary_writer):
        loss, rloss, summaries = 0, 0, []
        self.train_init.run()
        for _ in tqdm(range(self.train_steps)):
            _, ploss, prloss, summary = self.sess.run(self.train_fetch)
            loss += ploss / self.train_steps
            rloss += prloss / self.train_steps
            summaries.append(summary)

        val_loss, val_rloss, val_summaries = 0, 0, []
        self.valid_init.run()
        for _ in tqdm(range(self.valid_steps)):
            ploss, prloss, summary = self.sess.run(self.val_fetch)
            val_loss += ploss / self.valid_steps
            val_rloss += prloss / self.valid_steps

        line = "{}/{}".format(epoch, self.epochs)
        line += "[Train Total loss: {:.4f} MSE: {:.4f}] ".format(loss, rloss)
        line += "[Val Total loss: {:.4f} MSE: {:.4f}] ".format(val_loss, val_rloss)
        self.logger.info(line)
        self.saver.save(self.sess, self.checkpoint_file)
        summary_writer.add_summary(summary, epoch)

    def evaluate(self):

        with tf.Session() as self.sess:
            self.saver.restore(self.sess, self.checkpoint_file)
            self.test_init.run()
            fetch_test = [*self.inputs, self.outputs['reconstruction']]
            mse = 0
            for _ in tqdm(range(self.test_steps)):
                batch_mse = self.sess.run(self.losses['reconstruction_loss'])
                mse += batch_mse / self.test_steps

            [self.logger.info("Final Evaluation: MSE: {:.4f}".format(mse))]

    @staticmethod
    def check_config(config):
        if config.training_mode != 'unsupervised':
            raise TypeError("Invalid config for this trainer")
