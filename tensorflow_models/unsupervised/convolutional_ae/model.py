from keras.layers import Conv2D, Conv2DTranspose, Dropout, MaxPool2D, UpSampling2D

from tensorflow_models.unsupervised.autoencoder import Model as BaseModel


class ConvolutionalAE(BaseModel):

    def __init__(self, generator, config, logger):
        super().__init__(generator, config, logger)
        self.kernel_sizes = config.kernel_size
        self.pool_sizes = config.pool_sizes
        self.filters = config.filters

    def build_encoder(self, input_layer):
        if self.input_dropout:
            encoded = Dropout(self.input_dropout)(input_layer)
        else:
            encoded = input_layer

        n_layers = len(self.filters)
        assert n_layers == len(self.pool_sizes) == len(self.kernel_sizes)

        self.encoder_layers = []
        for i, (nf, ks, ps) in enumerate(zip(self.filters, self.kernel_sizes, self.pool_sizes)):
            self.encoder_layers.append(Conv2D(
                nf, ks, 
                padding="same", 
                activation=self.activation, 
                name='encoder_layer{}_conv'.format(i+1)
            ))

            encoded = self.encoder_layers[-1](encoded)
            
            if ps:
                self.encoder_layers.append(MaxPool2D(
                    ps, padding="same", name='encoder_layer{}_pool'.format(i+1)
                ))
                encoder = self.encoder_layers[-1](encoded)

            if self.dropout and not (i+1 == n_layers):
                self.encoder_layer.append(Dropout(
                    self.dropout, 
                    name="encoder_layer{}_dropout".format(i+1)))
                encoder = self.encoder_layers[-1](encoded)

        return encoded

    def build_decoder(self, encoded_layer):
        reconstruction = encoded_layer
        self.decoder_layers = []
        n_layers = len(self.filters)
        for i, (nf, ks, ps) in enumerate(reversed(zip(self.filters, self.kernel_sizes, self.pool_sizes))):
            self.decoder_layers.append(Conv2DTranspose(
                nf, ks, 
                padding="same", 
                activation=self.output_activation if (i+1 == n_layers) else self.activation, 
                name="decoder_layer{}_deconv".format(i+1))
            )
            reconstruction = self.decoder_layers[-1](reconstruction)

            if ps:
                self.decoder_layers.append(UpSampling2D(
                    ps, name='decoder_layer{}_upsampling'.format(i+1)))
                
                reconstruction = self.decoder_layers[-1](reconstruction)

            
