import numpy as np
from tqdm import tqdm
from keras.layers import Input
from keras.models import Model
from keras import optimizers, losses
from sklearn.metrics import normalized_mutual_info_score as nmi, adjusted_rand_score as ari
from sklearn.cluster import KMeans

from tensorflow_models.utils.ops import build_pairwise_kld_loss
from tensorflow_models.utils.custom_layers import KMeansLayer
from tensorflow_models.unsupervised.tied_ae import Model as BaseModel
from tensorflow_models.utils.np_utils import clustering_accuracy, target_distribution


class DeepClusteringAE(BaseModel):
    """
    Deep Clustering Auto-Encoder
    """

    def __init__(self, generator, config, logger):
        self.pretrain_options = config.pretrain_options
        self.clustering_loss = config.clustering_loss
        super(DeepClusteringAE, self).__init__(generator, config, logger)

    def build(self):
        input_layer = Input(shape=(self.input_size,), name='document_input')
        encoded_input = Input(shape=(self.encoder_dims[-1],), name='encoded_input')

        # encoder model: input --> encoded representation
        encoded_document = self.build_encoder(input_layer)
        self.encoder = Model(input_layer, encoded_document)

        # decoder model: encoded representation --> reconstruction
        reconstruction = self.build_decoder(encoded_input)
        self.decoder = Model(encoded_input, reconstruction)

        # autoencoder model: input --> reconstruction
        reconstruction = self.decoder(encoded_document)
        self.autoencoder = Model(inputs=input_layer, outputs=reconstruction)
        self.autoencoder.summary(print_fn=self.logger.info)

        # clustering model: input --> clusters
        kmeans_layer = KMeansLayer(self.generator.n_classes, name='kmeans')
        clusters = kmeans_layer(encoded_document)
        self.clustering = Model(inputs=input_layer, outputs=clusters)
        self.clustering.summary(print_fn=self.logger.info)

    def train(self):

        training_data = self.get_network_inputs('train')
        validation_data = self.get_network_inputs('validation')

        # Unsupervised pretraining
        self.logger.info('Pretraining the autoencoder')
        history = self.autoencoder.fit(training_data['inputs'], training_data['inputs'],
                                       validation_data=(validation_data['inputs'], validation_data['inputs']),
                                       callbacks=self.get_callbacks(),
                                       **self.pretrain_options)

        metrics_names = self.autoencoder.metrics_names
        self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])

        # Initialise the weights of the clustering layer
        self.logger.info('Initialise the weights of the clustering layer')
        kmeans = KMeans(self.generator.n_classes, n_init=20)
        yhat = kmeans.fit_predict(self.encoder.predict(training_data['inputs']))
        prev_yhat = yhat.copy()
        self.clustering.get_layer(name='kmeans').set_weights([kmeans.cluster_centers_])

        M = len(training_data['inputs']); update_interval = 1; tol = 1e-3
        loss = 0
        for epoch in range(1, self.epochs + 1):

            if epoch % update_interval == 0:
                q = self.clustering.predict(training_data['inputs'], verbose=0)
                p = target_distribution(q)  # update the auxiliary target distribution p

                # evaluate the clustering performance
                # TODO: evaluate on validation set (solves the problem of argmax with epc < 1.)
                #   atm: set epc=1. and evaluate on training set
                yhat = q.argmax(1)
                if training_data['outputs'][1] is not None:
                    y = training_data['outputs'][1].argmax(1)
                    acc_score = clustering_accuracy(y, yhat)
                    nmi_score = nmi(y, yhat)
                    ari_score = ari(y, yhat)
                    self.logger.info('Iter {}: acc = {:.4f}, nmi = {:.4f}, ari = {:.4f}; loss={:.4f}'
                                     .format(epoch, acc_score, nmi_score, ari_score, loss))
                else:
                    self.logger.info('Iter {}: loss={:4f}'.format(epoch, loss))

                # check stop criterion - model convergence
                delta_label = np.sum(yhat != prev_yhat).astype(np.float32) / yhat.shape[0]
                prev_yhat = np.copy(yhat)
                if epoch > 1 and delta_label < tol:
                    print('delta_label ', delta_label, '< tol ', tol)
                    print('Reached tolerance threshold. Stopping training.')
                    break

            losses = []; index = 0; indices = np.arange(M)

            for _ in tqdm(range(M // self.batch_size), ncols=80, leave=False, disable=None):

                idx = indices[index * self.batch_size: min((index + 1) * self.batch_size, M)]
                losses += [self.clustering.train_on_batch(x=training_data['inputs'][idx], y=p[idx])]
                index = index + 1 if (index + 1) * self.batch_size <= training_data['inputs'].shape[0] else 0

            loss = np.mean(losses)

        self.clustering.save_weights(self.output_dir + '/model_weights.h5')

    def get_network_inputs(self, subset='train'):
        X, y, _ = getattr(self.generator, subset)
        return dict(inputs=X, outputs=[X, y])

    def build_loss(self):
        if self.clustering_loss == 'pairwise_kld':
            return build_pairwise_kld_loss()
        else:
            return losses.get(self.clustering_loss)

    def compile(self):
        # Compile the autoencoder
        optimizer = (optimizers.get(self.pretrain_options.pop('optimizer'))
                     .from_config(self.pretrain_options.pop('optimizer_options')))
        self.autoencoder.compile(optimizer=optimizer, loss=self.pretrain_options.pop('loss'))

        # Compile the clustering model
        self.clustering.compile(optimizer=optimizer, loss=self.build_loss())