import numpy as np
from tqdm import tqdm
from keras.layers import Input
from keras.models import Model
from keras import optimizers, losses
from sklearn.metrics import normalized_mutual_info_score as nmi, adjusted_rand_score as ari
from sklearn.cluster import KMeans

from tensorflow_models.utils.ops import build_pairwise_kld_loss
from tensorflow_models.utils.custom_layers import KMeansLayer
from tensorflow_models.unsupervised.tied_ae import Model as BaseModel
from tensorflow_models.utils.np_utils import clustering_accuracy, target_distribution


class DeepClusteringAE(BaseModel):
    """
    Deep Clustering Auto-Encoder
    """

    def __init__(self, generator, config, logger):
        self.pretrain_options = config.pretrain_options
        self.clustering_loss = config.clustering_loss
        self.clustering_tolerance = config.clustering_tolerance
        self.alpha = config.alpha
        super(DeepClusteringAE, self).__init__(generator, config, logger)

    def build(self):
        input_layer = Input(shape=(self.input_size,), name='document_input')
        encoded_input = Input(shape=(self.encoder_dims[-1],), name='encoded_input')

        # encoder model: input --> encoded representation
        encoded_document = self.build_encoder(input_layer)
        self.encoder = Model(input_layer, encoded_document)

        # decoder model: encoded representation --> reconstruction
        reconstruction = self.build_decoder(encoded_input)
        self.decoder = Model(encoded_input, reconstruction)

        # autoencoder model: input --> reconstruction
        reconstruction = self.decoder(encoded_document)
        self.autoencoder = Model(inputs=input_layer, outputs=reconstruction)
        self.autoencoder.summary(print_fn=self.logger.info)

        # clustering model: input --> clusters
        kmeans_layer = KMeansLayer(self.generator.n_classes, name='kmeans')
        clusters = kmeans_layer(encoded_document)
        self.clustering = Model(inputs=input_layer, outputs=clusters)

        # combined model: input --> reconstruction, clusters
        self.combined = Model(inputs=input_layer, outputs=[reconstruction, clusters])
        self.combined.summary(print_fn=self.logger.info)

        losses = self.build_loss()

    def train(self):

        data = self.get_network_inputs()

        # Unsupervised pretraining
        self.logger.info('Pretraining the autoencoder')
        history = self.autoencoder.fit(data['inputs'], data['outputs'][0],
                                       validation_data=data['validation'],
                                       callbacks=self.get_callbacks(),
                                       **self.pretrain_options)

        metrics_names = self.autoencoder.metrics_names
        self.logger.log_history(history.history, metrics_names + ['val_' + m for m in metrics_names])

        # Initialise the weights of the clustering layer
        self.logger.info('Initialise the weights of the clustering layer')
        kmeans = KMeans(self.generator.n_classes, n_init=20)
        yhat = kmeans.fit_predict(self.encoder.predict(data['inputs']))
        prev_yhat = yhat.copy()
        self.clustering.get_layer(name='kmeans').set_weights([kmeans.cluster_centers_])

        M = len(data['inputs']); loss, rloss, closs = 0, 0, 0
        for epoch in range(1, self.epochs + 1):

            # TODO: This should be moved in own callback and fit should be called rather than train_on_batch
            q = self.clustering.predict(data['inputs'], verbose=0)
            p = target_distribution(q)  # update the auxiliary target distribution p

            # evaluate the clustering performance
            # TODO: evaluate on validation set (solves the problem of argmax with epc < 1.)
            #   a.t.m.: set epc=1. and evaluate on training set
            yhat = q.argmax(1)
            log = 'Iter {}: loss = {:.4f}, closs = {:.4f}, rloss = {:.4f}'.format(epoch, loss, closs, rloss)

            if data['outputs'][1] is not None:
                y = data['outputs'][1].argmax(1)
                acc_score = clustering_accuracy(y, yhat)
                nmi_score = nmi(y, yhat)
                ari_score = ari(y, yhat)
                log += ' acc = {:.4f}, nmi = {:.4f}, ari = {:.4f}; '.format(acc_score, nmi_score, ari_score)

            self.logger.info(log)

            # check stop criterion - model convergence
            delta_label = np.sum(yhat != prev_yhat).astype(np.float32) / yhat.shape[0]
            prev_yhat = np.copy(yhat)
            if epoch > 1 and delta_label < self.clustering_tolerance:
                print('delta_label ', delta_label, '< tol ', self.clustering_tolerance)
                print('Reached tolerance threshold. Stopping training.')
                break

            losses = []; index = 0; indices = np.arange(M)
            for _ in tqdm(range(M // self.batch_size), ncols=80, leave=False, disable=None):

                idx = indices[index * self.batch_size: min((index + 1) * self.batch_size, M)]
                losses += [self.combined.train_on_batch(x=data['inputs'][idx], y=[data['inputs'][idx], p[idx]])]
                index = index + 1 if (index + 1) * self.batch_size <= data['inputs'].shape[0] else 0

            loss, rloss, closs = np.mean(losses, axis=0)

        self.clustering.save_weights(self.output_dir + '/model_weights.h5')

    def get_network_inputs(self):
        X_train, y_train, _ = self.generator.train
        X_val, _, _ = self.generator.validation
        return dict(inputs=X_train, outputs=[X_train, y_train], validation=(X_val, X_val))

    def build_loss(self):
        autoencoder_loss = self.reconstruction_loss
        if self.clustering_loss == 'pairwise_kld':
            clustering_loss = build_pairwise_kld_loss()
        else:
            clustering_loss = losses.get(self.clustering_loss)
        return [autoencoder_loss, clustering_loss]

    def compile(self):
        # Compile the autoencoder
        optimizer = optimizers.get(self.optimizer).from_config(self.optimizer_options)
        self.autoencoder.compile(optimizer=optimizer, loss=losses[0])

        # # Compile the clustering model
        # self.clustering.compile(optimizer=optimizer, loss=self.build_loss())

        # Compile the full model
        self.combined.compile(optimizer=optimizer, loss=losses, loss_weights=[1 - self.alpha, self.alpha])
