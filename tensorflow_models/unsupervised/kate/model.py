from keras.layers import Dense, Lambda, multiply, Dropout

from tensorflow_models.utils.custom_layers import KCompetitive
from tensorflow_models.unsupervised.tied_ae import Model as BaseModel


class AutoEncoder(BaseModel):
    """
    Auto-encoder for semi-supervised text categorisation
    """

    def __init__(self, generator, config, logger):
        self.use_attention = config.get('use_attention')
        self.competitive_topk = config.get('competitive_topk')
        self.competitive_type = config.get('competitive_type')
        self.top_and_bottom_k = config.get('top_and_bottom', True)
        self.amplification = config.get('amplification')

        try:
            activations = {'kcomp': 'tanh', 'ksparse': 'linear', 'default': config.get('activation', 'relu')}
            self.activation = activations[self.competitive_type or 'default']
            logger.debug('Activation for encoder layer: {}'.format(self.activation))
        except KeyError:
            raise TypeError('unknown ctype: {}'.format(self.competitive_type))

        super(AutoEncoder, self).__init__(generator, config, logger)

    def build_encoder(self, X):

        input_dropout = getattr(self, 'input_dropout', None)
        if input_dropout and input_dropout > 0.:
            X = Dropout(input_dropout)(X)

        self.encoder_layers = []
        for i, units in enumerate(self.encoder_dims):
            self.encoder_layers.append(
                Dense(units,
                      activation=self.activation,
                      kernel_initializer='glorot_normal',
                      kernel_regularizer=self.get_regularizer(),
                      name='encoder_layer{}'.format(i + 1)))
            X = self.encoder_layers[-1](X)

        if self.competitive_topk:
            self.logger.debug('add k-competitive layer')
            X = KCompetitive(self.competitive_topk,
                             self.competitive_type,
                             self.top_and_bottom_k,
                             self.amplification)(X)

        if self.use_attention:
            self.logger.debug('add attention layer')
            attention_probs = Dense(int(X.shape[1]), activation='softmax', name='attention_vec')(X)
            X = multiply([X, attention_probs], name='attention_mul')

        X = Lambda(lambda x: x, name="encoding")(X)
        return X

