from tensorflow_models.data_loaders import load_newsgroups as load_data
from tensorflow_models.data_loaders import SkipgramGenerator as Generator
from tensorflow_models.data_loaders import EmbLoader
from tensorflow_models.unsupervised.skipgrams import Trainer
from .model import Model

import warnings

warnings.warn('This model is deprecated (old tensorflow)', DeprecationWarning)

settings = 'skipgrams.yaml'

__all__ = [

    "Trainer",
    "Model",
    "Generator",
    "EmbLoader",
    "load_data",
    "settings",

]
