import numpy as np
import tensorflow as tf

from tensorflow_models.base import BaseModel

# TODO: test
# https://www.tensorflow.org/tutorials/representation/word2vec
# https://github.com/tensorflow/tensorflow/blob/master/tensorflow/examples/tutorials/word2vec/word2vec_basic.py


class Model(BaseModel):

    def __init__(self, config):        
        self.embedding_dim = config.embedding_dim
        self.num_sampled = config.num_sampled
        self.vocabulary_size = config.tokenizer_options['num_words']
        self.embedding_shape = [self.vocabulary_size, self.embedding_dim]

    def __call__(self, sequence_input, context_input):

        context_input = tf.expand_dims(context_input, -1, name='context_input')

        # weights_init = tf.random_uniform(self.embedding_shape, -1., 1.)
        tf_weights_init = tf.placeholder('float32', self.embedding_shape, name='initial_weights')

        word_embeddings = tf.Variable(tf_weights_init, name="word_embeddings")
        weights_init = word_embeddings.initializer


        embed = tf.nn.embedding_lookup(word_embeddings, sequence_input)

        losses = self.build_loss(context_input, embed)
        outputs = {'embedding': embed, 'weights_init': [weights_init, tf_weights_init]}
        metrics = self.get_metrics()
        return outputs, losses, metrics

    def build_loss(self, labels, inputs):

        nce_weights = tf.Variable(
            tf.truncated_normal(
                self.embedding_shape,
                stddev=1. / np.sqrt(self.embedding_dim)),
            name="nce_weights")
        
        nce_biases = tf.Variable(tf.zeros([self.vocabulary_size]), name="nce_biases")
        # Compute the NCE loss, using a sample of the negative labels each time.
        nce_loss = tf.reduce_mean(tf.nn.nce_loss(
            weights=nce_weights,
            biases=nce_biases,
            labels=labels,
            inputs=inputs,
            num_sampled=self.num_sampled,
            num_classes=self.vocabulary_size), name='nce_loss')

        return {'loss': nce_loss}

    def get_metrics(self):
        return {}
