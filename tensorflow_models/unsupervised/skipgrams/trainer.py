import tensorflow as tf
from tqdm import tqdm

from tensorflow_models.base import BaseTrainer
from tensorflow_models.utils.nn_utils import get_optimizer

class Trainer(BaseTrainer):

    def build_model(self, model):
        self.outputs, self.losses, self.metrics = model(*self.inputs)

        self.logger.analyze_vars(tf.trainable_variables(), print_info=True, tensorboard=True)

        with tf.variable_scope('train'):
            self.optimizer = get_optimizer(self.optimizer_name)(**self.optimizer_options)
            self.training_op = self.optimizer.minimize(self.losses['loss'])
            self.init_ops = [tf.global_variables_initializer(), tf.local_variables_initializer()]

        self.summary = tf.summary.merge_all()
        self.saver = tf.train.Saver()
        self.train_fetch, self.val_fetch = self.fetch_tensors()

    def fetch_tensors(self):
        train = [
            self.training_op, 
            self.losses['loss'],
            # self.summary
        ]
        return train, train[1:]

    def train(self):
        self.logger.info('Start training')
        summary_writer = tf.summary.FileWriter(self.log_dir, graph=tf.get_default_graph())

        with tf.Session() as self.sess:
            w_init, tf_w_init = self.outputs['weights_init']
            w_init.run(feed_dict={tf_w_init: self.generator.emb_loader.build_embedding()})
            [init_op.run() for init_op in self.init_ops]
            for epoch in range(1, self.epochs + 1):
                tf.train.get_global_step()
                self.train_one_epoch(epoch, summary_writer)

    def train_one_epoch(self, epoch, summary_writer):

        loss, summaries = 0, []
        self.train_init.run()
        train_steps = 0
        while True:
            try:
                _, ploss = self.sess.run(self.train_fetch)
                loss += ploss
                train_steps += 1
                # self.logger.debug("train step: {} loss: {:.4f}".format(train_steps, ploss))
            except tf.errors.OutOfRangeError:
                self.logger.info("{}/{} Loss: {:.4f}".format(epoch, self.epochs, loss / train_steps))
                break

        # val_loss, val_summaries = 0, []
        # self.valid_init.run()
        # for _ in tqdm(range(self.valid_steps)):
        #     ploss, summary = self.sess.run(self.val_fetch)
        #     val_loss += ploss / self.valid_steps

        # line = "{}/{}".format(epoch, self.epochs)
        # line += "[Train Total loss: {:.4f}] ".format(loss)
        # line += "[Val Total loss: {:.4f}] ".format(val_loss)
        # self.logger.info(line)
        # self.saver.save(self.sess, self.checkpoint_file)
        # summary_writer.add_summary(summary, epoch)

    # def evaluate(self):
    #
    #     with tf.Session() as self.sess:
    #         self.saver.restore(self.sess, self.checkpoint_file)
    #         self.test_init.run()
    #         fetch_test = [*self.inputs, self.outputs['embedding']]
    #         mse = 0
    #         for _ in tqdm(range(self.test_steps)):
    #             w, c, emb = self.sess.run(fetch_test)
    #
    #         [self.logger.info("Final Evaluation: MSE: {:.4f}".format(mse))]

    @staticmethod
    def check_config(config):
        pass
