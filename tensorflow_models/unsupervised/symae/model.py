import tensorflow as tf
import tensorflow.contrib.layers as tcl

from ..autoencoder_tf import Model as BaseModel


class Model(BaseModel):

    name = 'tied_autoencoder'

    def __init__(self, config):
        super(Model, self).__init__(config)
        self.weights, self.biases, self.layers = [], [], []

    def __call__(self, X):

        with tf.variable_scope(self.name):

            encoding = self.encoder(X)
            decoded = self.decoder(encoding)

            losses = self.build_loss(X, decoded)
            metrics = self.get_metrics(X, decoded)

        outputs = {'encoding': encoding, 'reconstruction': decoded}

        for family, d in zip(['loss', 'metrics'], [losses, metrics]):
            for k, v in d.items():
                tf.summary.scalar(k, v, family=family)

        for k, v in outputs.items():
            tf.summary.histogram(k, v, family='output')

        return outputs, losses, metrics

    def encoder(self, previous):
        with tf.variable_scope('encoder'):
            for i, neurons in enumerate(self.encoder_layers + [self.encoding_dim]):
                with tf.variable_scope('layer_{}'.format(i+1)):
                    previous_dim = previous.get_shape().as_list()[1]
                    initializer = self.initializer([previous_dim, neurons])
                    weights = tf.Variable(initializer, dtype=tf.float32, name='weights_{}'.format(i+1))
                    biases = tf.Variable(tf.zeros(neurons), name='biases_{}'.format(i+1))
                    previous = self.activation(tf.matmul(previous, weights) + biases, name="fc_{}".format(i+1))
                    # print(i, len(self.weights), self.weights[-1].shape, self.biases[-1].shape)
                    self.weights.append(weights); self.biases.append(biases); self.layers.append(previous)
        
        return previous

    def decoder(self, previous):
        with tf.variable_scope('decoder'):
            for i in range(len(self.encoder_layers), -1, -1):
                layer_id = len(self.weights) + 1
                with tf.variable_scope('layer_{}'.format(layer_id)):
                    weights = tf.transpose(self.weights[i], name="weights_{}".format(layer_id))
                    bias_dim = weights.get_shape().as_list()[1]
                    biases = tf.Variable(tf.zeros(bias_dim), name="biases_{}".format(layer_id))
                    previous = self.activation(tf.matmul(previous, weights) + biases, name="fc_{}".format(i+1))
                    # print(i, layer_id, self.weights[-1].shape, self.biases[-1].shape)
                    self.weights.append(weights); self.biases.append(biases); self.layers.append(previous)

        return previous

    def build_loss(self, inputs, encoding):
        with tf.variable_scope('loss', reuse=tf.AUTO_REUSE):
            with tf.variable_scope('mean_squared_error'):
                reconstruction_loss = tf.reduce_mean(tf.square(encoding - inputs))

            reg_loss = 0
            for i in range(len(self.encoder_layers) + 1):
                reg_loss += self.regularizer(self.weights[i])

            loss = reconstruction_loss + reg_loss

        return {'loss': loss, 'reconstruction_loss': reconstruction_loss, 'reg_losses': reg_loss}

    def get_metrics(self, inputs, encoding):
        return {}
