from tensorflow_models.utils.custom_layers import TiedDense
from tensorflow_models.unsupervised.autoencoder import Model as BaseModel


class TiedAE(BaseModel):
    """
    Auto-encoder with tied weights
    """

    def build_decoder(self, X):

        self.decoder_layers = []
        for i, units in enumerate(reversed([self.input_size] + self.encoder_dims[:-1])):
            is_output = i + 1 == len(self.encoder_dims)
            self.decoder_layers.append(
                TiedDense(units,
                          activation=self.output_activation if is_output else self.activation,
                          tied_to=self.encoder_layers[-i - 1],
                          name='reconstruction' if is_output else 'decoder_layer{}'.format(i + 1)))

            X = self.decoder_layers[-1](X)

        return X
