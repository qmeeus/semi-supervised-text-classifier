import tensorflow as tf
import keras.backend as K
from keras.layers import Dense, InputSpec, Conv1D
from keras.engine import Layer
import warnings

from tensorflow_models.utils.ops import sparse_to_dense


class KCompetitive(Layer):

    def __init__(self, topk, ctype, top_and_bottom=True, amplification=1.0, **kwargs):
        """
        Apply KCompetitive layer

            :param topk:    number of activities to keep in the feed-forward phase (max value = code dim)
            :param ctype:   type of competitivity (ksparse or kcomp)
                ksparse:        enforce sparsity by only keeping the k highest activities in training phase
                kcomp:          enforce sparsity by inducing competition between neurons for the right to respond for a
                                given set of input patterns

            :param kwargs:  arguments passed to Layer

        """
        self.topk = topk
        self.ctype = ctype
        self.top_and_bottom = top_and_bottom
        self.uses_learning_phase = True
        self.supports_masking = True
        self.amplification = amplification
        super(KCompetitive, self).__init__(**kwargs)

    def call(self, X, mask=None):
        dim = int(X.get_shape()[1])
        if self.topk > dim:
            warnings.warn('topk should not be larger than dim: {}, found: {}, using {}'.format(dim, self.topk, dim))
            self.topk = dim

        if self.ctype == 'ksparse':
            return K.in_train_phase(self.k_sparse(X), X)
        elif self.ctype == 'kcomp':
            return K.in_train_phase(self.k_competitive(X), X)
        else:
            warnings.warn("Unknown ctype, using no competition.")
            return X

    def get_config(self):
        config = {'topk': self.topk,
                  'ctype': self.ctype,
                  'top_and_bottom': self.top_and_bottom,
                  'amplification': self.amplification}

        base_config = super(KCompetitive, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def k_competitive(self, X):  # factor=6.26
        factor = self.amplification
        pos = tf.reduce_sum((X + tf.abs(X)) / 2, axis=1, keep_dims=True)
        neg = tf.reduce_sum((X - tf.abs(X)) / 2, axis=1, keep_dims=True)
        factor *= (self.topk // 2) ** -1
        top = self.get_k_activations(X, self.topk // 2, top=True)
        bottom = self.get_k_activations(X, self.topk // 2, top=False)
        Epos = pos - tf.reduce_sum(top, axis=1, keep_dims=True)
        Eneg = neg - tf.reduce_sum(bottom, axis=1, keep_dims=True)
        norm_pos = tf.where(tf.not_equal(top, 0), top + factor * Epos, tf.zeros_like(top))
        norm_neg = tf.where(tf.not_equal(bottom, 0), bottom + factor * Eneg, tf.zeros_like(bottom))
        return norm_pos + norm_neg

    def k_sparse(self, X):
        if self.top_and_bottom:
            top = self.get_k_activations(X, self.topk // 2, top=True)
            bottom = self.get_k_activations(X, self.topk // 2, top=False)
            return top + bottom

        else:
            return self.get_k_activations(X, self.topk, top=True)

    @staticmethod
    def get_k_activations(X, k, top=True):
        dim = int(X.get_shape()[1])

        if not top:
            X = -X

        # Only keep topk neurons by resetting bottom dim - topk units to 0
        k = dim - k

        # indices will be [[0, 1], [2, 1]], values will be [[6., 2.], [5., 4.]]
        values, indices = tf.nn.top_k(-X, k)

        # We need to create full indices like [[0, 0], [0, 1], [1, 2], [1, 1]]
        repeat = tf.tile(tf.expand_dims(tf.range(0, tf.shape(indices)[0]), 1), [1, k])

        # change shapes to [N, k, 1] and [N, k, 1], to concatenate into [N, k, 2]
        full_indices = tf.reshape(tf.stack([repeat, indices], axis=2), [-1, 2])

        reset = sparse_to_dense(full_indices, tf.shape(X), tf.reshape(values, [-1]))

        return (X + reset) * (1 if top else -1)


class TiedDense(Dense):
    """
    A fully connected layer with tied weights.
    """

    def __init__(self,
                 units,
                 tied_to=None,
                 activation=None,
                 use_bias=True,
                 bias_initializer='zeros',
                 **kwargs):

        self.tied_weights = tied_to.kernel

        super(TiedDense, self).__init__(units=units,
                                        activation=activation,
                                        use_bias=use_bias,
                                        bias_initializer=bias_initializer,
                                        **kwargs)

    def build(self, input_shape):
        super(TiedDense, self).build(input_shape)

        if self.kernel in self.trainable_weights:
            self.trainable_weights.remove(self.kernel)

    def call(self, x, mask=None):
        self.kernel = K.transpose(self.tied_weights)
        output = K.dot(x, self.kernel)

        if self.use_bias:
            output += self.bias

        return self.activation(output)


class KMeansLayer(Layer):
    """
    Clustering layer converts input sample (feature) to soft label.

    # Example
    ```
        model.add(ClusteringLayer(n_clusters=10))
    ```
    # Arguments
        n_clusters: number of clusters.
        weights: list of Numpy array with shape `(n_clusters, n_features)` witch represents the initial cluster centers.
        alpha: degrees of freedom parameter in Student's t-distribution. Default to 1.0.
    # Input shape
        2D tensor with shape: `(n_samples, n_features)`.
    # Output shape
        2D tensor with shape: `(n_samples, n_clusters)`.
    """

    def __init__(self, n_clusters, weights=None, alpha=1.0, **kwargs):
        if 'input_shape' not in kwargs and 'input_dim' in kwargs:
            kwargs['input_shape'] = (kwargs.pop('input_dim'),)
        super(KMeansLayer, self).__init__(**kwargs)
        self.n_clusters = n_clusters
        self.alpha = alpha
        self.initial_weights = weights
        self.input_spec = InputSpec(ndim=2)

    def build(self, input_shape):
        assert len(input_shape) == 2
        input_dim = input_shape[1]
        self.input_spec = InputSpec(dtype=K.floatx(), shape=(None, input_dim))
        self.clusters = self.add_weight((self.n_clusters, input_dim), initializer='glorot_uniform', name='clusters')
        if self.initial_weights is not None:
            self.set_weights(self.initial_weights)
            del self.initial_weights
        self.built = True

    def call(self, inputs, mask=None):
        """ student t-distribution, as same as used in t-SNE algorithm.
                 q_ij = 1/(1+dist(x_i, µ_j)^2), then normalize it.
                 q_ij can be interpreted as the probability of assigning sample i to cluster j.
                 (i.e., a soft assignment)
        Arguments:
            inputs: the variable containing data, shape=(n_samples, n_features)
        Return:
            q: student's t-distribution, or soft labels for each sample. shape=(n_samples, n_clusters)
        """
        q = 1.0 / (1.0 + (K.sum(K.square(K.expand_dims(inputs, axis=1) - self.clusters), axis=2) / self.alpha))
        q **= (self.alpha + 1.0) / 2.0
        q = K.transpose(K.transpose(q) / K.sum(q, axis=1))  # Make sure each sample's 10 values add up to 1.
        return q

    def compute_output_shape(self, input_shape):
        assert input_shape and len(input_shape) == 2
        return input_shape[0], self.n_clusters

    def get_config(self):
        config = {'n_clusters': self.n_clusters}
        base_config = super(KMeansLayer, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))
