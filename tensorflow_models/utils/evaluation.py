import numpy as np
import pandas as pd
from sklearn import metrics
import os.path as p
import json
from sklearn.cluster import KMeans
from sklearn.svm import SVC
from sklearn.model_selection import cross_val_score

from tensorflow_models.utils.plotting import confusion_matrix, heatmap
from tensorflow_models.utils.np_utils import clustering_accuracy


def evaluate_autoencoder(model, X, save_encodings=False, save_topics=False, save_clusters=False, labels=None):
    """
    Evaluation function for unsupervised auto-encoder
    :param model: the autoencoder model instance
    :param X: the input data
    :param save_encodings: whether to save the encoded inputs
    :param save_topics: whether to save the topic strength
    :param save_clusters: if the labels are given, whether to save the clusters
    :param labels: the labels of the examples in case of supervised data. They are used to perform and evaluate
                   clustering.
    :return: a dictionary with the mse score
    """
    predictions = model.autoencoder.predict(X)
    predictions = predictions if type(predictions) is np.ndarray else predictions[0]

    if save_topics and len(model.encoder_layers) == 1:
        save_topics_strength(
            model.autoencoder,
            model.generator.vectorizer.get_feature_names(),
            p.join(model.output_dir, 'topics.txt'),
            model.words_per_topic)

    elif save_topics:
        model.logger.warn('Cannot extract the topic strength when the encoder has more than one layer')

    scores = dict()

    if labels is not None or save_encodings:
        encoded = model.encoder.predict(X)

    if labels is not None and save_encodings:
        scores = evaluate_clusters(model, encoded, labels, save_clusters)
        scores['SVM_f1_score'] = make_classification(encoded, labels)
        encoded = np.concatenate([labels.argmax(1).reshape(-1, 1), encoded], axis=1)

    if save_encodings:
        np.savetxt(p.join(model.output_dir, "encodings.csv"), encoded, delimiter=',')

    return dict(mse=metrics.mean_squared_error(predictions, X), **scores)


def evaluate_classifier(model, X, y, save_predictions=False):
    y_pred = model.classifier.predict(X, batch_size=model.batch_size)

    if save_predictions:
        np.savetxt(p.join(model.output_dir, 'predictions.csv'), y_pred, delimiter=',')

    y_pred = np.argmax(y_pred, 1)
    y_true = np.argmax(y, 1)
    cr = metrics.classification_report(y_true, y_pred, target_names=model.generator.target_names)
    f1_score = metrics.f1_score(y_true, y_pred, average='macro')
    acc_score = metrics.accuracy_score(y_true, y_pred)
    model.logger.info('=' * 50)
    model.logger.info('Final accuracy: {:.2%} f1 score: {:.2%}'.format(acc_score, f1_score))
    [model.logger.info(line) for line in cr.split('\n')]
    model.logger.info('=' * 50)
    confusion_matrix(y_true, y_pred, model.output_dir, classnames=model.generator.target_names)
    return {'f1_score': f1_score, 'accuracy': acc_score}


def evaluate_clusters(model, X, y, save_clusters=False):
    kmeans = KMeans(n_clusters=model.generator.n_classes, n_init=80, tol=1e-7, n_jobs=-1)
    clusters = kmeans.fit_predict(X)

    df = pd.DataFrame(np.stack([clusters, y.argmax(1), np.ones_like(clusters)], axis=1),
                      columns=['clusters', 'labels', 'count'])

    labels = y.argmax(1)
    acc = clustering_accuracy(labels, clusters)
    nmi = metrics.normalized_mutual_info_score(labels, clusters, average_method='arithmetic')
    ari = metrics.adjusted_mutual_info_score(labels, clusters, average_method='arithmetic')

    if save_clusters:
        filename = p.join(model.output_dir, 'clusters')
        df.to_csv(filename + '.csv', sep=',')
        heatmap(df.pivot_table(index='labels', columns='clusters', values='count', aggfunc='count').fillna(0),
                annot=True, cmap='Blues', yticklabels=model.generator.target_names, fmt='.0f',
                title='Clustering accuracy: {:.2%} Normalised MI: {:.2%} Adjusted MI: {:.2%}'.format(acc, nmi, ari),
                filename=filename + '.png',)

    return {'cluster_acc': acc, 'NMI': nmi, 'AMI': ari}


def make_classification(X, y):
    if np.ndim(y) == 2:
        y = y.argmax(1)

    clf = SVC(kernel='linear', gamma='scale')
    scores = cross_val_score(clf, X, y, cv=5, n_jobs=-1, scoring='f1_macro')

    return scores.mean()


def save_topics_strength(model, vocab, output_file, topn=10):
    topic_strengths = []
    weights = model.get_weights()[0]

    for idx in range(weights.shape[1]):
        token_idx = np.argsort(weights[:, idx])[::-1][:topn]
        topic_strengths.append([(vocab[x], weights[x, idx]) for x in token_idx])

    with open(output_file, 'w') as datafile:
        for top_words in topic_strengths:
            datafile.write('\n'.join(["{:<25}{}".format(*word_strength) for word_strength in top_words]) + '\n')
            datafile.write('\n\n')

