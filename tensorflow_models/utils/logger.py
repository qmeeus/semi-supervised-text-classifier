import os
import os.path as p
import tensorflow as tf
import logzero


class BaseLogger:
    def __init__(self, config, max_size, n_files):
        os.makedirs(config.output_dir, exist_ok=True)
        log_file = p.join(config.output_dir, 'training.txt')
        self.logger = logzero.logger
        self.log_level = config.log_level
        logzero.loglevel(self.log_level)
        logzero.logfile(log_file, maxBytes=max_size, backupCount=n_files)

    def info(self, text):
        self.logger.info(text)

    def debug(self, text):
        self.logger.debug(text)

    def warn(self, text):
        self.logger.warn(text)

    def error(self, text):
        self.logger.error(text)

    def log_config(self, config):
        _ = [self.info(line) for line in str(config).split('\n')]

    def log_history(self, history, metrics_names):
        if not history:
            return

        for i, metrics in enumerate(zip(*[history[n] for n in metrics_names])):
            m = list(zip(metrics_names, metrics))
            log = "{:02d} [train ".format(i + 1)
            log += " ".join(["{}: {:.4f}".format(mname, mval) for mname, mval in m if not mname.startswith("val_")])
            log += "]"
            if list(filter(lambda mname: mname.startswith("val_"), metrics_names)):
                log += " [val "
                log += " ".join(["{}: {:.4f}".format(mname, mval) for mname, mval in m if mname.startswith("val_")])
                log += "]"
            self.info(log)


class Logger(BaseLogger):

    def __init__(self, config, max_size=1000000.0, n_files=1):
        super().__init__(config, max_size=max_size, n_files=n_files)

    def analyze_vars(self, variables, print_info=False, tensorboard=True):

        def tensor_description(var):
            description = '(' + str(var.dtype.name) + ' '
            sizes = var.get_shape()
            for i, size in enumerate(sizes):
                description += str(size)
                if i < len(sizes) - 1:
                    description += 'x'
            description += ')'
            return description

        if print_info:
            self.info('---------')
            self.info('Variables: name (type shape) [size]')
            self.info('---------')

        total_size, total_bytes = 0, 0

        for var in variables:

            var_size = var.get_shape().num_elements() or 0
            var_bytes = var_size * var.dtype.size
            total_size += var_size
            total_bytes += var_bytes

            if print_info:
                self.info("{} {} [{}, bytes: {}]".format(var.name, tensor_description(var), var_size, var_bytes))

            if tensorboard:
                tf.summary.histogram(var.name, var, family=var.name.split('/')[0])

        if print_info:
            self.info('Total size of variables: {}'.format(total_size))
            self.info('Total bytes of variables: {}'.format(total_bytes))

        return total_size, total_bytes
