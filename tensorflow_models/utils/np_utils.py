import numpy as np
from operator import itemgetter
from sklearn.utils.linear_assignment_ import linear_assignment


def calc_ranks(x):
    """Given a list of items, return a list(in ndarray type) of ranks.
    """
    n = len(x)
    index = list(zip(*sorted(enumerate(x), key=itemgetter(1), reverse=True)))[0]
    rank = np.zeros(n)
    rank[index] = range(1, n + 1)
    return rank


def revdict(d):
    """
    Reverse a dictionary mapping.
    When two keys map to the same value, only one of them will be kept in the
    result (which one is kept is arbitrary).
    """
    return dict((v, k) for (k, v) in d.items())


def l1norm(x):
    return x / sum([np.abs(y) for y in x])


def vecnorm(x, norm='freq', epsilon=1e-7, axis=0):
    """
    Scale a vector to unit length. The only exception is the zero vector, which
    is returned back unchanged.
    """

    if not isinstance(x, np.ndarray):
        raise TypeError('x should be ndarray, found: {}'.format(type(x)))

    assert axis < x.ndim, "Axis {} is invalid for array with {} dimensions".format(axis, x.ndim)

    if norm == 'prob':
        return np.abs(x + epsilon) / np.abs(x + epsilon).sum(axis=axis, keepdims=True)
    elif norm == 'minmax':
        return (x - x.min(axis=axis) + epsilon) / (x.max(axis=axis) - x.min(axis=axis)  + epsilon)
    elif norm == 'logmax':
        return (np.log10(1. + x) + epsilon) / (np.max(x) + epsilon)
    elif norm == 'freq':
        return np.divide(x, x.sum(axis=1, keepdims=True), out=x, where=x.sum(axis=1) != 0)
    else:
        raise TypeError("Invalid norm: {}. Supported norms: 'prob', 'max' and 'logmax'.".format(norm))


def unitmatrix(matrix, norm='l2', axis=1):
    if norm == 'l1':
        reg = np.sum(np.abs(matrix), axis=axis)
    elif norm == 'l2':
        reg = np.linalg.norm(matrix, axis=axis)
    else:
        raise TypeError('Invalid argument: {}'.format(norm))

    if np.any(reg <= 0):
        return matrix
    else:
        reg = reg.reshape(1, len(reg)) if axis == 0 else reg.reshape(len(reg), 1)
        return matrix / reg


def gaussian_noise(X, corruption_ratio, mu=0., sigma=1., clip_values=None):
    clip_values = clip_values or [0, 1]
    X = X + corruption_ratio * np.random.normal(loc=mu, scale=sigma, size=X.shape)
    return np.clip(X, *clip_values)


def masking_noise(X, frac, value=0):
    if not 0 <= frac <= 1:
        raise TypeError('Argument p should be between 0 and 1')
    X = X.copy()
    m = X.shape[1]
    for i in range(len(X)):
        X[i, np.random.choice(m, int(m * frac), replace=False)] = value
    return X


def salt_pepper_noise(X, frac, n=1, p=.5):
    if not 0 <= frac <= 1:
        raise TypeError('Argument p should be between 0 and 1')
    X = X.copy()
    m = X.shape[1]
    for i in range(len(X)):
        X[i, np.random.choice(m, int(m * frac), replace=False)] = np.random.binomial(n, p, int(m * frac))
    return X


def add_some_noise(X, noise_type, noise_options=None):
    noise_functions = {
        'gaussian_noise': gaussian_noise,
        'salt_and_pepper': salt_pepper_noise,
        'masking_noise': masking_noise}

    if not noise_type:
        return X
    elif noise_type not in noise_functions:
        raise TypeError('Invalid argument: {}'.format(noise_type))
    elif not noise_options:
        raise TypeError('If noise_type is specified, you must provide noise_options')
    return noise_functions[noise_type](X, **noise_options)


def clustering_accuracy(y_true, y_pred):
    """
    Calculate clustering accuracy. Require scikit-learn installed
    # Arguments
        y: true labels, numpy.array with shape `(n_samples,)`
        y_pred: predicted labels, numpy.array with shape `(n_samples,)`
    # Return
        accuracy, in [0,1]
    """
    y_true = y_true.astype(np.int64)
    assert y_pred.size == y_true.size
    D = max(y_pred.max(), y_true.max()) + 1
    w = np.zeros((D, D), dtype=np.int64)
    for i in range(y_pred.size):
        w[y_pred[i], y_true[i]] += 1
    ind = linear_assignment(w.max() - w)
    return sum([w[i, j] for i, j in ind]) * 1.0 / y_pred.size


def target_distribution(q):
    weight = q ** 2 / q.sum(0)
    return (weight.T / weight.sum(1)).T
