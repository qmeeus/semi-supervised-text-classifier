import numpy as np
import tensorflow as tf
from keras import backend as K, losses

from tensorflow_models.utils.nn_utils import get_loss


def masked_supervised_loss(model, loss_id):

    def supervised_loss(labels, logits):
        if K.ndim(labels) == 1 or labels.get_shape()[1] == 1:
            mask = tf.cast(tf.not_equal(labels, -1), 'int32')
            labels = tf.one_hot(tf.cast(labels, 'int32'), model.n_classes)
        else:
            mask = tf.cast(tf.not_equal(K.sum(labels, axis=1), 0), 'int32')

        return get_loss(loss_id)(labels, logits, weights=mask)

    return supervised_loss


def orthogonal_regularizer(alpha):

    def squared_frobenius_norm(weight_matrix):
        m = K.dot(K.transpose(weight_matrix), weight_matrix) - np.eye(weight_matrix.shape)
        return alpha * K.sqrt(K.sum(K.square(K.abs(m))))

    return squared_frobenius_norm


def build_contractive_loss(model, loss_id, lam=1e-4):

    def contractive_loss(y_true, y_pred):
        loss = losses.get(loss_id)(y_true=y_true, y_pred=y_pred)

        W = K.variable(value=model.encoder.get_weights()[-1])  # N x N_hidden
        W = K.transpose(W)  # N_hidden x N
        h = model.encoder.output
        dh = h * (1 - h)  # N_batch x N_hidden

        # N_batch x N_hidden * N_hidden x 1 = N_batch x 1
        contractive = K.sum(dh ** 2 * K.sum(W ** 2, axis=1), axis=1)

        return loss + lam * contractive

    return contractive_loss


def build_pairwise_kld_loss():

    def pairwise_kld(y_true, y_pred):
        return losses.kld(y_true, y_pred) + losses.kld(y_pred, y_true)

    return pairwise_kld


def weighted_binary_crossentropy(feature_weights):
    def loss(y_true, y_pred):
        # try:
        #     x = K.binary_crossentropy(y_pred, y_true)
        #     # y = tf.Variable(feature_weights.astype('float32'))
        #     # z = K.dot(x, y)
        #     y_true = tf.pow(y_true + 1e-5, .75)
        #     y2 = tf.div(y_true, tf.reshape(K.sum(y_true, 1), [-1, 1]))
        #     z = K.sum(tf.mul(x, y2), 1)
        # except Exception as e:
        #     print(e)
        #     import pdb;pdb.set_trace()
        # return z
        return K.dot(K.binary_crossentropy(y_pred, y_true), K.variable(feature_weights.astype('float32')))

    return loss


def cluster_distance(centroids):

    def loss(x, r):
        with tf.variable_scope('cluster_distance'):
            N, K = tf.shape(x)[0], tf.shape(centroids)[0]
            x = tf.tile(tf.expand_dims(x, 1), [1, K, 1])
            dist = tf.sqrt(tf.reduce_sum(tf.square(x - centroids), axis=-1), name='euclidean_distance')
            r = tf.cast(r, 'float32')
            return tf.reduce_sum(r * dist, name='total_distance')

    return loss


def masked_accuracy(y_true, y_pred):
    mask = K.cast(K.equal(K.sum(y_true, axis=1), 1), 'float')
    y_true = K.argmax(y_true, axis=1)
    y_pred = K.argmax(K.softmax(y_pred), axis=1)
    n_sup = K.clip(K.sum(mask), 1, None)
    return K.sum(mask * K.cast(K.equal(y_true, y_pred), 'float')) / n_sup


def sparse_to_dense(sparse_indices, output_shape, sparse_values, default_value=0., validate=False, name=None):
    sparse_indices = tf.cast(sparse_indices, 'int64')
    output_shape = tf.cast(output_shape, 'int64')
    sparse = tf.sparse.SparseTensor(sparse_indices, sparse_values, output_shape)
    return tf.sparse.to_dense(sparse, default_value, validate, name)


def target_distribution(q):
    weights = K.square(q) / K.sum(q, axis=0)
    return K.transpose(K.transpose(weights) / K.sum(weights, axis=1))
