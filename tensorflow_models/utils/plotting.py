import os
import numpy as np

if not any(v in os.environ for v in ('MPLBACKEND', 'DISPLAY')):
    import matplotlib; matplotlib.use('Agg')

import matplotlib.pyplot as plt
import seaborn as sns; sns.set_style('white')
from sklearn.metrics import confusion_matrix as cm
from sklearn.metrics import precision_recall_fscore_support as prfs


def confusion_matrix(labels,
                     predictions,
                     save_directory=None,
                     filename='confusion_matrix.png',
                     figsize=(12, 10),
                     classnames=None):

    filename = os.path.join(save_directory or "", filename) if filename else None
    labels = labels if np.ndim(labels) == 1 else labels.argmax(1)
    predictions = predictions if np.ndim(predictions) == 1 else predictions.argmax(1)
    classnames = classnames or 'auto'

    matrix = cm(y_true=labels, y_pred=predictions)
    precision, recall, f1_score, _ = prfs(y_true=labels, y_pred=predictions, average='macro')
    final_score = np.mean(labels == predictions)

    title = ("accuracy: {:.2%}, precision: {:.2%} recall: {:.2%} f1 score: {:.2%}"
             "\n(all metrics are macro averages)".format(final_score, precision, recall, f1_score))

    def annot_format(t): return t.set_text("{:.0f}".format(float(t.get_text()))) if float(t.get_text()) > 0 else ""

    def ax_format(ax):
        ax.set_yticklabels(ax.get_yticklabels(), rotation=0, fontsize=10)
        ax.set_xticklabels(ax.get_xticklabels(), rotation=45, fontsize=10, rotation_mode='anchor', ha='right')

    kwargs = dict(annot=True, xticklabels=classnames, yticklabels=classnames, vmin=0, vmax=matrix.sum(axis=1).min(),
                  cmap='Blues', square=True, title=title, figsize=figsize, filename=filename,
                  annot_format=annot_format, ax_format=ax_format)

    heatmap(matrix, **kwargs)

    return final_score


def heatmap(mat, **kwargs):

    """
    plot a heatmap
    :param mat: matrix to plot
    :param kwargs: accepted kwargs: filename (pathlike), annot_format (callable or str), ax_format (callable to
    format ax components), ax (plt.Axes), title, x/ylabel (str) + any kwargs accepted by sns.heatmap
    :return: None
    """

    # Remove arguments not accepted by heatmap or set default
    filename = kwargs.pop('filename') if 'filename' in kwargs else None
    annot_format = kwargs.pop('annot_format') if 'annot_format' in kwargs else False
    ax_format = kwargs.pop('ax_format') if 'ax_format' in kwargs else False
    figsize = kwargs.pop('figsize') if 'figsize' in kwargs else (14, 12)
    kwargs['ax'] = kwargs.pop('ax') if 'ax' in kwargs else plt.figure(figsize=figsize).add_subplot()
    title = kwargs.pop('title') if 'title' in kwargs else None
    xlabel = kwargs.pop('xlabel') if 'xlabel' in kwargs else None
    ylabel = kwargs.pop('ylabel') if 'ylabel' in kwargs else None

    if type(annot_format) is str:
        kwargs['annot_format'] = annot_format

    ax = sns.heatmap(mat, **kwargs)

    if kwargs.get('annot') and callable(annot_format):
        list(map(annot_format, ax.texts))

    if title:
        ax.set_title(title)

    if xlabel:
        ax.set_xlabel(xlabel)

    if ylabel:
        ax.set_ylabel(ylabel)

    if ax_format and callable(ax_format):
        ax_format(ax)

    plt.savefig(filename) or plt.close('all') if filename else plt.show()


def plot_history(history, save_directory=None, filename="history.png", figsize=(12, 10)):
    # TODO: convert to pandas.DataFrame, add multiindex metrics/subset, stack and plot epoch vs metrics with hue=subset
    for metric in ["acc", "loss"]:
        fig = plt.figure(figsize=figsize)
        ax = fig.add_subplot(111)
        ax.plot(history[metric], label='train')
        ax.plot(history['val_{}'.format(metric)], label='val')
        plt.legend()
        plt.tight_layout()
        plt.savefig(os.path.join(save_directory, metric + "_" + filename))
        plt.close('all')

