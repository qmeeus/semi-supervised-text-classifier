import os.path as p

class TestConfig:

    root = p.abspath(p.join(p.dirname(__file__), "../.."))
    data_home = p.join(root, "datasets")
    preprocessed_data = p.join(root, "preprocessed_data")
    tokenizer_config = p.join(preprocessed_data, "tokenizer_small.json")
    tokenizer_options = {'num_words': 6000}
    
    categories = ['alt.atheism', 'comp.graphics', 'rec.autos']
    n_classes = 3

    examples_per_class = .1
    validation_split = .2

    max_sequence_length = 6000

    preprocess=True
    batch_size = 32

    categorical = True

    bow_features = 'binary'

    window_size = 5
    negative_samples = .1
    max_documents = 1000
    max_examples_per_doc = 1000