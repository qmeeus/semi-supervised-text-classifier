from time import time
import tensorflow as tf


def timeit(func, *args, **kwargs):
    print("Start timing...")
    t0 = time()
    out = func(*args, **kwargs)
    print("Took {:.2} s".format(time() - t0))
    return out


def run_tensors(initializer, tensors):
    with tf.Session() as sess:
        initializer.run()
        tf.global_variables_initializer().run()
        tf.local_variables_initializer().run()
        while True:
            try:
                yield sess.run(tensors)
            except tf.errors.OutOfRangeError:
                print("End of iterator")


def make_dataset_iterators(gen, bs):
    dt = gen.make_dataset()
    it = dt.make_one_shot_iterator()
    tensors = it.get_next()
    return dt, it, tensors


def _start_shell(local_ns=None):
    # An interactive shell is useful for debugging/development.
    import IPython
    user_ns = {}
    if local_ns:
        user_ns.update(local_ns)
        user_ns.update(globals())
    IPython.start_ipython(argv=[], user_ns=user_ns)
