import numpy as np

from tests.helper_functions import *

from tensorflow_models.base import Config
from tensorflow_models.data_loaders import load_newsgroups


def test_loading():
    print("Testing config")
    cfg = Config('fast_config.yaml')
    thres = cfg.examples_per_class

    def data_structure_checks(datasets, n_classes):
        # Check that datasets have 3 components
        assert all(len(ds) == 3 for ds in datasets)

        # Check that all 3 components have the same number of examples
        assert all(len(X) == len(y) == len(m) for X, y, m in datasets)

        # Check that all 3 components are numpy arrays
        assert all(type(d) == np.ndarray for dataset in datasets for d in dataset)

        # Check the components dimensions
        for dataset in datasets:
            X, y, m = dataset
            assert all(len(d.shape) == 1 for d in (X, m))
            assert y.shape[1] == n_classes

    def data_split_checks(datasets, vs):
        train, validation, _ = datasets
        (X, _, _), (Xv, _, _) = train, validation
        assert round(vs * (len(X) + len(Xv))) - len(Xv) < 5, vs

    def supervised_mask_checks(datasets, thres, n_classes):
        _, _, masks = tuple(zip(*datasets))
        train, validation, test = datasets
        (X, _, m) = train
        if thres <= 0:
            assert [sum(m) == 0 for m in masks], thres
            return
        assert all(sum(m) == len(m) for (_, _, m) in (validation, test)), thres
        if (thres == 1 and type(thres) is float) or thres * n_classes > len(X):
            assert [sum(m) / len(m) == 1 for m in masks], thres
            return
        elif 0 < thres < 1:
            assert abs(sum(m) / len(X) - thres) < 0.1, thres
        elif type(thres) == int:
            expected = n_classes * thres
            assert sum(m) == expected or expected > len(X), thres
        else:
            assert False, 'Invalid threshold: %s' % thres

    for thres in (-10, 0, 0., 1, 1., 10, 1000, 1000.):
        options = dict(data_home=cfg.data_home,
                       examples_per_class=thres,
                       validation_split=cfg.validation_split,
                       **cfg.preprocessing_options)

        datasets = load_newsgroups(**options)

        train, validation, test, target_names = datasets
        datasets = train, validation, test
        data_structure_checks(datasets, len(target_names))
        data_split_checks(datasets, cfg.validation_split)
        supervised_mask_checks(datasets, thres, len(target_names))


if __name__ == '__main__':
    # import ipdb; ipdb.set_trace()
    test_loading()
