import numpy as np
import pandas as pd
import pytest

from tensorflow_models.data_loaders import load_newsgroups, SkipgramGenerator, EmbLoader
from tensorflow_models.base import Config


GLOVE = "embedding/glove.6B/glove.6B.50d.txt"
GOOGLE_NEWS = "embedding/GoogleNews-vectors-negative300.bin"


def build_emb_loader(weights_file, embedding_dim):
    settings = "fast_config.yaml"
    cfg = Config(settings)
    cfg.embedding_weights = None
    generator = SkipgramGenerator(load_newsgroups, cfg)
    word_index = generator.get_word_index()
    cfg.embedding_weights = cfg.absolute_path(weights_file)
    cfg.embedding_dim = embedding_dim
    emb_loader = EmbLoader(word_index, generator.max_sequence_length, cfg)
    assert isinstance(emb_loader, EmbLoader)
    return emb_loader, word_index, cfg


def load_and_check(emb_loader):
    embedding_index = emb_loader.load()
    assert isinstance(embedding_index, dict)
    assert len(embedding_index), len(emb_loader.word_index)
    return embedding_index


def build_and_check(emb_loader):
    emb_loader.build()
    assert hasattr(emb_loader, 'embedding_matrix')
    embedding_matrix = emb_loader.embedding_matrix
    assert isinstance(embedding_matrix, np.ndarray)
    assert embedding_matrix.shape == (len(emb_loader.word_index) + 1, emb_loader.embedding_dim)
    return embedding_matrix


def test_case_emb_loader():
    embedding_dim = 50
    emb_loader, word_index, cfg = build_emb_loader(GLOVE, embedding_dim)
    # embedding_index = load_and_check(emb_loader)
    build_and_check(emb_loader)


def test_integration_emb_loader_generator():
    settings = "fast_config.yaml"
    cfg = Config(settings)
    cfg.embedding_weights = GLOVE
    cfg.embedding_dim = 50
    generator = SkipgramGenerator(load_newsgroups, cfg)
    assert isinstance(generator, SkipgramGenerator)
    assert generator.emb_loader is not None
    build_and_check(generator.emb_loader)


@pytest.mark.skip(reason="Freezes memory and should work except if changes were made to emb_loader.load_bin_file")
def test_emb_loader_bin():
    embedding_dim = 300
    emb_loader, word_index, cfg = build_emb_loader(GOOGLE_NEWS, embedding_dim)
    build_and_check(emb_loader)


if __name__ == '__main__':
    test_case_emb_loader()
    test_integration_emb_loader_generator()
    # test_emb_loader_bin()
