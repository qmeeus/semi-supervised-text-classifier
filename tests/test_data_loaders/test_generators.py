import numpy as np
import pandas as pd
import tensorflow as tf
import pytest

from tensorflow_models.data_loaders import load_newsgroups
from tensorflow_models.data_loaders import Generator, SequenceGenerator, BOWGenerator, TfIdfGenerator, \
    SkipgramGenerator, NoisyBOWGenerator
from tensorflow_models.base import Config


def build_generator(Generator, config_file='fast_config.yaml'):
    func = load_newsgroups
    cfg = Config(config_file)
    gen = Generator(func, cfg)
    return gen, func, cfg


def make_dataset_checks(gen, bs, expected_shapes, expected_types):
    dt = gen.make_dataset()
    assert isinstance(dt, tf.data.Dataset), dt
    it = dt.make_one_shot_iterator()
    tensors = it.get_next()
    run_tensors(tensors, expected_shapes, expected_types)


def run_tensors(tensors, expected_shapes, expected_types, n=3):
    with tf.Session() as sess:
        for _ in range(n):
            outputs = sess.run(tensors)
            # import ipdb; ipdb.set_trace()
            for out, exp, typ in zip(outputs, expected_shapes, expected_types):
                assert out.shape == exp, "{}\t{}".format(out.shape, exp)
                assert out.dtype == typ, "{}\t{}".format(out.dtype, typ)


def init_sess(gen, bs):
    dt = gen.make_dataset()
    vdt = gen.make_dataset('valid')
    it = tf.data.Iterator.from_structure(
        output_shapes=dt.output_shapes,
        output_types=dt.output_types)
    inputs = it.get_next()
    train_init = it.make_initializer(dt)
    valid_init = it.make_initializer(vdt)
    train_steps = int(gen.n_examples / bs)
    valid_steps = int(len(gen.validation[0]) / bs)
    return inputs, train_init, valid_init, train_steps, valid_steps


def run_all_epoch(gen, bs, epochs):
    inputs, train_init, valid_init, train_steps, valid_steps = init_sess(gen, bs)
    print("Expected training steps: {} valid steps: {}".format(train_steps, valid_steps))

    with tf.Session() as sess:
        i = 0
        while i < epochs:
            i += 1
            try:
                train_init.run()
                for j1 in range(train_steps):
                    X, y, m = sess.run(inputs)
                    print(i, j1, X.shape, y.shape, sum(m))

                valid_init.run()
                for j2 in range(valid_steps):
                    Xv, yv, mv = sess.run(inputs)
                    print(i, j2, Xv.shape, yv.shape, sum(mv))
            except tf.errors.OutOfRangeError:
                assert j1 == train_steps, (j1, train_steps)
                assert j2 == valid_steps, (j2, valid_steps)


def run_two_batches(gen, bs):
    inputs, train_init, valid_init, train_steps, valid_steps = init_sess(gen, bs)
    with tf.Session() as sess:
        train_init.run()
        _, y1, _ = sess.run(inputs)
        train_init.run()
        _, y2, _ = sess.run(inputs)
        assert (y1 != y2).any()
        # # Shuffle is set to False by default --> does not work
        # valid_init.run()
        # _, y1, _ = sess.run(inputs)
        # valid_init.run()
        # _, y2, _ = sess.run(inputs)
        # assert (y1 != y2).any()


def test_default_generator():
    gen, func, cfg = build_generator(Generator)
    assert isinstance(gen, Generator), gen
    assert all(hasattr(gen, ds) for ds in ('train', 'validation', 'test'))


def test_tokenizer():
    # import ipdb; ipdb.set_trace()
    gen, func, cfg = build_generator(SequenceGenerator)
    tokenizer = gen.tokenizer
    assert tokenizer.word_index
    assert tokenizer.is_trained
    assert tokenizer.num_words == cfg.tokenizer_options['num_words']
    # import ipdb; ipdb.set_trace()


def test_get_datasets():
    # import ipdb; ipdb.set_trace()
    gen, func, cfg = build_generator(Generator)

    for subset in ('train', 'valid', 'test'):
        for i in range(1, 3):
            out = gen.get_dataset(i, subset)
            assert type(out) == tuple and len(out) == i, len(out)

    for thres in (0, 0.1, 1, 5, 100):
        cfg.examples_per_class = thres
        cfg.training_mode = 'supervised'
        gen = Generator(func, cfg)
        _, _, m = gen.get_dataset(3, 'train')

        X, _ = gen.get_dataset(2, 'train')

        if type(thres) == int:
            exp = thres * gen.n_classes
        else:
            exp = round(gen.n_examples * thres)

        assert exp == sum(m) == len(X), (exp, sum(m), len(X))


def test_sequence_generator():
    gen, func, cfg = build_generator(SequenceGenerator)
    bs = cfg.batch_size
    expected_shapes = [(bs, gen.max_sequence_length), (bs, gen.n_classes), (bs,)]
    expected_types = ['int32', 'int32', 'bool']
    make_dataset_checks(gen, bs, expected_shapes, expected_types)


def test_bow_generator():
    func = load_newsgroups
    cfg = Config("fast_config.yaml")

    for mode in ("binary", "count", "tfidf", "freq"):
        print('Test BOW with {}'.format(mode))
        cfg.bow_features = mode
        gen = BOWGenerator(func, cfg)
        bs = cfg.batch_size
        expected_shapes = [(bs, gen.num_words), (bs, gen.n_classes), (bs,)]
        expected_types = ['float32', 'int32', 'bool']
        make_dataset_checks(gen, bs, expected_shapes, expected_types)


def test_noisy_bow_generator():
    func = load_newsgroups
    cfg = Config("fast_config.yaml")

    for mode in ("binary", "count", "tfidf", "freq"):
        print('Test Noisy BOW with {}'.format(mode))
        # import ipdb; ipdb.set_trace()
        cfg.bow_features = mode
        gen = NoisyBOWGenerator(func, cfg)
        bs = cfg.batch_size
        expected_shapes = [(bs, gen.num_words), (bs, gen.num_words), (bs, gen.n_classes), (bs,)]
        expected_types = ['float32', 'float32', 'int32', 'bool']
        make_dataset_checks(gen, bs, expected_shapes, expected_types)


def test_tfidf_generator():
    gen, func, cfg = build_generator(TfIdfGenerator, 'fast_config.yaml')
    bs = cfg.batch_size
    expected_shapes = [(bs, gen.num_words), (bs, gen.n_classes), (bs,)]
    expected_types = ['float32', 'int32', 'bool']
    make_dataset_checks(gen, bs, expected_shapes, expected_types)


@pytest.mark.skip(reason="Expected steps not equal to steps per epoch")
def test_skipgram_generator():
    gen, func, cfg = build_generator(SkipgramGenerator)
    cfg.max_documents = None
    cfg.max_examples_per_doc = None
    cfg.negative_samples = 1.
    cfg.window_size = 1

    bs = cfg.batch_size = 32
    print(cfg)
    expected_shapes = [(bs,), (bs,), (bs,)]
    expected_types = ['int32', 'int32', 'int32']
    make_dataset_checks(gen, bs, expected_shapes, expected_types)
    run_all_epoch(gen, cfg.batch_size, 1)
    # import ipdb; ipdb.set_trace()


def test_make_dataset():
    cfg = Config("fast_config.yaml")
    cfg.examples_per_class = 0.5
    # import ipdb; ipdb.set_trace()
    gen = BOWGenerator(load_newsgroups, cfg)
    run_all_epoch(gen, cfg.batch_size, 3)
    run_two_batches(gen, cfg.batch_size)


def inspect_data():
    import matplotlib.pyplot as plt

    gen, func, cfg = build_generator(BOWGenerator)
    bs = cfg.batch_size

    train = gen.make_dataset()
    valid = gen.make_dataset('valid')
    iterator = tf.data.Iterator.from_structure(train.output_types, train.output_shapes)
    inputs = iterator.get_next()
    train_init = iterator.make_initializer(train)
    valid_init = iterator.make_initializer(valid)
    mapping = gen.tokenizer.index_word

    with tf.Session() as sess:
        train_init.run()
        X, y, m = sess.run(inputs)

        fig, axs = plt.subplots(1, 4, figsize=(16, 10))
        for i, doc_id in enumerate(np.random.randint(0, bs, 4)):
            label = gen.target_names[y[doc_id].argmax()]
            doc = pd.DataFrame(X[doc_id], columns=[label])
            doc.index = doc.index.map(mapping)
            doc = doc.sort_values(label)
            doc.tail(25).plot.barh(ax=axs[i])

        plt.suptitle(cfg.bow_features)
        plt.tight_layout(pad=2., h_pad=2.)
        plt.show()


if __name__ == '__main__':
    # test_default_generator()
    # test_tokenizer()
    # test_get_datasets()
    # test_sequence_generator()
    # test_bow_generator()
    # test_noisy_bow_generator()
    # test_skipgram_generator()
    # test_make_dataset()
    # inspect_data()
    test_tfidf_generator()
