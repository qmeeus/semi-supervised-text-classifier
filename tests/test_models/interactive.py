import sys
import importlib
from IPython import embed
import tensorflow as tf
import pandas as pd
import numpy as np
from keras.models import Model
import keras.backend as K
import matplotlib.pyplot as plt
import seaborn as sns

from tensorflow_models.base import Config
from tensorflow_models.utils import Logger
from tensorflow_models.mains.train import REGISTERED_MODELS, train


def build_model(model_name, config):
    K.clear_session()

    m = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name][0])

    if not isinstance(config, Config):
        config = Config(config, dict(output_dir='tests/outputs'))

    load_data = m.load_data
    Generator = m.Generator
    Model = m.Model

    logger = Logger(config)
    logger.log_config(config)
    generator = Generator(load_data, config, logger)

    model = Model(generator, config, logger)
    model.compile()

    return model


def setup_session(keras_model, layers=None):

    # input placeholders
    placeholders = dict()
    placeholders['inputs'] = keras_model.inputs
    placeholders['targets'] = keras_model.targets
    placeholders['sample_weights'] = keras_model.sample_weights
    placeholders['learning_phase'] = K.learning_phase()

    # output tensors
    tensors = dict()
    tensors['outputs'] = keras_model.outputs
    tensors['loss'] = keras_model.total_loss

    if layers:
        for layer_name in layers:
            layer = keras_model.get_layer(layer_name)
            tensors[layer_name] = dict(input=layer.input, output=layer.output)

    session = tf.Session()
    session.run(tf.global_variables_initializer())

    return placeholders, tensors, session


def setup_environnement(model_name, config_file, N=100, weight_file=None):

    model = build_model(model_name, config_file)

    if hasattr(model, 'autoencoder'):
        keras_model = model.autoencoder
    elif hasattr(model, 'classifier'):
        keras_model = model.classifier
    else:
        print('Available models: ')
        print('\n'.join([k for k, v in model.__dict__ if isinstance(v, Model)]))
        user_input = input('Choice? ').strip()
        if not hasattr(model, user_input):
            sys.exit(1)
        keras_model = getattr(model, user_input)

    data = model.get_network_inputs()
    input_data, output_data = [[d[:N] for d in data[s]] if type(data[s]) == list else [data[s][:N]] for s in data]

    if weight_file:
        keras_model.load_weights(weight_file)

    return model, keras_model, (input_data, output_data)


def interactive_debugging(model_name, config_file, layers=None, N=100, weight_file=None, mode='train'):

    model, keras_model, (input_data, output_data) = setup_environnement(model_name, config_file, N, weight_file)
    phl, tensors, sess = setup_session(keras_model, layers)
    phl_list = [*phl['inputs'], *phl['targets'], *phl['sample_weights']]
    input_values = input_data + output_data + [np.ones(N)] * len(phl['sample_weights'])
    fd = {k: v for k, v in zip(phl_list, input_values)}
    fd[phl['learning_phase']] = 1

    print('Available variables: ')
    print('\n'.join([k for k in list(locals()) if not k.startswith('_')]))
    embed()

    sess.close()


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('model_name', type=str, choices=list(REGISTERED_MODELS))
    parser.add_argument('--mode', type=str, choices=['train', 'test'], default='train')
    parser.add_argument('--config', type=str, required=False)
    parser.add_argument('--n_examples', type=int, required=False, default=100)
    parser.add_argument('--layers', nargs='*', required=False)
    parser.add_argument('--weights', type=str, required=False)

    args = parser.parse_args()

    if args.config is None:
        args.config = REGISTERED_MODELS[args.model_name][1] + '.yaml'

    interactive_debugging(args.model_name, args.config, args.layers, args.n_examples, args.weights, args.mode)
