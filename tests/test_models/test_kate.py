import numpy as np
import tensorflow as tf
import pytest


@pytest.mark.skip(reason="Not testing anything")
def test_boolean_mask():
    labels = np.array([1, 2, -1, 1, 2, -1])
    logits = np.array([
        [0.2, 0.5, 0.3],
        [0.1, 0.2, 0.8],
        [0.3, 0.3, 0.3],
        [0.1, 0.7, 0.1],
        [0.3, 0.3, 0.9],
        [0.4, 0.5, 0.6]])

    tf_labels = tf.placeholder('int32', [None,], 'labels')
    tf_logits = tf.placeholder('float32', [None, 3], 'logits')
    mask = tf.cast(tf.not_equal(tf_labels, -1), 'int32')
    masked_logits = tf_logits * tf.expand_dims(tf.cast(mask, 'float32'), -1)
    masked_labels = tf.one_hot(tf_labels, 3)
    crossentropy = tf.losses.softmax_cross_entropy(masked_labels, masked_logits, reduction='none')
    loss = tf.reduce_sum(crossentropy) / tf.cast(tf.reduce_sum(mask), 'float32')
    weighted_loss = tf.losses.softmax_cross_entropy(masked_labels, tf_logits, weights=mask)

    with tf.Session() as sess:
        fetches = [masked_labels, masked_logits, crossentropy, loss, weighted_loss]
        feed_dict = {tf_labels: labels, tf_logits: logits}
        ypred, ytrue, ce, l, wl = sess.run(fetches, feed_dict)
        print('labels:\n{}'.format(ytrue))
        print('logits:\n{}'.format(ypred))
        print('crossentropy:\n{}'.format(ce))
        print('loss:\n{}'.format(l))
        print('weighted_crossentropy:\n{}'.format(wl))


if __name__ == '__main__':
    test_boolean_mask()
