import tensorflow as tf
import numpy as np
import keras.backend as K
import importlib

from tensorflow_models.base import Config
from tensorflow_models.utils import Logger
from tensorflow_models.mains.train import REGISTERED_MODELS, train


def build_model(model_name, config):
    K.clear_session()

    m = importlib.import_module('tensorflow_models.' + REGISTERED_MODELS[model_name])

    if not isinstance(config, Config):
        config = Config(config, dict(output_dir='tests/outputs'))

    load_data = m.load_data
    Generator = m.Generator
    Model = m.Model

    logger = Logger(config)
    logger.log_config(config)
    generator = Generator(load_data, config, logger)

    model = Model(generator, config, logger)
    model.compile()

    return model


def test_kate():

    model = build_model('kate', 'kate.yaml')
    autoencoder = model.autoencoder

    layers = {layer_name: autoencoder.get_layer(layer_name) for layer_name in ['k_competitive_1']}

    # input placeholders
    inputs, targets, sample_weights = autoencoder.inputs, autoencoder.targets, autoencoder.sample_weights

    # network tensors
    kcomp_input, kcomp_output = layers['k_competitive_1'].input, layers['k_competitive_1'].output
    outputs, loss = autoencoder.outputs, autoencoder.total_loss

    data = model.get_network_inputs()
    input_data, output_data, _ = [[d[:100] for d in data[s]] if type(data[s]) == list else [data[s][:100]] for s in data]

    fd = {k: v for k, v in zip(inputs + targets + sample_weights, input_data + output_data + [np.ones(100)] * len(sample_weights))}
    fd[K.learning_phase()] = 1

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    out = sess.run(outputs + [loss], feed_dict=fd)
    output_arrays, l = out[:-1], out[-1]

    assert len(output_arrays) == len(output_data)
    assert type(l) == np.float32

    kcomp_in, kcomp_out = sess.run([kcomp_input, kcomp_output], feed_dict=fd)

    assert np.apply_along_axis(lambda ar: (ar != 0).sum() == model.competitive_topk, 1, kcomp_out).all()

    sess.close()
    return


# def test_all_model_build():
#     for model_name in REGISTERED_MODELS:
#         config_file = model_name + '.yaml'
#
#         model = build_model(model_name, config_file)


if __name__ == '__main__':
    test_kate()
