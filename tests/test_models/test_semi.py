import tensorflow as tf
import pytest

from tensorflow_models.base import BaseModel, Config
from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import reset_graph
from tests.helper_functions import make_dataset_iterators, run_tensors


pytestmark = pytest.mark.skip("Deprecated model")


def test_ssae_model():
    from tensorflow_models.semi_supervised.ssae import (
        settings, Model, Trainer, Generator, load_data)

    cfg = Config(settings)
    model = Model(cfg)
    assert isinstance(model, BaseModel)

    gen = Generator(load_data, cfg)
    datasets = [gen.make_dataset(subset) for subset in ('train', 'test', 'valid')]
    it = tf.data.Iterator.from_structure(datasets[0].output_types, datasets[0].output_shapes)
    inputs = it.get_next()
    outputs, losses, metrics = model(*inputs)
    fetches = [outputs['predictions'], outputs['reconstruction'], losses['loss'], metrics['accuracy']]
    initializers = [it.make_initializer(d) for d in datasets]

    for dataset_id, name in enumerate(['train', 'valid', 'test']):
        init = initializers[dataset_id]
        for i, (pred, rec, loss, acc) in enumerate(run_tensors(init, fetches)):
            for k, v in [('predictions', pred), ('reconstruction', rec)]:
                print("{}: {}".format(k, v.shape))
            for k, v in [('loss', loss), ('acc', acc)]:
                print('{}: {}'.format(k, v))
            if i == 3: break


def test_ssae_train():
    from tensorflow_models.semi_supervised.ssae import (
        settings, Model, Trainer, Generator, load_data)

    settings = 'fast_config.yaml'
    cfg = Config(settings)

    reset_graph()
    logger = Logger(cfg)
    logger.log_config(cfg)
    gen = Generator(load_data, cfg)
    model = Model(cfg)
    trainer = Trainer(gen, model, cfg, logger)

    assert isinstance(trainer.iterator, tf.data.Iterator)
    for subset in ('train', 'valid', 'test'):
        assert isinstance(getattr(trainer, subset + '_set'), tf.data.Dataset)
        assert isinstance(getattr(trainer, subset + '_init'), tf.Operation)
        n_steps = getattr(trainer, subset + '_steps')
        _, _, m = getattr(gen, subset + ('' if subset[0] == 't' else 'ation'))
        assert type(n_steps) is int, n_steps
        print(n_steps, gen.batch_size, len(m))
        assert n_steps == max(len(m) // gen.batch_size, 1), (n_steps, len(m))

    # import ipdb; ipdb.set_trace()
    trainer.train()
    trainer.evaluate()


if __name__ == '__main__':
    test_ssae_model()
    test_ssae_train()
