import tensorflow as tf
import pytest

from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import reset_graph
from tensorflow_models.base import Config, BaseModel
from tests.helper_functions import make_dataset_iterators

pytestmark = pytest.mark.skip("Deprecated model")


def test_skipgram_model():
    from tensorflow_models.unsupervised.skipgrams import (
        Model, load_data, settings, Generator)

    cfg = Config(settings)
    model = Model(cfg)
    cfg.training_mode = "unsupervised"
    assert isinstance(model, BaseModel)

    gen = Generator(load_data, cfg)
    dt, it, tensors = make_dataset_iterators(gen, cfg.batch_size)

    outputs, losses, metrics = model(*tensors)
    fetches = [outputs['embedding'], losses['loss']]

    init = it.make_initializer(dt)

    with tf.Session() as sess:
        w_init, tf_w_init = outputs['weights_init']
        w_init.run(feed_dict={tf_w_init: gen.emb_loader.build_embedding()})
        init.run()
        tf.global_variables_initializer().run()
        tf.local_variables_initializer().run()
        i = 0
        while True:
            try:
                embedding, loss = sess.run(fetches)
                for k, v in [('embedding', embedding), ]:
                    print("{}: {}".format(k, v.shape))
                for k, v in [('loss', loss), ]:
                    print('{}: {}'.format(k, v))
                if i == 3: break
                i += 1
            except tf.errors.OutOfRangeError:
                print("End of iterator")


def test_skipgram_train():
    from tensorflow_models.unsupervised.skipgrams import (
        settings, Model, Trainer, Generator, load_data)

    cfg = Config(settings)

    reset_graph()
    logger = Logger(cfg)
    logger.log_config(cfg)
    gen = Generator(load_data, cfg)
    model = Model(cfg)
    trainer = Trainer(gen, model, cfg, logger)

    assert isinstance(trainer.iterator, tf.data.Iterator)
    for subset in ('train', 'valid', 'test'):
        assert isinstance(getattr(trainer, subset + '_set'), tf.data.Dataset)
        assert isinstance(getattr(trainer, subset + '_init'), tf.Operation)
        n_steps = getattr(trainer, subset + '_steps')
        _, _, m = getattr(gen, subset + ('' if subset[0] == 't' else 'ation'))
        assert type(n_steps) is int, n_steps
        print(n_steps, gen.batch_size, len(m))
        # assert n_steps == max(len(m) // gen.batch_size, 1), (n_steps, len(m))

    trainer.train()
    # trainer.evaluate()


if __name__ == '__main__':
    test_skipgram_model()
    test_skipgram_train()
