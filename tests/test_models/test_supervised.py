import tensorflow as tf
import pytest

from tensorflow_models.base import BaseModel, Config
from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import reset_graph
from tests.helper_functions import run_tensors

pytestmark = pytest.mark.skip("Deprecated model")


def classifier_test_case(settings, load_data, Model, Generator):
    cfg = Config(settings)
    model = Model(cfg)
    assert isinstance(model, BaseModel)

    gen = Generator(load_data, cfg)
    datasets = [gen.make_dataset(subset) for subset in ('train', 'test', 'valid')]
    it = tf.data.Iterator.from_structure(datasets[0].output_types, datasets[0].output_shapes)
    inputs = it.get_next()
    outputs, losses, metrics = model(*inputs)
    fetches = [outputs['logits'], outputs['predictions'], losses['loss']]
    initializers = [it.make_initializer(d) for d in datasets]

    for dataset_id, name in enumerate(['train', 'valid', 'test']):
        init = initializers[dataset_id]
        for i, (log, pred, loss) in enumerate(run_tensors(init, fetches)):
            for k, v in [('encoding', log), ('reconstruction', pred)]:
                print("{}: {}".format(k, v.shape))
            for k, v in [('loss', loss), ]:
                print('{}: {}'.format(k, v))
            if i == 3: break


def test_fc_classifier_model():
    from tensorflow_models.supervised.fc_classifier_tf import (
        Model, load_data, Generator)
    settings = 'fast_config.yaml'
    classifier_test_case(settings, load_data, Model, Generator)


def test_fc_classifier_train():
    from tensorflow_models.supervised.fc_classifier_tf import (
        Model, Trainer, Generator, load_data)

    settings = 'fast_config.yaml'
    cfg = Config(settings)
    cfg.training_mode = 'supervised'

    for epc in (0, 1, .1, 100, 1000):
        reset_graph()
        cfg.examples_per_class = epc
        logger = Logger(cfg)
        logger.log_config(cfg)
        gen = Generator(load_data, cfg)
        model = Model(cfg)
        try:
            trainer = Trainer(gen, model, cfg, logger)
        except TypeError as err:
            if "Invalid config" in str(err):
                continue
            else:
                raise err

        assert isinstance(trainer.iterator, tf.data.Iterator)
        for subset in ('train', 'valid', 'test'):
            assert isinstance(getattr(trainer, subset + '_set'), tf.data.Dataset)
            assert isinstance(getattr(trainer, subset + '_init'), tf.Operation)
            n_steps = getattr(trainer, subset + '_steps')
            _, _, m = getattr(gen, subset + ('' if subset[0] == 't' else 'ation'))
            assert type(n_steps) is int, (n_steps, type(n_steps))
            assert n_steps == max(sum(m) // gen.batch_size, 1), (n_steps, sum(m))

        trainer.train()
        trainer.evaluate()


if __name__ == '__main__':
    test_fc_classifier_model()
    test_fc_classifier_train()
