import tensorflow as tf
import pytest

from tensorflow_models.base import BaseModel, Config
from tensorflow_models.utils import Logger
from tensorflow_models.utils.nn_utils import reset_graph
from tests.helper_functions import make_dataset_iterators, run_tensors


pytestmark = pytest.mark.skip("Deprecated model")


def autoencoder_test_case(settings, load_data, Model, Generator):
    cfg = Config(settings)
    model = Model(cfg)
    cfg.training_mode = "unsupervised"
    assert isinstance(model, BaseModel)

    gen = Generator(load_data, cfg)
    dt, it, tensors = make_dataset_iterators(gen, cfg.batch_size)
    outputs, losses, metrics = model(*tensors)
    fetches = [
        outputs['encoding'], outputs['reconstruction'],
        losses['loss'], losses['reconstruction_loss']]

    init = it.make_initializer(dt)
    for i, (enc, dec, loss, rloss) in enumerate(run_tensors(init, fetches)):
        for k, v in [('encoding', enc), ('reconstruction', dec)]:
            print("{}: {}".format(k, v.shape))
        for k, v in [('loss', loss), ('reconstruction_loss', rloss)]:
            print('{}: {}'.format(k, v))
        if i == 3: break


def train_autoencoder_test_case(settings, load_data, Model, Generator, Trainer):
    cfg = Config(settings)
    cfg.training_mode = 'unsupervised'

    reset_graph()
    logger = Logger(cfg)
    logger.log_config(cfg)
    gen = Generator(load_data, cfg)
    model = Model(cfg)
    trainer = Trainer(gen, model, cfg, logger)

    assert isinstance(trainer.iterator, tf.data.Iterator)
    for subset in ('train', 'valid', 'test'):
        assert isinstance(getattr(trainer, subset + '_set'), tf.data.Dataset)
        assert isinstance(getattr(trainer, subset + '_init'), tf.Operation)
        n_steps = getattr(trainer, subset + '_steps')
        _, _, m = getattr(gen, subset + ('' if subset[0] == 't' else 'ation'))
        assert type(n_steps) is int, n_steps
        print(n_steps, gen.batch_size, len(m))
        assert n_steps == max(len(m) // gen.batch_size, 1), (n_steps, len(m))

    trainer.train()
    trainer.evaluate()


def test_autoencoder_train():
    from tensorflow_models.unsupervised.autoencoder_tf import (
        Model, Trainer, Generator, load_data)
    settings = 'fast_config.yaml'

    train_autoencoder_test_case(settings, load_data, Model, Generator, Trainer)


def test_symae_train():
    from tensorflow_models.unsupervised.symae import (
        Model, Trainer, Generator, load_data)
    settings = 'fast_config.yaml'

    train_autoencoder_test_case(settings, load_data, Model, Generator, Trainer)


def test_autoencoder_model():
    from tensorflow_models.unsupervised.autoencoder_tf import (
        Model, load_data, Generator)
    settings = 'fast_config.yaml'
    autoencoder_test_case(settings, load_data, Model, Generator)


def test_symae_model():
    from tensorflow_models.unsupervised.symae import (
        Model, load_data, Generator)
    settings = 'fast_config.yaml'
    autoencoder_test_case(settings, load_data, Model, Generator)


if __name__ == '__main__':
    test_autoencoder_model()
    test_autoencoder_train()
    test_symae_model()
    test_symae_train()
